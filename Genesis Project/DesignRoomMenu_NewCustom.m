//
//  DesignRoomMenu_NewCustom.m
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 10. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "DesignRoomMenu.h"

@implementation DesignRoomMenu_NewCustom {
    SKSpriteNode *backButton;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [title setFontColor:[UIColor darkGrayColor]];
        [title setFontSize:25.0*[Functions deviceFactor]];
        [title setPosition:CGPointMake(self.size.width/2, self.size.height-40.0*[Functions deviceFactor])];
        [title setText:NSLocalizedString(@"Custom Map size",nil)];
        [self addChild:title];
        
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2+15.0, self.size.height-backButton.size.height/2)];
        [backButton setZPosition:1.0];
        [backButton setZRotation:-M_PI/2];
        [self addChild:backButton];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    
    if ([backButton containsPoint:location]) {
        [self.view presentScene:_mainMenu transition:[SKTransition pushWithDirection:SKTransitionDirectionDown duration:0.7]];
    }
}

@end
