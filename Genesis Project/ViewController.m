//
//  ViewController.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "ViewController.h"
#import "MainMenu.h"
#import "LevelLoader.h"
#import "Functions.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Functions init];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    SKView * skView = (SKView *)self.view;
    NSLog(@"VIEW: %f x %f",skView.bounds.size.width,skView.bounds.size.height);
    if (!skView.scene) {
        [Functions setViewSize:skView.bounds.size];
        [[LevelLoader loader]setView:skView];
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        skView.showsDrawCount = YES;
        skView.ignoresSiblingOrder = YES; //HUGE PERFORMANCE BOOST
        
        SKScene * scene = [MainMenu sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
