//
//  GameScene.h
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Player.h"

@interface GameScene : SKScene <UIGestureRecognizerDelegate,NSCoding>

+(GameScene*)game;
+(GameScene*)gameSceneWithSize:(CGSize)size;
+(NSArray*)getLevelTiles;
+(void)loadGame:(GameScene*)loadedGame;
+(void)reset;

-(void)initGameWithDict:(NSDictionary*)levelDictionary andSettings:(NSDictionary*)gameSettings forPlayers:(NSArray*)players;
-(BOOL)tileHasUnits: (CGPoint) tilePos;
-(void)unitDiedInCombat: (Unit*)unit;
/*
-(void)unitAttack;
-(void)setSelectedUnit:(id)unit;
-(void)checkStructuresForCapture;
-(BOOL)createUnit:(int)unitNum ofType:(int)unitType on:(SKNode*)tile;
-(void)moveUnit:(Unit*)selectedUnit ToX:(int)endX Y:(int)endY with:(NSArray*)costArray;
-(void)checkStructuresForAction;
*/
@property (nonatomic,strong) NSArray *terrainDefence;

@end
