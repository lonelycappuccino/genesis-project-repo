//
//  Functions.h
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>


#define factor [Functions deviceFactor]
#define viewSize [Functions getViewSize]

@interface Functions : NSObject

+(void)init;
+(CGFloat)deviceFactor;
+(CGFloat)tileSize;
+(void)setViewSize:(CGSize)size;
+(CGSize)getViewSize;

@end

@interface KeyboardView : UIView <UIKeyInput>

@property (nonatomic, strong) SKLabelNode *textlabel;
@property (nonatomic) CGFloat maxLength;

@end

@interface NSObject (MyDeepCopy)
-(id)deepMutableCopy;
@end
