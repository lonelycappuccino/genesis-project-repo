//
//  Structure.h
//  Genesis Project
//
//  Created by Klemen Košir on 17/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Structure : SKSpriteNode <NSCoding>

@property (nonatomic) BOOL isUsable;
@property (nonatomic, strong) SKSpriteNode *menu;
@property (nonatomic) int owner;
@property (nonatomic,strong) NSArray *unitsArray;
@property (nonatomic,strong) NSString *structureName;
@property (nonatomic,strong) NSDictionary *structureDictionary;
@property (nonatomic) int revenue;


+(void)setPlayers:(NSMutableArray*)playersArray;
+(void)setAllStructuresOnMap:(NSMutableArray*)array;
+(Structure*)initWithDictionary:(NSDictionary*)structureDictionary andArray:(NSArray*)unitsArray;
-(SKSpriteNode*)getMenuForPlayer:(int)playerNum withPosition:(CGPoint)pos;
+(NSMutableArray*)getAllStructuresOnMap;
-(void)capturing:(int)playerNum;
-(void)repairStructure:(int)playerNum;
/*
 -(void)selected;
*/
@end
