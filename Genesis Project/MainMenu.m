//
//  MyScene.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "Functions.h"
#import "MainMenu.h"
#import "StoryMenu.h"
#import "CoopMenu.h"
#import "DesignRoomMenu.h"
#import "SelectMapMenu.h"
#import "LevelLoader.h"
#import "SettingsMenu.h"
#import "LoadMenu.h"


@implementation MainMenu {
    SKLabelNode *storyLabel, *versusLabel, *designRoomLabel;
    SKScene *storyMenu, *coopMenu;
    DesignRoomMenu *designRoomMenu;
    SelectMapMenu *versusNewMenu;
    SettingsMenu *settingsMenu;
    LoadMenu *loadMenu;
    //LoadMenu *loadMenu;
    SKShapeNode *storyButton, *versusButton, *designRoomButton, *corporationsButton, *settingsButton, *storeButton, *onlineButton;
    BOOL versusMenu;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        //[self deleteFile:@"CustomMaps.plist"];
        [self checkGameSettings];
        self.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1.0];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        titleLabel.fontColor = [UIColor darkGrayColor];
        titleLabel.fontSize = 50.0*[Functions deviceFactor];
        titleLabel.text = @"Genesis";
        titleLabel.position = CGPointMake(self.size.width/2+10.0*factor, self.size.height*4/5);
        [titleLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeRight];
        [self addChild:titleLabel];
        
        SKLabelNode *titleLabel2 = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        titleLabel2.fontColor = [UIColor darkGrayColor];
        titleLabel2.fontSize = 30.0*[Functions deviceFactor];
        titleLabel2.text = @"Project";
        titleLabel2.position = CGPointMake(self.size.width/2+20.0*factor, self.size.height*4/5);
        [titleLabel2 setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [self addChild:titleLabel2];
        
        storyButton = [SKShapeNode shapeNodeWithCircleOfRadius:60.0*factor];
        storyButton.position = CGPointMake(self.size.width/2, self.size.height/2-20.0);
        [storyButton setFillColor:[UIColor colorWithRed:0.0 green:150.0/255.0 blue:145.0/255.0 alpha:1.0]];
        [storyButton setStrokeColor:[UIColor colorWithRed:0.0 green:100.0/255.0 blue:95.0/255.0 alpha:1.0]];
        [storyButton setLineWidth:0.7*factor];
        //[storyButton setAntialiased:YES];
        [self addChild:storyButton];
        
        [storyButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        storyLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        storyLabel.fontColor = [UIColor whiteColor];
        storyLabel.fontSize = 27.0*[Functions deviceFactor];
        storyLabel.text = @"Story";
        storyLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [storyButton addChild:storyLabel];
        
        versusButton = [SKShapeNode shapeNodeWithCircleOfRadius:42.5*factor];
        versusButton.position = CGPointMake(self.size.width/2+storyButton.frame.size.width/2+versusButton.frame.size.width/2+15.0*factor, storyButton.position.y+25.0*factor);
        [versusButton setFillColor:[UIColor colorWithRed:180.0/255.0 green:0.0 blue:0.0 alpha:1.0]];
        [versusButton setStrokeColor:[UIColor colorWithRed:100.0/255.0 green:0.0 blue:0.0 alpha:1.0]];
        [versusButton setLineWidth:0.7*factor];
        
        [versusButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        versusLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        versusLabel.fontColor = [UIColor whiteColor];
        versusLabel.fontSize = 22.0*[Functions deviceFactor];
        versusLabel.text = @"Versus";
        versusLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [versusButton addChild:versusLabel];
        
        SKShapeNode *newVersusButton = [SKShapeNode shapeNodeWithCircleOfRadius:30.0*factor];
        [newVersusButton setName:@"newVersusButton"];
        [newVersusButton setFillColor:[UIColor colorWithRed:40.0/255.0 green:240.0/255.0 blue:40.0/255.0 alpha:1.0]];
        [newVersusButton setStrokeColor:[UIColor colorWithRed:5.0/255.0 green:140.0/255.0 blue:5.0/255.0 alpha:1.0]];
        [newVersusButton setLineWidth:0.7*factor];
        [newVersusButton setPosition:CGPointMake(versusButton.position.x+versusButton.frame.size.width/2+newVersusButton.frame.size.width/2+6.0*factor, versusButton.position.y+40.0*factor)];
        [self addChild:newVersusButton];
        
        [newVersusButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *newVersusLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [newVersusLabel setText:@"New"];
        [newVersusLabel setFontColor:[UIColor whiteColor]];
        [newVersusLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [newVersusLabel setFontSize:18.0*[Functions deviceFactor]];
        [newVersusButton addChild:newVersusLabel];
        
        SKShapeNode *loadVersusButton = [SKShapeNode shapeNodeWithCircleOfRadius:30.0*factor];
        [loadVersusButton setName:@"loadVersusButton"];
        [loadVersusButton setFillColor:[UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:235.0/255.0 alpha:1.0]];
        [loadVersusButton setStrokeColor:[UIColor colorWithRed:5.0/255.0 green:5.0/255.0 blue:130.0/255.0 alpha:1.0]];
        [loadVersusButton setLineWidth:0.7*factor];
        [loadVersusButton setPosition:CGPointMake(versusButton.position.x+versusButton.frame.size.width/2+loadVersusButton.frame.size.width/2+7.5*factor, versusButton.position.y-27.5*factor)];
        [self addChild:loadVersusButton];
        
        [loadVersusButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *loadVersusLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [loadVersusLabel setText:@"Load"];
        [loadVersusLabel setFontColor:[UIColor whiteColor]];
        [loadVersusLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [loadVersusLabel setFontSize:18.0*[Functions deviceFactor]];
        [loadVersusButton addChild:loadVersusLabel];
        //REMOVE DIS
        versusMenu = YES;
        [self addChild:versusButton];
        
        designRoomButton = [SKShapeNode shapeNodeWithCircleOfRadius:42.5*factor];
        designRoomButton.position = CGPointMake(self.size.width/2-storyButton.frame.size.width/2-designRoomButton.frame.size.width/2-22.5*factor, storyButton.position.y+30.0*factor);
        [designRoomButton setFillColor:[UIColor colorWithRed:0.95 green:0.75 blue:0.0 alpha:1.0]];
        [designRoomButton setStrokeColor:[UIColor colorWithRed:0.8 green:0.6 blue:0.0 alpha:1.0]];
        [designRoomButton setLineWidth:0.7*factor];
        [self addChild:designRoomButton];
        
        [designRoomButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        designRoomLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        designRoomLabel.fontColor = [UIColor whiteColor];
        designRoomLabel.fontSize = 22.0*[Functions deviceFactor];
        designRoomLabel.text = @"Maps";
        designRoomLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [designRoomButton addChild:designRoomLabel];
        
        settingsButton = [SKShapeNode shapeNodeWithCircleOfRadius:30.0*factor];
        settingsButton.position = CGPointMake(storyButton.position.x, storyButton.position.y-100.0*[Functions deviceFactor]);
        [settingsButton setFillColor:[UIColor lightGrayColor]];
        [settingsButton setStrokeColor:[UIColor darkGrayColor]];
        [settingsButton setLineWidth:0.7*factor];
        [self addChild:settingsButton];
        
        [settingsButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *settingsLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        settingsLabel.fontColor = [UIColor whiteColor];
        settingsLabel.fontSize = 18.0*[Functions deviceFactor];
        settingsLabel.text = @"Settings";
        settingsLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [settingsLabel setXScale:0.8];
        [settingsButton addChild:settingsLabel];
        
        storeButton = [SKShapeNode shapeNodeWithCircleOfRadius:40.0*factor];
        storeButton.position = CGPointMake(designRoomButton.position.x+25.0*factor, designRoomButton.position.y-100.0*[Functions deviceFactor]);
        [storeButton setFillColor:[UIColor whiteColor]];
        [storeButton setStrokeColor:[UIColor lightGrayColor]];
        [storeButton setLineWidth:0.7*factor];
        [self addChild:storeButton];
        
        [storeButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *storeLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        storeLabel.fontColor = [UIColor darkGrayColor];
        storeLabel.fontSize = 22.0*[Functions deviceFactor];
        storeLabel.text = @"Store";
        storeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [storeButton addChild:storeLabel];
        
        onlineButton = [SKShapeNode shapeNodeWithCircleOfRadius:40.0*factor];
        onlineButton.position = CGPointMake(versusButton.position.x-20.0*factor, storyButton.position.y-70.0*[Functions deviceFactor]);
        [onlineButton setFillColor:[UIColor whiteColor]];
        [onlineButton setStrokeColor:[UIColor lightGrayColor]];
        [onlineButton setLineWidth:0.7*factor];
        [self addChild:onlineButton];
        
        [onlineButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *onlineLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        onlineLabel.fontColor = [UIColor darkGrayColor];
        onlineLabel.fontSize = 22.0*[Functions deviceFactor];
        onlineLabel.text = @"Online";
        onlineLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [onlineButton addChild:onlineLabel];
        
        settingsMenu = [SettingsMenu sceneWithSize:self.size];
        [settingsMenu setMainMenu:self];
        
        storyMenu = [StoryMenu sceneWithSize:self.size];
        coopMenu = [CoopMenu sceneWithSize:self.size];
        designRoomMenu = [DesignRoomMenu sceneWithSize:self.size];
        [designRoomMenu setMainMenu:self];
        
        //[[LevelLoader loader]saveAllLevelImages];
        
        versusNewMenu = [SelectMapMenu sceneWithSize:self.size];
        
        loadMenu = [LoadMenu sceneWithSize:self.size];
        
        //[self saveTexture];
        //[self readTexture];
        
        
        SKLabelNode *buildLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        buildLabel.fontColor = [UIColor blackColor];
        buildLabel.fontSize = 11.0*[Functions deviceFactor];
        buildLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        [buildLabel setPosition:CGPointMake(3.0*factor, 3.0*factor)];
        buildLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeBottom;
        buildLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
        [self addChild:buildLabel];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *gestureRecognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:gestureRecognizer];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"TAP");
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([storyButton containsPoint:location]) {
        SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:0.0 green:150.0/255.0 blue:145.0/255.0 alpha:1.0] duration:0.7];
        [transition setPausesOutgoingScene:NO];
        [storyButton setZPosition:99999.0];
        [storeButton removeAllActions];
        [storyButton.children[0] runAction:[SKAction fadeOutWithDuration:0.05]];
        [storyButton runAction:[SKAction scaleTo:12.0 duration:0.2]];
        [self.view presentScene:storyMenu transition:transition];
    }
    else if ([versusButton containsPoint:location]) {
        if (!versusMenu) {
            /*
            [designRoomButton runAction:[SKAction moveToY:designRoomButton.position.y-designRoomButton.size.height duration:0.1]];
            [settingsButton runAction:[SKAction moveToY:settingsButton.position.y-settingsButton.size.height duration:0.1]];
            [[self childNodeWithName:@"newVersusButton"]runAction:[SKAction moveToY:versusButton.position.y-versusButton.size.height duration:0.1]];
            [[self childNodeWithName:@"loadVersusButton"]runAction:[SKAction moveToY:versusButton.position.y-versusButton.size.height duration:0.1] completion:^{
                versusMenu = YES;
            }];
             */
        }
    }
    else if ([designRoomButton containsPoint:location]) {
        SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:0.95 green:0.75 blue:0.0 alpha:1.0] duration:0.75];
        [transition setPausesOutgoingScene:NO];
        [designRoomButton setZPosition:99999.0];
        [designRoomButton removeAllActions];
        [designRoomButton.children[0] runAction:[SKAction fadeOutWithDuration:0.05]];
        [designRoomButton runAction:[SKAction scaleTo:15.0 duration:0.2]];
        [self.view presentScene:designRoomMenu transition:transition];
    }
    else if ([settingsButton containsPoint:location]) {
        NSLog(@"SETTINGS");
        SKTransition *transition = [SKTransition fadeWithColor:[UIColor lightGrayColor] duration:0.7];
        [transition setPausesOutgoingScene:NO];
        [settingsButton setZPosition:99999.0];
        [settingsButton removeAllActions];
        [settingsButton.children[0] runAction:[SKAction fadeOutWithDuration:0.05]];
        [settingsButton runAction:[SKAction scaleTo:15.0 duration:0.2]];
        [self.view presentScene:settingsMenu transition:transition];
    }
    else if (versusMenu) {
        if ([[self childNodeWithName:@"newVersusButton"]containsPoint:location]) {
            SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:40.0/255.0 green:240.0/255.0 blue:40.0/255.0 alpha:1.0] duration:0.7];
            [transition setPausesOutgoingScene:NO];
            [[self childNodeWithName:@"newVersusButton"] setZPosition:99999.0];
            [[self childNodeWithName:@"newVersusButton"] removeAllActions];
            [[self childNodeWithName:@"newVersusButton"].children[0] runAction:[SKAction fadeOutWithDuration:0.05]];
            [[self childNodeWithName:@"newVersusButton"] runAction:[SKAction scaleTo:18.0 duration:0.2]];
            [self.view presentScene:versusNewMenu transition:transition];
        }
        else if ([[self childNodeWithName:@"loadVersusButton"]containsPoint:location]) {
            SKTransition *transition = [SKTransition fadeWithColor:[UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:235.0/255.0 alpha:1.0] duration:0.7];
            [transition setPausesOutgoingScene:NO];
            [[self childNodeWithName:@"loadVersusButton"] setZPosition:99999.0];
            [[self childNodeWithName:@"loadVersusButton"] removeAllActions];
            [[self childNodeWithName:@"loadVersusButton"].children[0] runAction:[SKAction fadeOutWithDuration:0.05]];
            [[self childNodeWithName:@"loadVersusButton"] runAction:[SKAction scaleTo:18.0 duration:0.2]];
            [self.view presentScene:loadMenu transition:transition];
        }
        else {
            /*
            [designRoomButton runAction:[SKAction moveToY:designRoomButton.position.y+designRoomButton.size.height duration:0.25]];
            [settingsButton runAction:[SKAction moveToY:settingsButton.position.y+settingsButton.size.height duration:0.25]];
            [[self childNodeWithName:@"newVersusButton"]runAction:[SKAction moveToY:versusButton.position.y duration:0.1]];
            [[self childNodeWithName:@"loadVersusButton"]runAction:[SKAction moveToY:versusButton.position.y duration:0.1] completion:^{
                versusMenu = NO;
            }];
             */
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {

}

-(void)resetDesignMenu {
    designRoomMenu = [DesignRoomMenu sceneWithSize:self.size];
}

-(void)saveTexture {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    /*
    //DELETE ALL FILES IN DOCUMENTS DIRECTORY
     for (NSString *file in [fileManager contentsOfDirectoryAtPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] error:nil]) {
     [fileManager removeItemAtPath:file error:nil];
     }
    */
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"screenshots1.plist"];
    NSMutableDictionary *screenshotsDict;
    //NSLog(@"file path: %@",filePath);
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        screenshotsDict = [NSMutableDictionary dictionary];
        //NSLog(@"new file");
    }
    else {
            screenshotsDict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    }
    SKTexture *texture = [SKTexture textureWithImageNamed:@"Sea"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:texture];
    [screenshotsDict setObject:data forKey:@"texture"];
    //BOOL success = [screenshotsDict writeToFile:filePath atomically:YES];
    //NSLog(@"SUCCESS: %d",success);
}

-(void)readTexture {
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"screenshots1.plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSData *data = [dict objectForKey:@"Level0"];
    SKTexture *texture = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    SKSpriteNode *testNode = [SKSpriteNode spriteNodeWithTexture:texture];
    [testNode setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [self addChild:testNode];
}

-(void)deleteFile:(NSString*)fileName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:fileName];
    NSError *error;
    [fileManager removeItemAtPath:filePath error:&error];
    if (error) {
        NSLog(@"ERROR: %@",error);
    }
}

-(void)checkGameSettings {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"gameSettings.plist"];
    
    NSDictionary *gameDataDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"GameData" ofType:@"plist"]];

    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:[gameDataDict objectForKey:@"Settings"] attributes:nil];
    }
    
    filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"teams.data"];
    NSLog(@"teams1: %@",[gameDataDict objectForKey:@"Teams"]);
    if (![fileManager fileExistsAtPath:filePath]) {
        NSLog(@"success %d",[fileManager createFileAtPath:filePath contents:[gameDataDict objectForKey:@"Teams"] attributes:nil]);
    }
    
    //SAVED GAMES PLIST
    filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"savedGames.save"];
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
}

@end
