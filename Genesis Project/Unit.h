//
//  Unit.h
//  Genesis Project
//
//  Created by Klemen Košir on 17/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Unit : SKSpriteNode

@property (nonatomic, strong) NSString *unitName;
@property (nonatomic) int primaryAmmo;
@property (nonatomic, strong) NSString *primaryWeapon;
@property (nonatomic, strong) NSArray *primaryWeaponDamage;
@property (nonatomic) int weaponRange;
@property (nonatomic, strong) NSString *secondaryWeapon;
@property (nonatomic, strong) NSArray *secondaryWeaponDamage;
@property (nonatomic) int health;
@property (nonatomic) int movement;
@property (nonatomic) int fuel;
@property (nonatomic) int vision;
@property (nonatomic) int armor;
@property (nonatomic) BOOL invisible;
@property (nonatomic) CGPoint unitPosition;
@property (nonatomic) CGPoint unitPositionOnWorld;
@property (nonatomic) int owner;
@property (nonatomic) BOOL canMove;
@property (nonatomic) BOOL canAttack;
@property (nonatomic, strong) UIColor *unitColor;
@property (nonatomic) int type;
@property (nonatomic) int maxHealth;
@property (nonatomic) int maxMovement;
@property (nonatomic) int movementType;
@property (nonatomic) double bestChoice;
@property (nonatomic, strong) NSString *special;
@property (nonatomic, strong) NSArray *objectivesMap;

-(void)createUnitWithDictionary:(NSDictionary*)dict;
//-(void)selected;
-(void)moveUnitFromStart:(NSArray*)path;
//-(void)showUnitMenu;
//-(void)hideUnitMenu;
//-(int)menuSelected:(CGPoint)location;
-(int)getMovement;
-(int)getWeaponRange;
-(void)updateCostMapWithMap: (NSArray*) levelCostMap;
-(int)checkPath:(int)endX :(int)endY with:(NSArray*)costArray;
-(CGFloat)attackDamageOnUnit:(Unit*)attackedUnit;
-(void)loadUnit:(Unit*)unit;
-(Unit*)unloadUnit;
-(BOOL)hasLoadedUnit;
-(Unit*)getLoadedUnit;
-(NSArray*)findNodesInMovementRange;
-(NSArray*)isAmbushedOnWayToX:(int)endX Y:(int)endY with:(NSArray*)levelCostMap;

+(void)unit:(Unit*)firstUnit attacksUnit:(Unit*)secondUnit;

@end
