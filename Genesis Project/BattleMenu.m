//
//  BattleMenu.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "BattleMenu.h"

@implementation BattleMenu

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        titleLabel.fontColor = [UIColor darkGrayColor];
        titleLabel.fontSize = 35.0;
        titleLabel.text = @"Battle";
        titleLabel.position = CGPointMake(self.size.width/2, self.size.height*3/4);
        [self addChild:titleLabel];
        
        SKLabelNode *localLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        localLabel.fontColor = [UIColor grayColor];
        localLabel.fontSize = 30.0;
        localLabel.text = @"Local";
        localLabel.position = CGPointMake(self.size.width/3, self.size.height/2);
        [self addChild:localLabel];
        
        SKLabelNode *onlineLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        onlineLabel.fontColor = [UIColor grayColor];
        onlineLabel.fontSize = 30.0;
        onlineLabel.text = @"Online";
        onlineLabel.position = CGPointMake(self.size.width*2/3, self.size.height/2);
        [self addChild:onlineLabel];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

-(void)update:(CFTimeInterval)currentTime {
    
}

@end
