//
//  Player.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "Player.h"
#import "GameScene.h"

@implementation Player

static __strong NSMutableArray *players = nil;

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt:_teamID forKey:@"teamID"];
    [aCoder encodeInt:_playerNumber forKey:@"playerNumber"];
    [aCoder encodeInt:_money forKey:@"money"];
    [aCoder encodeInt:_agressive forKey:@"agressive"];
    [aCoder encodeInt:_defensive forKey:@"defensive"];
    [aCoder encodeInt:_neutral forKey:@"neutral"];
    [aCoder encodeInt:_shortGame forKey:@"shortGame"];
    [aCoder encodeInt:_longGame forKey:@"longGame"];
    [aCoder encodeInt:_guerillaTactics forKey:@"guerillaTactics"];
    [aCoder encodeInt:_humanWaveDoctrine forKey:@"humanWaveDoctrine"];
    [aCoder encodeInt:_moneyPerTurn forKey:@"moneyPerTurn"];
    
    [aCoder encodeObject:_friendlyUnits forKey:@"friendlyUnits"];
    [aCoder encodeObject:_enemyUnits forKey:@"enemyUnits"];
    [aCoder encodeObject:_structures forKey:@"structures"];
    [aCoder encodeObject:_levelCostMap forKey:@"levelCostMap"];
    [aCoder encodeObject:_playerName forKey:@"playerName"];
    [aCoder encodeObject:_levelMap forKey:@"levelMap"];
    [aCoder encodeObject:_level forKey:@"level"];
    [aCoder encodeObject:_extraDefence forKey:@"extraDefence"];
    [aCoder encodeObject:_playerColor forKey:@"playerColor"];
    [aCoder encodeObject:_moneyLabel forKey:@"moneyLabel"];
    [aCoder encodeBool:_computer forKey:@"computer"];
    [aCoder encodeCGPoint:_cameraPosition forKey:@"cameraPosition"];
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _teamID = [aDecoder decodeIntForKey:@"teamID"];
        _playerNumber = [aDecoder decodeIntForKey:@"playerNumber"];
        _money = [aDecoder decodeIntForKey:@"money"];
        _agressive = [aDecoder decodeIntForKey:@"agressive"];
        _defensive = [aDecoder decodeIntForKey:@"defensive"];
        _neutral = [aDecoder decodeIntForKey:@"neutral"];
        _shortGame = [aDecoder decodeIntForKey:@"shortGame"];
        _longGame = [aDecoder decodeIntForKey:@"longGame"];
        _guerillaTactics = [aDecoder decodeIntForKey:@"guerillaTactics"];
        _humanWaveDoctrine = [aDecoder decodeIntForKey:@"humanWaveDoctrine"];
        _moneyPerTurn = [aDecoder decodeIntForKey:@"moneyPerTurn"];
        
        _friendlyUnits = [aDecoder decodeObjectForKey:@"friendlyUnits"];
        _enemyUnits = [aDecoder decodeObjectForKey:@"enemyUnits"];
        _structures = [aDecoder decodeObjectForKey:@"structures"];
        _levelCostMap = [aDecoder decodeObjectForKey:@"levelCostMap"];
        _playerName = [aDecoder decodeObjectForKey:@"playerName"];
        _levelMap = [aDecoder decodeObjectForKey:@"levelMap"];
        _level = [aDecoder decodeObjectForKey:@"level"];
        _extraDefence = [aDecoder decodeObjectForKey:@"extraDefence"];
        _playerColor = [aDecoder decodeObjectForKey:@"playerColor"];
        _moneyLabel = [aDecoder decodeObjectForKey:@"moneyLabel"];
        _computer = [aDecoder decodeBoolForKey:@"computer"];
        _cameraPosition = [aDecoder decodeCGPointForKey:@"cameraPosition"];
    }
    return self;
}

-(id)init {
    if (self = [super init]) {
        _friendlyUnits = [NSMutableArray array];
        _enemyUnits = [NSMutableArray array];
        _structures = [NSMutableArray array];
        //NSLog(@"unit");
        _money = 10000;
        _moneyPerTurn = 0;
    }
    return self;
}

+(void)createPlayers:(int)numberOfPlayers {
    players = [NSMutableArray array];
    for (int i = 0; i < numberOfPlayers; i++) {
        Player *player = [Player new];
        
        [players addObject:player];
    }
}

+(Player*)getPlayer:(int)playerNumber {
    return players[playerNumber];
}

+(Player*)addPlayer {
    Player *player = [Player new];
    return player;
}

+(void)setPlayers:(NSMutableArray*)playersArray {
    players = playersArray;
}

+(void)removePlayer:(int)index {
    [players removeObjectAtIndex:index];
}


-(void)addUnit:(Unit*)unit forPlayer:(int)playerNum {
    if (_playerNumber != playerNum) {
        [_enemyUnits[playerNum] addObject:unit];
    }
    else {
        [_friendlyUnits addObject:unit];
    }
}

-(void)removeUnit:(Unit *)unit {
    if (_playerNumber != unit.owner) {
        [_enemyUnits[unit.owner] removeObject:unit];
    }
    else {
        [_friendlyUnits removeObject:unit];
    }
}

-(void)addStructure:(Structure*)structure {
    [_structures addObject:structure];
    _moneyPerTurn += structure.revenue;
}

-(void)removeStructure:(Structure*)structure {
    [_structures removeObject:structure];
    _moneyPerTurn -= structure.revenue;
}

-(void)setMoney:(int)money {
    _money = money;
    if (_moneyLabel) {
        [_moneyLabel setText:[NSString stringWithFormat:@"%d",money]];
    }
}

-(void)addUnitArray {
    for (int i = 0; i < players.count; i++) {
        [_enemyUnits addObject:[NSMutableArray array]];
    }
}

-(NSArray*)findNodesInMovementRangeForUnit:(Unit*) unit {
    [unit updateCostMapWithMap:_levelCostMap];
    NSArray *posibleMovesTmp = [unit findNodesInMovementRange];
    NSMutableArray *posibleMoves = [NSMutableArray array];
    NSArray *levelTiles = [GameScene getLevelTiles];
    for (NSValue *value in posibleMovesTmp) {
        if (_level[(int)[value CGPointValue].x][(int)[value CGPointValue].y][2] == [NSNull null]) {
            [posibleMoves addObject: levelTiles[(int)[value CGPointValue].x][(int)[value CGPointValue].y]];
        }
    }
    return posibleMoves;
}


-(void)createLevelArrayWithLevelMap:(NSArray*) levelMapArray {
    _level = [NSMutableArray array];
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (int y = 0; y < levelMapArray.count; y++) {
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (int x = 0; x < ((NSArray*)levelMapArray[y]).count; x++) {
            NSMutableArray *tmpZ = [[NSMutableArray alloc] init];
            NSMutableArray *tempZ = [[NSMutableArray alloc] init];
            tempZ = [NSMutableArray arrayWithArray:levelMapArray[y][x]];
            //0 = terrain
            if (tempZ.count > 0) {
                [tmpZ addObject:tempZ[0]];
            }
            else {
                [tmpZ addObject:[NSNull null]];
            }
            //1 = structures
            [tmpZ addObject:[NSNull null]];
            //2 = units
            [tmpZ addObject:[NSNull null]];
            //3 = fog of war (0 = fog, else visible)
            BOOL visible = NO;
            [tmpZ addObject:[NSNumber numberWithBool:visible]];
            //TODO add this somewhere else and go thu(???through???) it only once
            for (int i = 0; i < players.count; i++) {
                for (Structure* building in ((Player*)players[i]).structures) {
                    CGPoint pos = CGPointFromString([building.parent.userData objectForKey:@"tilePosition"]);
                    if (pos.x == x && pos.y == y) {
                        //NSLog(@"%d %d pos",x,y);
                        [tmpZ setObject:building atIndexedSubscript:1];
                    }
                }
            }
            
            //4 = extra defence
            if ([tmpZ objectAtIndex:1] == [NSNull null]) {
                //NSLog(@"%@",[GameScene game]);
                if (tmpZ[0] != [NSNull null]) {
                    [tmpZ addObject:[GameScene game].terrainDefence[[tmpZ[0][0] intValue]]];
                }
                else {
                    [tmpZ addObject:[NSNumber numberWithInt:-1]];
                }
            }
            else {
                [tmpZ addObject:[((NSDictionary*)((Structure*)tmpZ[1]).structureDictionary) objectForKey:@"ExtraDefence"]];
            }
            //NSLog(@"qwe %@  %@", tmpZ[1], tmpZ[2]);
            
            [tmp addObject:tmpZ];
        }
        [temp addObject:tmp];
    }
    
    
    /*
    BOOL visible = (BOOL)temp[y][x][4];
    //check the visible area for any units that might be visible (friendly or hostile)
    if (visible) {
        for (int i = 0; i < players.count; i++) {
            for (Unit* unit in ((Player*)players[i]).units[i]) {
                //NSLog(@"%@ %@",unit, ((Player*)players[i]).units[i]);
                CGPoint pos = unit.unitPosition;
                if (pos.x == x && pos.y == y) {
                    [tmpZ setObject:unit atIndexedSubscript:2];
                }
            }
        }
    }*/

    _level = [NSArray arrayWithArray:temp];
}

-(void)updateFogMap {
    
    //TODO implement the vision of players and buildings and calculate the shadows of certain terrains / buildings
#pragma mark turnOff-FOG
    BOOL visible = YES;
    //NO = FOG, YES = NO FOG
    for (int y = 0; y < _level.count; y++) {
        for (int x = 0; x < ((NSArray*)_level[y]).count; x++) {
            [_level[y][x] setObject:[NSNumber numberWithBool:visible] atIndex:3];
            [_level[y][x] setObject:[NSNull null] atIndex:2];
        }
    }
    
    visible = YES;
    //for buildings
    for (Structure *building in _structures) {
        CGPoint pos = CGPointFromString([building.parent.userData objectForKey:@"tilePosition"]);
        NSArray *neighbors = [self getNeighborsForTile:pos];
        for (int i = 0; i < neighbors.count; i++) {
            CGPoint neighbor = [((NSValue*)neighbors[i]) CGPointValue];
            [_level[(int)neighbor.y][(int)neighbor.x] setObject:[NSNumber numberWithBool:visible] atIndex: 3];
        }
        [_level[(int)pos.y][(int)pos.x] setObject:[NSNumber numberWithBool:visible] atIndex: 3];
    }
    
    
    //and units
    for (Unit *unit in _friendlyUnits) {
        CGPoint pos = unit.unitPosition;
        NSArray *neighbors = [self getNeighborsForTile:pos];
        //TODO include the range of vision of units
        for (int i = 0; i < neighbors.count; i++) {
            CGPoint neighbor = [((NSValue*)neighbors[i]) CGPointValue];
            [_level[(int)neighbor.y][(int)neighbor.x] setObject:[NSNumber numberWithBool:visible] atIndex: 3];
        }
        [_level[(int)pos.y][(int)pos.x] setObject:[NSNumber numberWithBool:visible] atIndex: 3];
        [_level[(int)pos.y][(int)pos.x] setObject: unit atIndex: 2];
    }
}

-(BOOL)tileIsInVisibleMap:(CGPoint)tilePosition {
    //TODO optimize
    NSArray *terrainMap = _levelMap;
    if (tilePosition.y < terrainMap.count && tilePosition.y >= 0 && tilePosition.x < ((NSArray*)terrainMap[(int)tilePosition.y]).count && tilePosition.x >= 0) {
        //NSLog(@"%@",terrainMap[(int)tilePosition.x][(int)tilePosition.y]);
        //NSLog(@"%@",terrainMap[(int)tilePosition.y][(int)tilePosition.x]);
        if (terrainMap[(int)tilePosition.y][(int)tilePosition.x] != [NSNull null]) {
            return YES;
        }
    }
    
    return NO;
}

-(NSArray*)getNeighborsForTile:(CGPoint)tilePosition {
    NSMutableArray *neighbors = [NSMutableArray array];
    
    //check if neighbors are on the map and valid places
    //TODO optimize this if needed
    if ([self tileIsInVisibleMap:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]];
    }
    return neighbors;
}

-(void)updateArrays {
    //update level array of units, buildings and fog of war
    //[GameScene updatefog];
    
    /*
     for (int y = 0; y < _level.count; y++) {
     for (int x = 0; x < ((NSArray*)_level[y]).count; x++) {
     [_level[y][x] setObject:[NSNull null] atIndexedSubscript:2];
     //if you cant see the tile do not update the array for it and remove any unit on it
     if (_level[y][x][3] == 0) {
     [_level[y][x] setObject:[NSNull null] atIndexedSubscript:2];
     continue;
     }
     for (int i = 0; i < players.count; i++) {
     for (Unit* unit in ((Player*)players[i]).units[i]) {
     //NSLog(@"%@",unit);
     CGPoint pos = unit.unitPosition;
     if (pos.x == x && pos.y == y) {
     [_level[y][x] setObject:unit atIndexedSubscript:2];
     }
     }
     }
     
     }
     }*/
    
    
}

-(void)updateLevelCostMapWith: (NSMutableArray*) baseLevelCostMap {
    //TODO if a bridge is destroyed it will "know" so make player specific baseLevelCostMaps
    for (int i = 0; i < baseLevelCostMap.count; i++) {
        for (int j = 0; j < ((NSArray*)baseLevelCostMap[i]).count; j++) {
            if ([_level[i][j][3] boolValue]) {
                
            }
        }
    }
    _levelCostMap = [NSMutableArray arrayWithArray: baseLevelCostMap];
    for (NSArray *hostileUnits in _enemyUnits) {
        for (Unit *unit in hostileUnits) {
            //NSMutableArray *temp = _levelCostMap[(int)unit.unitPosition.x][(int)unit.unitPosition.y];
            for (int i = 0; i < 7; i++) {
                //NSLog(@"awa");
                [_levelCostMap[(int)unit.unitPosition.y][(int)unit.unitPosition.x] replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:9999]];
                //NSLog(@"bwa");
            }
        }
    }
}


/*
-(void)chooseAIPersonalityForDifficulty:(int) diff {
    //more complex way of determening AI personality may be implemented in the future ...NO SHIT
    _guerillaTactics = (arc4random() % diff);
    _humanWaveDoctrine = (arc4random() % (diff-_guerillaTactics));
    
    _longGame = (arc4random() % diff);
    _shortGame = (arc4random() % (diff-_longGame));
    
    _neutral = (arc4random() % (diff/2));
    _agressive = (arc4random() % (diff-_neutral));
    _defensive = (arc4random() % (diff-_neutral));
    NSLog(@"AI Player %d will play with the following atribues: Neutral (%d), Agressive (%d), Defensive (%d), Guerilla (%d), Human Wave (%d) with a game pace for short games (%d) and long games (%d)", _playerNumber, _neutral, _agressive, _defensive, _guerillaTactics, _humanWaveDoctrine, _shortGame, _longGame);
}


-(void)createUnitObjectivesMapForUnit: (Unit*) unit {
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (int y = 0; y < _mapHeight; y++) {
        NSMutableArray *tmp = [[NSMutableArray alloc] init];
        for (int x = 0; x < _mapWidth; x++) {
            double bla =0.0;
            [tmp addObject:[NSNumber numberWithInt:bla]];
        }
        [temp addObject:tmp];
    }
    unit.objectivesMap = [NSArray arrayWithArray:temp];
}

-(void)playForAI {
    //NSLog(@"Play for AI");
   *//* for (int i = 0; i < players.count; i++) {
        NSLog(@"%d %d",players.count, i);
        for (Unit* unit in ((Player*)players[i]).units[i]) {
            NSLog(@"PPPPPPPPPPP%d",[_units[i] indexOfObject:unit]);
        }
    }*//*
    [self buildUnits];
    [self moveUnits];
    
}


-(void)buildUnits {
    //build units on predetermined rating
    
    for (Structure* building in _structures) {
        //rate this building for every unit type it can produce and decide on the best unit to build (if any at all)
        if (([[building.userData objectForKey:@"Name"] isEqualToString:@"Factory"])) {
            NSLog(@"factory building infantry on %@",[building.parent.userData objectForKey:@"tilePosition"]);
            CGPoint tilePosition = CGPointFromString([building.parent.userData objectForKey:@"tilePosition"]);
            
            //check theArray if the tile is visible & does not have a unit and then build on it
            //[[GameScene game] createUnit:0 ofType:0 on:building];
            //NSLog(@"_level[(i %@",_level[(int) tilePosition.y][(int) tilePosition.x][2]);
            if (_level[(int) tilePosition.y][(int) tilePosition.x][3] != 0 && (_level[(int) tilePosition.y][(int) tilePosition.x][2] == [NSNull null])) {
                
                [[GameScene game] createUnit:0 ofType:0 on:building];
                //int unitNum = ((NSArray*)_units[_playerNumber]).count;
                [_level[(int) tilePosition.y][(int) tilePosition.x] setObject:[_units[_playerNumber] lastObject] atIndexedSubscript:2];
                //ADD the unit to theArray and run the update
                //level[tilePosition.x][tilePosition.y][0]
            }
        }
    }
}

-(void)ratePlaceForUnit:(Unit*) unit {
    //rate places for every unit
    for (int y = 0; y < _mapHeight; y++) {
        for (int x = 0; x < _mapWidth; x++) {
            if (_level[y][x][2] != [NSNull null]) {
                //if the field is ocupied by units dont go there (duh)
                unit.objectivesMap[y][x] = [NSNumber numberWithDouble: -9999.99];
                //if it's our unit provide backup (defensive) otherwise calculate agressive posibilities
                double dispersion = 0.0;
                if(((Unit*)_level[y][x][2]).owner == _playerNumber) {
                    dispersion += _defensive/2;
                    //check if the nearby area has any good spots
                    int posibleY = unit.unitPosition.y - unit.getWeaponRange;
                    if (posibleY < 0) {
                        posibleY = 0;
                    }
                    int maxHeight = unit.unitPosition.y + unit.getWeaponRange;
                    if (maxHeight >= _mapHeight) {
                        maxHeight = _mapHeight - 1;
                    }
                    for (; posibleY <= maxHeight; posibleY++) {
                        int rangeY = abs(posibleY - unit.unitPosition.y);
                        int posibleX = unit.unitPosition.x - unit.getWeaponRange;
                        if (posibleX < 0) {
                            posibleX = 0;
                        }
                        int maxWidth = unit.unitPosition.x + unit.getWeaponRange;
                        if (maxWidth >= _mapWidth) {
                            maxWidth = _mapWidth - 1;
                        }
                        for (; posibleX <= maxWidth; posibleX++) {
                            int rangeX = abs(posibleX - unit.unitPosition.x);
                            //target in in weapon range
                            if ((rangeX + rangeY) <= unit.getWeaponRange) {
                                if(_level[posibleY][posibleX][2] != [NSNull null]){
                                    unit.objectivesMap[posibleY][posibleX] = [NSNumber numberWithDouble: -9999.99];
                                }
                                else {
                                    unit.objectivesMap[posibleY][posibleX] = [NSNumber numberWithDouble:([unit.objectivesMap[posibleY][posibleX] doubleValue] + dispersion + ([_level[posibleY][posibleX][4] doubleValue] * _defensive))];
                                }
                            }
                        }
                    }

                }
                else {
                    dispersion += _agressive;
                    //check if the nearby area has any good spots
                    int posibleY = unit.unitPosition.y - unit.getWeaponRange-1;
                    if (posibleY < 0) {
                        posibleY = 0;
                    }
                    int maxHeight = unit.unitPosition.y + unit.getWeaponRange+1;
                    if (maxHeight >= _mapHeight) {
                        maxHeight = _mapHeight - 1;
                    }
                    for (; posibleY <= maxHeight; posibleY++) {
                        int rangeY = abs(posibleY - unit.unitPosition.y);
                        int posibleX = unit.unitPosition.x - unit.getWeaponRange-1;
                        if (posibleX < 0) {
                            posibleX = 0;
                        }
                        int maxWidth = unit.unitPosition.x + unit.getWeaponRange+1;
                        if (maxWidth >= _mapWidth) {
                            maxWidth = _mapWidth - 1;
                        }
                        for (; posibleX <= maxWidth; posibleX++) {
                            int rangeX = abs(posibleX - unit.unitPosition.x);
                            //target in in weapon range
                            if ((rangeX + rangeY) <= unit.getWeaponRange) {
                                if(_level[posibleY][posibleX][2] != [NSNull null]){
                                    unit.objectivesMap[posibleY][posibleX] = [NSNumber numberWithDouble: -9999.99];
                                }
                                else {
                                    unit.objectivesMap[posibleY][posibleX] = [NSNumber numberWithDouble:([unit.objectivesMap[posibleY][posibleX] doubleValue] + dispersion + ([_level[posibleY][posibleX][4] doubleValue] * _defensive))];
                                }
                            }
                        }
                    }

                }
                
            }
        }
    }
    
}

-(void)moveUnits {
    
    for (Unit* unit in _units[_playerNumber]) {
        //rate posible moves for this unit
        [self ratePlaceForUnit:unit];
        
        //move this unit on the above rating
        int bestX = -1;
        int bestY = -1;
        //NSLog(@"i %@",unit.objectivesMap);
        
        printf("\nObjectives map\n");
        for (int i = unit.objectivesMap.count-1; i >= 0; i--) {
            NSArray *row = unit.objectivesMap[i];
            for (int j = 0; j < row.count; j++) {
                printf("%f ", [row[j] doubleValue]);
            }
            printf("\n");
        }
        
        for (int y = 0; y < _mapHeight; y++) {
            for (int x = 0; x < _mapWidth; x++) {
                //NSLog(@"(%d,%d) %f %f",y,x, unit.bestChoice, [unit.objectivesMap[y][x]doubleValue]);
                //if the place already has a unit ignore it unless it's the current unit
                if (_level[y][x][2] != [NSNull null]) {
                    continue;
                }
                
                if ((unit.bestChoice < [unit.objectivesMap[y][x]doubleValue]) && ([unit checkPath:x :y with:_levelCostMap])){
                    unit.bestChoice = [unit.objectivesMap[y][x]doubleValue];
                    bestX = x;
                    bestY = y;
                }
            }
        }
        if ((bestY != -1) && (bestX != -1)) {
            //NSLog(@"weer");
            //NSLog(@"move unit with name %@ from %@ to (%d, %d) %d %d", unit.name, NSStringFromCGPoint (unit.unitPosition), bestY, bestX, (int) unit.unitPosition.y, (int) unit.unitPosition.x);
            //NSLog(@"%d", ((NSArray*)_level[(int) unit.unitPosition.y][(int) unit.unitPosition.x]).count);
            //remove the unit from where it was
            [_level[(int) unit.unitPosition.y][(int) unit.unitPosition.x] setObject:[NSNull null] atIndexedSubscript:2];
            //move the unit
            [[GameScene game] moveUnit:unit ToX:bestX Y:bestY with:_levelCostMap];
            //and add it to the position it moved to
            [_level[bestY][bestX] setObject:unit atIndexedSubscript:2];
        }
        
        //check if the unit should attack another unit
        if ([self shouldAttackWithUnit:unit]) {
            [self attackWithUnit:unit];
        }
    }
}

-(BOOL)shouldAttackWithUnit:(Unit*)unit {
    int y = unit.unitPosition.y - unit.getWeaponRange;
    if (y < 0) {
        y = 0;
    }
    int maxHeight = unit.unitPosition.y + unit.getWeaponRange;
    if (maxHeight >= _mapHeight) {
        maxHeight = _mapHeight - 1;
    }
    NSMutableArray *unitsToAttack = [NSMutableArray array];
    for (; y <= maxHeight; y++) {
        int rangeY = abs(y - unit.unitPosition.y);
        int x = unit.unitPosition.x - unit.getWeaponRange;
        if (x < 0) {
            x = 0;
        }
        int maxWidth = unit.unitPosition.x + unit.getWeaponRange;
        if (maxWidth >= _mapWidth) {
            maxWidth = _mapWidth - 1;
        }
        for (; x <= maxWidth; x++) {
            int rangeX = abs(x - unit.unitPosition.x);
            //target in in weapon range
            if ((rangeX + rangeY) <= unit.getWeaponRange) {
                if(_level[y][x][2] != [NSNull null]){
                    //NSLog(@"%d != %d ∑ %d, %d %@", ((Unit*)_level[y][x][2]).owner, _playerNumber, x,y,NSStringFromCGPoint (unit.unitPosition));
                    if(((Unit*)_level[y][x][2]).owner != _playerNumber){
                        [unitsToAttack addObject:(Unit*)_level[y][x][2]];
                        NSLog(@"ATTAK DA UNIT %@ %d %d %d %d",NSStringFromCGPoint (unit.unitPosition),x,y,_playerNumber,((Unit*)_level[y][x][2]).owner);
                    }
                }
            }
        }
    }
    for (Unit* posibleUnitToAttack in unitsToAttack) {
        //TODO calculate the best ratio between dmg inflicted and dmg done
        [Unit unit:unit attacksUnit:posibleUnitToAttack];
        return YES;
    }
    return NO;
}

-(void)attackWithUnit:(Unit*)unit {
    //Attack the highest rated target in range of the unit
}*/

@end
