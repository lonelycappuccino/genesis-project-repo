//
//  DesignRoomMenu_New.m
//  Genesis Project
//
//  Created by Klemen Kosir on 25. 09. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "DesignRoomMenu.h"

@implementation DesignRoomMenu_New {
    DesignRoomMenu_Builder *builder;
    SKSpriteNode *backButton;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [title setFontColor:[UIColor darkGrayColor]];
        [title setFontSize:25.0*[Functions deviceFactor]];
        [title setPosition:CGPointMake(self.size.width/2, self.size.height-40.0*[Functions deviceFactor])];
        [title setText:NSLocalizedString(@"Select Map size",nil)];
        [self addChild:title];
        
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-15.0)];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        
        NSArray *stringsModes = @[NSLocalizedString(@"SMALL",nil),NSLocalizedString(@"MEDIUM",nil),NSLocalizedString(@"LARGE",nil)];
        NSArray *stringsInfo = @[@"10x10",@"25x25",@"50x50"];
        for (int i = 0; i < 3; i++) {
            SKSpriteNode *button = [SKSpriteNode spriteNodeWithColor:[UIColor grayColor] size:CGSizeMake(125.0*factor, 125.0*factor)];
            [button setPosition:CGPointMake(self.size.width/2+(button.size.width+50.0)*(-1+i), self.size.height/2)];
            [button setName:stringsModes[i]];
            [self addChild:button];
            
            SKLabelNode *buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [buttonLabel setFontColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
            [buttonLabel setFontSize:22.5*factor];
            [buttonLabel setText:stringsModes[i]];
            [buttonLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [button addChild:buttonLabel];
            
            SKLabelNode *buttonLabel2 = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [buttonLabel2 setFontColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
            [buttonLabel2 setFontSize:12.5*factor];
            [buttonLabel2 setText:stringsInfo[i]];
            [buttonLabel2 setPosition:CGPointMake(0.0, -20.0*factor)];
            [buttonLabel2 setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [button addChild:buttonLabel2];
        }
        
        SKSpriteNode *button = [SKSpriteNode spriteNodeWithColor:[UIColor grayColor] size:CGSizeMake(125.0*factor, 50.0*factor)];
        [button setPosition:CGPointMake(self.size.width/2, self.size.height/5)];
        [button setName:@"CUSTOM"];
        [self addChild:button];
        
        SKLabelNode *buttonLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [buttonLabel setFontColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
        [buttonLabel setFontSize:22.5*factor];
        [buttonLabel setText:NSLocalizedString(@"CUSTOM",nil)];
        [buttonLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [button addChild:buttonLabel];
        builder = [DesignRoomMenu_Builder sceneWithSize:self.size];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([[self childNodeWithName:@"SMALL"]containsPoint:location]) {
        [self showLoadingScreen];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10000000), dispatch_get_main_queue(), ^{
            [builder setMapSize:CGSizeMake(10, 10)];
            [self.view presentScene:builder];
        });
    }
    else if ([[self childNodeWithName:@"MEDIUM"]containsPoint:location]) {
        [self showLoadingScreen];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10000000), dispatch_get_main_queue(), ^{
            [builder setMapSize:CGSizeMake(25, 25)];
            [self.view presentScene:builder];
        });
    }
    else if ([[self childNodeWithName:@"LARGE"]containsPoint:location]) {
        [self showLoadingScreen];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10000000), dispatch_get_main_queue(), ^{
            [builder setMapSize:CGSizeMake(50, 50)];
            [self.view presentScene:builder];
        });
    }
    else if ([[self childNodeWithName:@"CUSTOM"]containsPoint:location]) {
        DesignRoomMenu_NewCustom *custom = [DesignRoomMenu_NewCustom sceneWithSize:self.size];
        [custom setMainMenu:self];
        [self.view presentScene:custom transition:[SKTransition pushWithDirection:SKTransitionDirectionUp duration:0.5]];
    }
    else if ([backButton containsPoint:location]) {
        [self.view presentScene:_prevMenu transition:[SKTransition pushWithDirection:SKTransitionDirectionRight duration:0.7]];
    }
}

-(void)showLoadingScreen {
    [self removeAllChildren];
    
    SKSpriteNode *dim = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:self.size];
    [dim setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [self addChild:dim];
    [dim setZPosition:9999999.0];
    
    SKLabelNode *loadinglabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [loadinglabel setFontColor:[UIColor whiteColor]];
    [loadinglabel setFontSize:15.0*factor];
    [loadinglabel setPosition:CGPointMake(0.0, 75.0*factor)];
    [loadinglabel setText:@"LOADING"];
    [dim addChild:loadinglabel];
    /*
    SKLabelNode *loading = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [loading setFontColor:[UIColor whiteColor]];
    [loading setFontSize:35.0*factor];
    [loading setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [loading setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [dim addChild:loading];
    
    SKSpriteNode *loadingIcon = [SKSpriteNode spriteNodeWithImageNamed:@"loadingIcon"];
    [loadingIcon runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:-M_PI*2 duration:1.7]]];
    [dim addChild:loadingIcon];
     */
}

@end
