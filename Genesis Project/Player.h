//
//  Player.h
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Unit.h"
#import "Structure.h"

@interface Player : NSObject <NSCoding>

@property (nonatomic) int teamID;
@property (nonatomic, strong) NSMutableArray *friendlyUnits;
@property (nonatomic, strong) NSMutableArray *enemyUnits;
@property (nonatomic, strong) NSMutableArray *structures;
@property (nonatomic, strong) NSMutableArray *levelCostMap;
@property (nonatomic, strong) NSString *playerName;
@property (nonatomic, strong) NSArray *levelMap;
@property (nonatomic, strong) NSArray *level;
@property (nonatomic, strong) NSArray *extraDefence;
@property (nonatomic) int playerNumber;
@property (nonatomic) BOOL computer;
@property (nonatomic,strong) UIColor *playerColor;
@property (nonatomic) int money;
@property (nonatomic) int agressive;
@property (nonatomic) int defensive;
@property (nonatomic) int neutral;
@property (nonatomic) int shortGame;
@property (nonatomic) int longGame;
@property (nonatomic) int guerillaTactics;
@property (nonatomic) int humanWaveDoctrine;
@property (nonatomic) int moneyPerTurn;
@property (nonatomic, strong) SKLabelNode *moneyLabel;
@property (nonatomic) CGPoint cameraPosition;

-(void)addUnit:(Unit*)unit forPlayer:(int)playerNum;
-(void)addUnitArray;
-(void)addStructure:(Structure*)structure;
-(void)removeUnit:(Unit*)unit;
-(void)removeStructure:(Structure*)structure;
-(void)createLevelArrayWithLevelMap:(NSMutableArray*) levelMapArray;
-(void)updateFogMap;
-(void)updateLevelCostMapWith: (NSArray*) baseLevelCostMap;

/*
-(void)playForAI;
-(void)chooseAIPersonalityForDifficulty:(int) diff;*/
-(void)updateArrays;

-(NSArray*)findNodesInMovementRangeForUnit:(Unit*) unit;
+(void)setPlayers:(NSMutableArray*)playersArray;
+(Player*)getPlayer:(int)playerNumber;
+(Player*)addPlayer;

@end
