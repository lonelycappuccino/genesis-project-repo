//
//  CoopMenu.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "CoopMenu.h"

@implementation CoopMenu

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        titleLabel.fontColor = [UIColor darkGrayColor];
        titleLabel.fontSize = 35.0;
        titleLabel.text = @"Co-op";
        titleLabel.position = CGPointMake(self.size.width/2, self.size.height*3/4);
        [self addChild:titleLabel];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

-(void)update:(CFTimeInterval)currentTime {
    
}

@end
