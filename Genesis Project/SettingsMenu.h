//
//  SettingsMenu.h
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SettingsMenu : SKScene

@property (nonatomic,strong) SKScene *mainMenu;

@end
