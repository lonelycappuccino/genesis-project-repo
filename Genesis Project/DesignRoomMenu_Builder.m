//
//  DesignRoomMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 7. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "DesignRoomMenu.h"
#import "Functions.h"


@implementation DesignRoomMenu_Builder {
    SKNode *menu, *startMenu, *mapOverlay, *playersMenu;
    SKSpriteNode *menuTerrain, *menuStructures, *menuUnits, *map;
    SKCropNode  *paintMenu;
    CGRect menuTerrainRect, menuStructuresRect, menuUnitsRect;
    int currentMenu, mapSize, currentTileType;
    CGPoint twoFingerPanStartLocation, mapStartPosition;
    CGFloat initialScale,mapWidth,mapHeight;
    
    NSString *mapName;
    
    NSDictionary *gameDataDict;
    NSMutableArray *allMenuItems;
    
    SKSpriteNode *paintNode, *hqStructureNode;
    NSMutableArray *terrainArray, *structuresArray, *unitsArray;
    NSMutableArray *mapTiles;
    
    SKSpriteNode *menuButton, *addPlayerButton, *saveIcon, *newIcon, *exitIcon;
    
    CGFloat menuStartX, panStartXPosition;
    
    NSMutableArray *menusArray;
    SKSpriteNode *rightMenuUI;
    BOOL rightMenuVisible, saveMenuVisible, paintMode, terrainTypeMenuVisible,mapSizeMenuVisible, terrainTypeMenuExtended;
    SKSpriteNode *dimScreen, *selectedTileCard;
    
    SKSpriteNode *saveMenu;
    
    KeyboardView *keyboard;
    SKSpriteNode *selectedTile;
    
    SKNode *typeChooserNode;
    SKNode *lastTapped;
    int terrainType,numOfPlayers;
    /*
     TERRAIN TYPE
     1 - normal
     2 - desert
     3 - arctic
     4 - jungle
     5 - tropic
     6 - pollution
     
     */
    
    CGFloat startScale;
    CGPoint startPosition, gestureTargetPosition;
    
    int owner;
    NSArray *ownerColors;
    NSMutableArray *ownerColorNodes;
    
    int userMapWidth, userMapHeight;
    SKSpriteNode *mapSizeMenu;
    
    int playerOwnership[4][2];
    
    SKNode *terrainTypeMenu;
    NSMutableArray *terrainTypesArray;
    SKSpriteNode *circleSelectedType;
    SKSpriteNode *dim;
    SKLabelNode *loading;
    CGFloat loadingValue;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.5 alpha:1.0]];
        //[self setBackgroundColor:[UIColor whiteColor]];
        
        gameDataDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"GameData" ofType:@"plist"]];
        
        keyboard = [[KeyboardView alloc]init];
        //[self mapSizeMenu];
        
        /**** SET MAP SIZE HERE ****/
        //userMapWidth = 6;
        //userMapHeight = 3;
        /***************************/
        
        //[self createDesignRoom];
        numOfPlayers = 2;
    }
    return self;
}

-(void)setMapSize:(CGSize)size {
    /*
    mapSizeMenu = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(500.0, 250.0)];
    [mapSizeMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2+100.0)];
    [self addChild:mapSizeMenu];
    
    SKSpriteNode *widthNode = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:CGSizeMake(140.0, 70.0)];
    [widthNode setPosition:CGPointMake(-mapSizeMenu.size.width/5, 0.0)];
    [widthNode setName:@"widthNode"];
    [mapSizeMenu addChild:widthNode];
    
    SKSpriteNode *heightNode = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:CGSizeMake(140.0, 70.0)];
    [heightNode setPosition:CGPointMake(mapSizeMenu.size.width/5, 0.0)];
    [heightNode setName:@"heightNode"];
    [mapSizeMenu addChild:heightNode];
    
    SKLabelNode *widthLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [widthLabel setFontColor:[UIColor darkGrayColor]];
    [widthLabel setFontSize:35.0];
    [widthLabel setText:@"WIDTH"];
    [widthLabel setPosition:CGPointMake(widthNode.position.x, 45.0)];
    [mapSizeMenu addChild: widthLabel];
    
    SKLabelNode *heightLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [heightLabel setFontColor:[UIColor darkGrayColor]];
    [heightLabel setFontSize:35.0];
    [heightLabel setText:@"HEIGHT"];
    [heightLabel setPosition:CGPointMake(heightNode.position.x, 45.0)];
    [mapSizeMenu addChild: heightLabel];
    
    SKLabelNode *widthInputLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [widthInputLabel setFontColor:[UIColor whiteColor]];
    [widthInputLabel setFontSize:35.0];
    [widthInputLabel setPosition:widthNode.position];
    [widthInputLabel setName:@"widthInput"];
    [widthInputLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [mapSizeMenu addChild: widthInputLabel];
    
    SKLabelNode *heightInputLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [heightInputLabel setFontColor:[UIColor whiteColor]];
    [heightInputLabel setFontSize:35.0];
    [heightInputLabel setPosition:heightNode.position];
    [heightInputLabel setName:@"heightInput"];
    [heightInputLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [mapSizeMenu addChild: heightInputLabel];
    mapSizeMenuVisible = YES;
    
    SKSpriteNode *doneButtonNode = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.95 alpha:1.0] size:CGSizeMake(130.0, 50.0)];
    [doneButtonNode setPosition:CGPointMake(0.0, -80.0)];
    [doneButtonNode setName:@"doneButton"];
    [mapSizeMenu addChild:doneButtonNode];
    
    SKLabelNode *doneLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [doneLabel setFontColor:[UIColor greenColor]];
    [doneLabel setFontSize:35.0];
    [doneLabel setText:@"DONE"];
    [doneLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [doneButtonNode addChild: doneLabel];
     */
    // +++ FOR TESTING +++ //;
    userMapHeight = (int)size.height;
    userMapWidth = (int)size.width;
    /*
    SKView *loadingView = [[SKView alloc]initWithFrame:self.frame];
    [loadingView setBackgroundColor:[UIColor redColor]];
    SKScene *loadingScene = [SKScene sceneWithSize:self.size];
    [loadingView presentScene:loadingScene];
    */
    /*
    dim = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:self.size];
    [dim setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [self addChild:dim];
    [dim setZPosition:9999999.0];
    
    SKLabelNode *loadinglabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [loadinglabel setFontColor:[UIColor whiteColor]];
    [loadinglabel setFontSize:15.0*factor];
    [loadinglabel setPosition:CGPointMake(0.0, 75.0*factor)];
    [loadinglabel setText:@"LOADING"];
    [dim addChild:loadinglabel];
    
    loading = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [loading setFontColor:[UIColor whiteColor]];
    [loading setFontSize:35.0*factor];
    [loading setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [loading setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [dim addChild:loading];
    
    SKSpriteNode *loadingIcon = [SKSpriteNode spriteNodeWithImageNamed:@"loadingIcon"];
    [loadingIcon runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:-M_PI*2 duration:1.7]]];
    [dim addChild:loadingIcon];
    
    NSInvocationOperation *invOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(createDesignRoom) object:nil];
    NSOperationQueue *opQueue = [[NSOperationQueue alloc]init];
    //dispatch_queue_t dispatchQueue = dispatch_queue_create("com.lonelycappuccino.customQueue", DISPATCH_QUEUE_SERIAL);
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    [opQueue setUnderlyingQueue:dispatchQueue];
    [opQueue addOperation:invOperation];
    */
    /*
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self createDesignRoom];
    });
     */
    /*
    NSThread *thread1 = [[NSThread alloc]initWithTarget:self selector:@selector(createDesignRoom) object:nil];
    [thread1 setThreadPriority:1.0];
    [thread1 start];
    */
    /*
    NSThread *thread2 = [[NSThread alloc]initWithTarget:self selector:@selector(createPaintMenu) object:nil];
    [thread2 setThreadPriority:0.5];
    [thread2 start];
    
    NSThread *thread3 = [[NSThread alloc]initWithTarget:self selector:@selector(createUI) object:nil];
    [thread3 setThreadPriority:0.5];
    [thread3 start];
     */
    
    [self createDesignRoom];
}

-(void)createMap {
    
}

-(void)createDesignRoom {
    //NSLog(@"map size: %d x %d",userMapWidth,userMapHeight);
    map = [SKSpriteNode node];
    mapOverlay = [SKNode node];
    [mapOverlay setName:@"overlay"];
    [self addChild:map];
    //[map addChild:mapOverlay];
    owner = 0;
    ownerColors = @[[UIColor whiteColor], [UIColor redColor], [UIColor blueColor], [UIColor greenColor], [UIColor yellowColor]];
    
    mapTiles = [NSMutableArray array];
    NSMutableArray *items = [gameDataDict objectForKey:@"Terrain"];
    NSMutableDictionary *item = items[6];
    NSMutableDictionary *itemTmp = [NSMutableDictionary dictionaryWithDictionary:item];
    [itemTmp setObject:[NSNumber numberWithInteger:[items indexOfObject:item]] forKey:@"itemNumber"];
    loadingValue+=5;
    CGFloat sqrtValue = sqrt(3)/2*64.0*[Functions deviceFactor];
    
    for (int i = 0; i < userMapHeight; i++) {
        NSMutableArray *mapTilesRow = [NSMutableArray array];
        for (int j = 0; j < userMapWidth; j++) {
            SKNode *tile = [SKNode node];
            [tile setName:@"Tile"];
            SKSpriteNode *terrain = [SKSpriteNode spriteNodeWithImageNamed:@"Sea"];
            [terrain setName:@"Terrain"];
            [terrain setUserData:itemTmp];
            SKSpriteNode *tileBorder = [SKSpriteNode spriteNodeWithImageNamed:@"grid"];
            [tileBorder setColor:[UIColor blackColor]];
            [tileBorder setColorBlendFactor:1.0];
            tile.position = CGPointMake(32.0*factor+(sqrtValue)*j+(i%2 != 0 ? 0 : -(sqrtValue)/2), 32.0*factor+3.0/4.0*64.0*factor*i);
            tileBorder.position = tile.position;
            [tile addChild:terrain];
            [map addChild:tile];
            //[mapOverlay addChild:tileBorder];
            [terrain setZPosition:0.0];
            [tileBorder setZPosition:10.0];
            [tileBorder setScale:0.97];
            [tileBorder setAlpha:0.08];
            //terrainArray[i][j] = terrain;
            [mapTilesRow addObject:tile];
        }
        [mapTiles addObject:mapTilesRow];
        loadingValue += (80.0/userMapHeight);
    }
    NSLog(@"LOADING VALUE: %d",(int)loadingValue);
    //NSLog(@"MAP SIZE: %ld x %ld",mapTiles.count, ((NSArray*)mapTiles[0]).count);
    
    [self createPaintMenu];
    
    [mapOverlay setZPosition:999.0];
    [map addChild:mapOverlay];
    loadingValue+=5;
    /*
    SKEffectNode *effectNode = [SKEffectNode node];
    [effectNode addChild:mapOverlay];
    [map addChild:effectNode];
    
    CGFloat alpha = 0.15;
    CGFloat rgba[4] = {0.0, 0.0, 0.0, alpha};
    CIFilter *colorMatrix = [CIFilter filterWithName:@"CIColorMatrix"];
    [colorMatrix setDefaults];
    [colorMatrix setValue:[CIVector vectorWithValues:rgba count:4] forKey:@"inputAVector"];
    
    [effectNode setFilter:colorMatrix];
    */
    [mapOverlay setAlpha:0.15];
    
    mapWidth = map.calculateAccumulatedFrame.size.width;
    //NSLog(@"%f",mapWidth);
    mapHeight = map.calculateAccumulatedFrame.size.height;
    //mapName = @"CUSTOM LEVEL 1";
    
    [self createUI];
    loadingValue+=5;
    
    //PRINT MAP ARRAY
    /*
    NSLog(@"MAP ARRAY");
    for (NSArray *row in mapTiles) {
        for (SKNode *tile in row) {
            printf("%d ",[[[tile childNodeWithName:@"Terrain"].userData objectForKey:@"itemNumber"]intValue]);
        }
        printf("\n");
    }
     */
    [dim removeFromParent];
    NSLog(@"DONE LOADING");
}

-(void)createPaintMenu {
    paintMenu = [SKCropNode node];
    SKSpriteNode *paintMenuBg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.8 alpha:1.0] size:CGSizeMake(self.size.width-45.0*factor, self.size.height)];
    [paintMenuBg setAnchorPoint:CGPointMake(0.0, 0.0)];
    [paintMenu setPosition:CGPointMake(-self.size.width, 0.0)];
    menusArray = [NSMutableArray array];
    [paintMenu addChild:paintMenuBg];
    [paintMenu setMaskNode:paintMenuBg.copy];
    
    currentMenu = 0;
    NSArray *type = @[@"Terrain", @"Structures", @"Units"];
    for (int i = 0; i < 3; i++) {
        SKSpriteNode *typeButton = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(100.0*factor, 35.0*factor)];
        [typeButton setZPosition:1.0];
        [typeButton setPosition:CGPointMake(paintMenuBg.size.width/2-typeButton.size.width-15.0*factor+(typeButton.size.width+15.0*factor)*i, paintMenuBg.size.height-25.0*factor)];
        [typeButton setName:type[i]];
        [paintMenu addChild:typeButton];
        
        SKLabelNode *buttonLabel = [SKLabelNode labelNodeWithText:type[i]];
        [buttonLabel setFontColor:[UIColor whiteColor]];
        [buttonLabel setFontSize:20.0*factor];
        [buttonLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [typeButton addChild:buttonLabel];
        
        SKNode *menuNode = [SKNode node];
        [menuNode setZPosition:1.0];
        menuNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(100.0, 100.0)];
        menuNode.physicsBody.affectedByGravity = NO;
        menuNode.physicsBody.allowsRotation = NO;
        menuNode.physicsBody.linearDamping = 7.0;
        menuNode.physicsBody.collisionBitMask = 0;
        menuNode.physicsBody.contactTestBitMask = 0;
        menuNode.physicsBody.mass = 1.0;
        
        [self loadMenuTilesOnNode:menuNode ofType:type[i]];
        if (i != currentMenu) {
            [menuNode setHidden:YES];
        }
        [menusArray addObject:menuNode];
        [paintMenu addChild:menuNode];
        
    }
    
    SKSpriteNode *exitButton = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
    [exitButton setPosition:CGPointMake(paintMenuBg.size.width-25.0*factor, paintMenuBg.size.height-25.0*factor)];
    [exitButton setName:@"exit"];
    [paintMenu addChild:exitButton];
    
    [paintMenu setZPosition:9999999.0];
    [paintMenu setHidden:YES];
    [self addChild:paintMenu];
    
    loadingValue += 5;
}

-(void)editLevel:(NSDictionary*)levelDict {
    //NSLog(@"dict: %@",levelDict);
    map = [SKSpriteNode node];
    mapOverlay = [SKNode node];
    [mapOverlay setName:@"overlay"];
    [self addChild:map];
    [map addChild:mapOverlay];
    
    numOfPlayers = [[levelDict objectForKey:@"Players"]intValue];
    
    owner = 0;
    ownerColors = @[[UIColor whiteColor], [UIColor redColor], [UIColor blueColor], [UIColor greenColor], [UIColor yellowColor]];
    
    mapTiles = [NSMutableArray array];
    NSMutableArray *items = [gameDataDict objectForKey:@"Terrain"];
    NSMutableDictionary *item = items[6];
    NSMutableDictionary *itemTmp = [NSMutableDictionary dictionaryWithDictionary:item];
    [itemTmp setObject:[NSNumber numberWithInteger:[items indexOfObject:item]] forKey:@"itemNumber"];
    
    terrainArray = [gameDataDict objectForKey:@"Terrain"];
    structuresArray = [gameDataDict objectForKey:@"Structures"];
    unitsArray = [gameDataDict objectForKey:@"Units"];
    
    NSArray *levelData = [levelDict objectForKey:@"LevelMap"];
    //NSLog(@"levelData: %@",levelData);
    userMapWidth = (int)[[levelData firstObject]count];
    userMapHeight = (int)[levelData count];
    int coll = 0, row = 0;
    for (NSArray *levelDataRow in levelData) {
        //NSLog(@"---- START ROW ----");
        NSMutableArray *levelTileNodesRow = [NSMutableArray array];
        [mapTiles addObject:levelTileNodesRow];
        for (NSArray *values in levelDataRow) {
            SKNode *tile = [SKNode node];
            [tile setName:@"Tile"];
            tile.position = CGPointMake(64.0+128.0*coll, 64.0+128.0*row);
            int numOfValues = (int)values.count;
            
            if (numOfValues >= 1) {
                NSString *name = [terrainArray[((NSNumber*)values[0]).intValue] objectForKey:@"Name"];
                //NSLog(@"NAME: %@",name);
                SKSpriteNode *node;
                if ([name isEqualToString:@"Plain"]) {
                    node = [SKSpriteNode spriteNodeWithImageNamed:@"terrain_normal_plain"];
                }
                else {
                    node = [SKSpriteNode spriteNodeWithImageNamed:name];
                }
                [node setName:@"Terrain"];
                NSMutableDictionary *nodeDict = [NSMutableDictionary dictionaryWithDictionary:terrainArray[((NSNumber*)values[0]).intValue]];
                [nodeDict setObject:values[0] forKey:@"itemNumber"];
                [node setUserData:nodeDict];
                [node setZPosition:0.0];
                [tile addChild:node];
            }
            
            if (numOfValues >= 2 && ((NSArray*)values[1]).count > 0) {
                //NSLog(@"LEVEL HAS STRUCTURES");
                NSString *name = [structuresArray[((NSNumber*)values[1][0]).intValue] objectForKey:@"Texture"];
                SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed:name];

                [node setName:@"Structures"];
                NSMutableDictionary *nodeDict = [NSMutableDictionary dictionaryWithDictionary:structuresArray[((NSNumber*)values[1][0]).intValue]];
                [nodeDict setObject:values[1][0] forKey:@"itemNumber"];
                [nodeDict setObject:values[1][1] forKey:@"Owner"];
                [node setUserData:nodeDict];
                [node setZPosition:0.1];
                [node runAction:[SKAction colorizeWithColor:ownerColors[((NSNumber*)values[1][1]).intValue+1] colorBlendFactor:1.0 duration:0.0]];
                [tile addChild:node];
            }
            if (numOfValues == 3) {
                //NSLog(@"LEVEL HAS UNITS");
                NSString *name = [unitsArray[((NSNumber*)values[2][0]).intValue] objectForKey:@"Name"];
                SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed:name];
                
                [node setName:@"Units"];
                NSMutableDictionary *nodeDict = [NSMutableDictionary dictionaryWithDictionary:unitsArray[((NSNumber*)values[2][0]).intValue]];
                [nodeDict setObject:values[2][0] forKey:@"itemNumber"];
                [nodeDict setObject:values[2][1] forKey:@"Owner"];
                [node setUserData:nodeDict];
                [node setZPosition:0.2];
                [node runAction:[SKAction colorizeWithColor:ownerColors[((NSNumber*)values[2][1]).intValue+1] colorBlendFactor:1.0 duration:0.0]];
                [tile addChild:node];
            }
            NSMutableDictionary *tileDict = [NSMutableDictionary dictionaryWithObject:NSStringFromCGPoint(CGPointMake(coll, row)) forKey:@"tilePosition"];
            //NSLog(@"DICT: %@",tileDict);
            [tile setUserData:tileDict];
            [tile setZPosition:levelData.count-1-row];
            coll++;
            [map addChild:tile];
            [levelTileNodesRow addObject:tile];
            //add tileBorder
            SKSpriteNode *tileBorder = [SKSpriteNode spriteNodeWithImageNamed:@"tileBorder"];
            tileBorder.position = tile.position;
            tileBorder.zPosition = 9999.0;
            [mapOverlay addChild:tileBorder];
        }
        row++;
        coll = 0;
        //[levelTileNodes addObject:levelTileNodesRow];
    }
    [self createPaintMenu];
    
    mapWidth = map.calculateAccumulatedFrame.size.width;
    //NSLog(@"%f",mapWidth);
    mapHeight = map.calculateAccumulatedFrame.size.height;
    mapName = [levelDict objectForKey:@"LevelName"];
    
    [self createUI];
}

-(void)createUI {
    dimScreen = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:self.size];
    [dimScreen setAlpha:0.4];
    [dimScreen setName:@"dim"];
    [dimScreen setZPosition:99999.0];
    [dimScreen setAnchorPoint:CGPointZero];
    //[self addChild:dimScreen];
    
    saveMenu = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.95 alpha:0.95] size:CGSizeMake(300.0*factor, 200.0*factor)];
    saveMenu.position = CGPointMake(self.size.width/2, self.size.height*1.5);
    [saveMenu setZPosition:999999.0];
    [self addChild:saveMenu];
    [saveMenu setHidden:YES];
    
    SKLabelNode *saveTitle = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [saveTitle setText:NSLocalizedString(@"SAVE MAP",nil)];
    [saveTitle setFontSize:35.0*factor];
    [saveTitle setFontColor:[UIColor blackColor]];
    [saveTitle setPosition:CGPointMake(0.0, saveMenu.size.height/2-50.0*factor)];
    [saveMenu addChild:saveTitle];
    
    SKSpriteNode *textBackground = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(225.0*factor, 40.0*factor)];
    [textBackground setName:@"textBackground"];
    SKSpriteNode *textBackgroundLight = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(224.0*factor, 39.0*factor)];
    [textBackground addChild:textBackgroundLight];
    [saveMenu addChild:textBackground];
    
    SKLabelNode *text = [SKLabelNode node];
    [text setFontColor:[UIColor blackColor]];
    [text setFontSize:30.0*factor];
    [text setName:@"text"];
    [text setVerticalAlignmentMode:SKLabelVerticalAlignmentModeBaseline];
    [text setPosition:CGPointMake(0.0, -text.fontSize/2.0)];
    [saveMenu addChild:text];
    
    SKLabelNode *cancelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [cancelLabel setText:NSLocalizedString(@"CANCEL",nil)];
    [cancelLabel setFontSize:30.0*factor];
    [cancelLabel setFontColor:[UIColor redColor]];
    [cancelLabel setPosition:CGPointMake(-saveMenu.size.height/3, -saveMenu.size.height/3)];
    [cancelLabel setName:@"cancelLabel"];
    [saveMenu addChild:cancelLabel];
    
    SKLabelNode *saveLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [saveLabel setText:NSLocalizedString(@"SAVE",nil)];
    [saveLabel setFontSize:30.0*factor];
    [saveLabel setFontColor:[UIColor greenColor]];
    [saveLabel setPosition:CGPointMake(saveMenu.size.height/3, -saveMenu.size.height/3)];
    [saveLabel setName:@"saveLabel"];
    [saveMenu addChild:saveLabel];
    
    selectedTileCard = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(65.0*factor, 65.0*factor)];
    [selectedTileCard setPosition:CGPointMake(selectedTileCard.size.width/2+10.0, selectedTileCard.size.height/2+10.0)];
    [selectedTileCard setZPosition:9999.0];
    [selectedTileCard setName:@"selectedItem"];
    [self addChild:selectedTileCard];
    
    selectedTile = [SKSpriteNode node];
    [selectedTile setSize:CGSizeMake(85.0, 85.0)];
    [selectedTileCard addChild:selectedTile];
    
    SKSpriteNode *tool = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
    [tool setPosition:CGPointMake(selectedTileCard.position.x+selectedTileCard.size.width/2+tool.size.width/2+5.0, tool.size.height/2+10.0)];
    [tool setName:@"tool"];
    [tool setZPosition:9999.0];
    [tool setAlpha:1.0];
    [self addChild:tool];
    
    SKSpriteNode *toolSec = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(10.0*factor, 10.0*factor)];
    [toolSec setPosition:CGPointMake(tool.size.width/2-4.0*factor, -tool.size.height/2+4.0*factor)];
    [toolSec setZPosition:1.0];
    [toolSec setAlpha:1.0];
    [tool addChild:toolSec];
    
    
    ownerColorNodes = [NSMutableArray array];
    playersMenu = [SKNode node];
    for (int i = 0; i < 5; i++) {
        SKSpriteNode *ownerColorNode = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [ownerColorNode setColor:ownerColors[i]];
        [ownerColorNode setColorBlendFactor:1.0];
        [ownerColorNode setPosition:CGPointMake(self.size.width-ownerColorNode.size.width/2-10.0, 10.0+ownerColorNode.size.height/2+(10.0+ownerColorNode.size.height)*i)];
        [ownerColorNode setZPosition:99999.0];
        [ownerColorNode setName:@(i).stringValue];
        [playersMenu addChild:ownerColorNode];
        [ownerColorNodes addObject:ownerColorNode];
        if (i == numOfPlayers+1 && numOfPlayers != 4) {
            addPlayerButton = [SKSpriteNode spriteNodeWithImageNamed:@"addPlayerButton"];
            [addPlayerButton setColorBlendFactor:0.8];
            [addPlayerButton setColor:[UIColor greenColor]];
            [addPlayerButton setAnchorPoint:CGPointMake(0.5, 0.9)];
            [addPlayerButton setPosition:ownerColorNode.position];
            [addPlayerButton setZPosition:99999.0];
            [playersMenu addChild:addPlayerButton];
        }
        if (i > numOfPlayers) {
            [ownerColorNode setHidden:YES];
        }
        if (i > 0) {
            [ownerColorNode setAlpha:0.4];
        }
    }
    [self addChild:playersMenu];
    
    NSArray *types = @[NSLocalizedString(@"Normal",nil),NSLocalizedString(@"Desert",nil),NSLocalizedString(@"Arctic",nil),NSLocalizedString(@"Tropic",nil),NSLocalizedString(@"Jungle",nil),NSLocalizedString(@"Pollution",nil)];
    NSArray *typeColors = @[[UIColor colorWithRed:0.3 green:0.4 blue:1.0 alpha:1.0],[UIColor yellowColor],[UIColor whiteColor],[UIColor orangeColor],[UIColor colorWithRed:0.0 green:0.55 blue:0.0 alpha:1.0],[UIColor purpleColor]];
    
    terrainTypeMenu = [SKNode node];
    terrainTypesArray = [NSMutableArray array];
    for (int i = 0; i < types.count; i++) {
        SKSpriteNode *circleType = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [circleType setZPosition:99999.0+(types.count-i)];
        //[circleType setPosition:CGPointMake(0.0, 75.0*i)];
        [circleType setHidden:YES];
        [circleType setScale:0.85];
        [circleType setColorBlendFactor:1.0];
        [circleType setColor:typeColors[i]];
        //[circleType setName:types[i]];
        
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [label setFontColor:[UIColor blackColor]];
        [label setFontSize:9.0*factor];
        [label setText:types[i]];
        [label setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [label setZPosition:1.0];
        [circleType addChild:label];
        [terrainTypeMenu addChild:circleType];
        [terrainTypesArray addObject:circleType];
    }
    
    circleSelectedType = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
    [circleSelectedType setZPosition:999999.0];
    [circleSelectedType setScale:0.85];
    [circleSelectedType setColorBlendFactor:1.0];
    [circleSelectedType setColor:typeColors[0]];
    //[circleSelectedType setName:types[0]];
    
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [label setFontColor:[UIColor blackColor]];
    [label setFontSize:9.0*factor];
    [label setText:types[0]];
    [label setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [label setZPosition:1.0];
    [circleSelectedType addChild:label];
    [terrainTypeMenu addChild:circleSelectedType];
    terrainType = 0;
    
    [terrainTypeMenu setPosition:CGPointMake(25.0*factor, 95.0*factor)];
    [self addChild:terrainTypeMenu];
    
    /*
    SKSpriteNode *terrainTypeMenu = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.95 alpha:0.95] size:CGSizeMake(700.0, 500.0)];
    [terrainTypeMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [terrainTypeMenu setZPosition:9999999.0];
    [self addChild:terrainTypeMenu];
    
    SKLabelNode *terrainTypeLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [terrainTypeLabel setText:@"TERRAIN TYPE"];
    [terrainTypeLabel setFontSize:60.0];
    [terrainTypeLabel setFontColor:[UIColor darkGrayColor]];
    [terrainTypeLabel setPosition:CGPointMake(0.0, terrainTypeMenu.size.height/3)];
    [terrainTypeMenu addChild:terrainTypeLabel];
    
    NSArray *types = @[@"Normal",@"Desert",@"Arctic",@"Tropic",@"Jungle",@"Pollution"];
    
    typeChooserNode = [SKNode node];
    for (int i = 0; i < types.count; i++) {
        SKSpriteNode *node = [SKSpriteNode spriteNodeWithColor:[UIColor grayColor] size:CGSizeMake(150.0, 150.0)];
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [label setText:types[i]];
        [label setFontColor:[UIColor whiteColor]];
        [node addChild:label];
        [node setName:types[i]];
        [node setUserData:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInteger:i] forKey:@"number"]];
        [node setPosition:CGPointMake((i < types.count/2 ? i : i-types.count/2)*195.0, (i < types.count/2 ? 170 : 0.0))];
        [typeChooserNode addChild:node];
    }
    [typeChooserNode setPosition:CGPointMake(-typeChooserNode.calculateAccumulatedFrame.size.width/2+75.0, -typeChooserNode.calculateAccumulatedFrame.size.height/2+40.0)];
    [terrainTypeMenu addChild:typeChooserNode];
    terrainTypeMenuVisible = YES;
    
    SKSpriteNode *backButton = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.95 alpha:0.95] size:CGSizeMake(150.0, 60.0)];
    [backButton setAnchorPoint:CGPointMake(0.5, 1.0)];
    [backButton setPosition:CGPointMake(80.0, self.size.height)];
    [backButton setZPosition:9999999.0];
    [backButton setName:@"backButton"];
    [self addChild:backButton];
    
    SKLabelNode *backLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [backLabel setText:@"BACK"];
    [backLabel setPosition:CGPointMake(0.0, -backButton.size.height/2)];
    [backLabel setFontColor:[UIColor darkGrayColor]];
    [backLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter],
    [backButton addChild:backLabel];
    */
    SKSpriteNode *circle1 = [SKSpriteNode spriteNodeWithImageNamed:@"pauseCircle_small"];
    [circle1 setPosition:CGPointMake(self.size.width-circle1.size.width/2-10.0, self.size.height-circle1.size.height/2-10.0)];
    [circle1 setName:@"circle1"];
    [circle1 setZPosition:99999.0];
    [circle1 setScale:1.2];
    [self addChild:circle1];
    
    SKSpriteNode *circle2 = [SKSpriteNode spriteNodeWithImageNamed:@"pauseCircle_large"];
    [circle2 setPosition:CGPointMake(self.size.width-circle1.size.width/2-10.0, self.size.height-circle1.size.height/2-10.0)];
    [circle2 setName:@"circle2"];
    [circle2 setScale:circle1.size.width/circle2.size.width];
    [circle2 setAlpha:0.0];
    [circle2 setZPosition:99999.0];
    [self addChild:circle2];
    
    SKSpriteNode *pauseIcon = [SKSpriteNode spriteNodeWithImageNamed:@"pauseIcon"];
    [pauseIcon setPosition:circle1.position];
    [pauseIcon setScale:1.0];
    [pauseIcon setZPosition:99999.1];
    [self addChild:pauseIcon];
    
    saveIcon = [SKSpriteNode spriteNodeWithImageNamed:@"saveIcon"];
    [saveIcon setName:@"saveButton"];
    [saveIcon setPosition:CGPointMake(-80.0*factor, -5.0*factor)];
    [saveIcon setZPosition:99999.1];
    [circle2 addChild:saveIcon];
    
    newIcon = [SKSpriteNode spriteNodeWithImageNamed:@"newButton"];
    [newIcon setName:@"newMapButton"];
    [newIcon setSize:saveIcon.size];
    [newIcon setPosition:CGPointMake(-55.0*factor, -55.0*factor)];
    [newIcon setZPosition:99999.1];
    [circle2 addChild:newIcon];
    
    exitIcon = [SKSpriteNode spriteNodeWithImageNamed:@"cancelButton"];
    [exitIcon setName:@"cancelMapButton"];
    [exitIcon setPosition:CGPointMake(-5.0*factor, -80.0*factor)];
    [exitIcon setSize:saveIcon.size];
    [exitIcon setZPosition:99999.1];
    [circle2 addChild:exitIcon];
    
    [saveIcon setScale:0.0];
    [newIcon setScale:0.0];
    [exitIcon setScale:0.0];
    
    loadingValue += 5;
}

-(void)loadMenuTilesOnNode:(SKNode*)node ofType:(NSString*)type {
    NSArray *items = [gameDataDict objectForKey:type];
    if ([type isEqualToString:@"Units"]) {
        NSMutableArray *tmpItems = [NSMutableArray arrayWithArray:items[0]];
        [tmpItems addObjectsFromArray:items[1]];
        [tmpItems addObjectsFromArray:items[2]];
        items = tmpItems;
    }
    int counter = 1;
    CGFloat xPos = 95.0*factor, yPos = self.size.height/2+70.0*factor;
    for (NSMutableDictionary *item in items) {
        NSString *name = [item objectForKey:@"Texture"];
        if (!name) {
            name = [item objectForKey:@"Name"];
        }
        SKSpriteNode *itemNode = [SKSpriteNode spriteNodeWithImageNamed:name];
        if ([name isEqualToString:@"HQ"]) {
            hqStructureNode = itemNode;
        }
        [itemNode setSize:CGSizeMake(100.0*factor, 100.0*factor)];
        if (counter % 2 == 0) {
            [itemNode setPosition:CGPointMake(xPos, yPos-140.0*factor)];
            xPos += 125.0*factor;
        }
        else {
            [itemNode setPosition:CGPointMake(xPos, yPos)];
        }
        [itemNode setZPosition:1.0];
        NSMutableDictionary *itemTmp = [NSMutableDictionary dictionaryWithDictionary:item];
        [itemTmp setObject:[NSNumber numberWithInteger:[items indexOfObject:item]] forKey:@"itemNumber"];
        [itemNode setUserData:itemTmp];
        
        SKLabelNode *itemName = [SKLabelNode labelNodeWithText:[item objectForKey:@"Name"]];
        [itemName setFontColor:[UIColor blackColor]];
        [itemName setFontSize:20.0*factor];
        [itemName setPosition:CGPointMake(0.0, -itemNode.size.height/2-20.0*factor)];
        [itemNode addChild:itemName];
        
        [node addChild:itemNode];
        counter++;
    }
}
/*
-(void)loadMenuElementsOnNode:(SKSpriteNode*)node ofType:(NSString*)type {
    NSArray *items = [gameDataDict objectForKey:type];
    if ([type isEqualToString:@"Units"]) {
        NSMutableArray *tmpItems = [NSMutableArray arrayWithArray:items[0]];
        [tmpItems addObjectsFromArray:items[1]];
        [tmpItems addObjectsFromArray:items[2]];
        items = tmpItems;
    }
    CGFloat midX = node.size.width/2;
    CGFloat nodeHeight = node.size.height-50.0;
    int counter = 0;
    SKNode *menuHolder = [SKNode node];
    SKCropNode *cropNode = [SKCropNode node];
    SKSpriteNode *mask = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(80.0, node.size.height-10.0)];
    [mask setAnchorPoint:CGPointMake(0.0, 0.0)];
    [mask setPosition:CGPointMake(10.0, 5.0)];
    NSMutableArray *itemsArray = [NSMutableArray array];
    for (NSMutableDictionary *item in items) {
        NSString *name = [item objectForKey:@"Texture"];
        if (!name) {
            name = [item objectForKey:@"Name"];
        }
        //NSLog(@"ITEM: %@ - %ld",name,[items indexOfObject:item]);
        SKSpriteNode *itemNode = [SKSpriteNode spriteNodeWithImageNamed:name];
        [itemNode setSize:CGSizeMake(70.0, 70.0)];
        if ([type isEqualToString:@"Structures"] || [type isEqualToString:@"Units"]) {
            [itemNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
        }
        itemNode.position = CGPointMake(midX, nodeHeight-80.0*counter);
        NSMutableDictionary *itemTmp = [NSMutableDictionary dictionaryWithDictionary:item];
        [itemTmp setObject:[NSNumber numberWithInteger:[items indexOfObject:item]] forKey:@"itemNumber"];
        [itemNode setUserData:itemTmp];
        [menuHolder addChild:itemNode];
        [itemsArray addObject:itemNode];
        counter++;
    }
    [cropNode setMaskNode:mask];
    [cropNode addChild:menuHolder];
    [node addChild:cropNode];
    [allMenuItems addObject:itemsArray];
}
*/

-(void)didMoveToView:(SKView *)view {
    
    for (UIGestureRecognizer *gestureRecognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:gestureRecognizer];
    }
    
    [view addSubview:keyboard];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [tap setDelegate:self];
    [view addGestureRecognizer:tap];
    
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panHandler:)];
    [pan setDelegate:self];
    [pan setMaximumNumberOfTouches:1];
    //[pan requireGestureRecognizerToFail:tap];
    [view addGestureRecognizer:pan];
    
    UIPanGestureRecognizer *pan2 = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panTwoFingerHandler:)];
    [pan2 setDelegate:self];
    [pan2 setMaximumNumberOfTouches:2];
    [pan2 setMinimumNumberOfTouches:2];
    [view addGestureRecognizer:pan2];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchHandler:)];
    //[view addGestureRecognizer:pinch];
    
    UIScreenEdgePanGestureRecognizer *edgeSwipeInRight = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(edgeSwipeInRightHandler:)];
    [edgeSwipeInRight setDelegate:self];
    [edgeSwipeInRight setEdges:UIRectEdgeRight];
    [view addGestureRecognizer:edgeSwipeInRight];
    
    UIScreenEdgePanGestureRecognizer *edgeSwipeInLeft = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(edgeSwipeInLeftHandler:)];
    [edgeSwipeInLeft setDelegate:self];
    [edgeSwipeInLeft setEdges:UIRectEdgeLeft];
    //[view addGestureRecognizer:edgeSwipeInLeft];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeGesture setNumberOfTouchesRequired:1];
    [swipeGesture setDelegate:self];
    //[view addGestureRecognizer:swipeGesture];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressHandler:)];
    [longPress setNumberOfTapsRequired:0];
    [longPress setNumberOfTouchesRequired:1];
    [longPress setDelegate:self];
    [longPress setMinimumPressDuration:0.7];
    [view addGestureRecognizer:longPress];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])) {
        return YES;
    }
    return NO;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) ||
        ([otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]] && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])) {
        return YES;
    }
    else if (([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) ||
        ([otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] && [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])) {
        return YES;
    }
    else if (([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) ||
             ([otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])) {
        return YES;
    }
    return NO;
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (mapSizeMenuVisible) {
        if ([[mapSizeMenu childNodeWithName:@"widthNode"]containsPoint:[mapSizeMenu convertPoint:location fromNode:self]]) {
            if (![keyboard isFirstResponder]) {
                NSLog(@"become first responder");
                [keyboard becomeFirstResponder];
            }
            else if ([lastTapped isEqual:[mapSizeMenu childNodeWithName:@"widthNode"]]) {
                [keyboard resignFirstResponder];
            }
            [keyboard setTextlabel:((SKLabelNode*)[mapSizeMenu childNodeWithName:@"widthInput"])];
            lastTapped = [mapSizeMenu childNodeWithName:@"widthNode"];
        }
        else if ([[mapSizeMenu childNodeWithName:@"heightNode"]containsPoint:[mapSizeMenu convertPoint:location fromNode:self]]) {
            if (![keyboard isFirstResponder]) {
                NSLog(@"become first responder");
                [keyboard becomeFirstResponder];
            }
            else if ([lastTapped isEqual:[mapSizeMenu childNodeWithName:@"heightNode"]]) {
                [keyboard resignFirstResponder];
            }
            [keyboard setTextlabel:((SKLabelNode*)[mapSizeMenu childNodeWithName:@"heightInput"])];
            lastTapped = [mapSizeMenu childNodeWithName:@"heightNode"];
        }
        else if ([[mapSizeMenu childNodeWithName:@"doneButton"]containsPoint:[mapSizeMenu convertPoint:location fromNode:self]]) {
            userMapWidth = ((SKLabelNode*)[mapSizeMenu childNodeWithName:@"widthInput"]).text.intValue;
            userMapHeight = ((SKLabelNode*)[mapSizeMenu childNodeWithName:@"heightInput"]).text.intValue;
            if (keyboard.isFirstResponder) {
                [keyboard resignFirstResponder];
            }
            [self createDesignRoom];
            [mapSizeMenu runAction:[SKAction fadeOutWithDuration:0.25] completion:^{
                [mapSizeMenu removeFromParent];
                mapSizeMenuVisible = NO;
            }];
        }
        return;
    }
    
    if (terrainTypeMenuVisible) {
        location = [typeChooserNode convertPoint:location fromNode:self];
        for (SKNode *node in typeChooserNode.children) {
            if ([node containsPoint:location]) {
                terrainType = [[node.userData objectForKey:@"number"]intValue];
                [typeChooserNode.parent runAction:[SKAction moveByX:0.0 y:-800.0 duration:0.25]];
                [[self childNodeWithName:@"backButton"]runAction:[SKAction moveByX:0.0 y:80.0 duration:0.25]];
                [dimScreen runAction:[SKAction fadeOutWithDuration:0.35] completion:^{
                    [dimScreen removeFromParent];
                }];
                terrainTypeMenuVisible = NO;
                break;
            }
        }
    }
    else if (!paintMenu.hidden) {
        CGPoint paintMenuLocation = [paintMenu convertPoint:location fromNode:self];
        [((SKNode*)menusArray[currentMenu]).physicsBody setVelocity:CGVectorMake(0.0, 0.0)];
        if ([[paintMenu childNodeWithName:@"exit"]containsPoint:paintMenuLocation]) {
            [paintMenu runAction:[SKAction moveTo:CGPointMake(-self.size.width, 0.0) duration:0.18] completion:^{
                [paintMenu setHidden:YES];
            }];
            return;
        }
        if ([[paintMenu childNodeWithName:@"Terrain"]containsPoint:paintMenuLocation]) {
            currentMenu = 0;
            for (int i = 0; i < 3; i++) {
                if (i == currentMenu) {
                    [menusArray[i] setHidden:NO];
                }
                else {
                    [menusArray[i] setHidden:YES];
                }
                [menusArray[i]setPosition:CGPointMake(0.0, 0.0)];
            }
        }
        else if ([[paintMenu childNodeWithName:@"Structures"]containsPoint:paintMenuLocation]) {
            currentMenu = 1;
            for (int i = 0; i < 3; i++) {
                if (i == currentMenu) {
                    [menusArray[i] setHidden:NO];
                }
                else {
                    [menusArray[i] setHidden:YES];
                }
                [menusArray[i]setPosition:CGPointMake(0.0, 0.0)];
            }
        }
        else if ([[paintMenu childNodeWithName:@"Units"]containsPoint:paintMenuLocation]) {
            currentMenu = 2;
            for (int i = 0; i < 3; i++) {
                if (i == currentMenu) {
                    [menusArray[i] setHidden:NO];
                }
                else {
                    [menusArray[i] setHidden:YES];
                }
                [menusArray[i]setPosition:CGPointMake(0.0, 0.0)];
            }
        }
        
        SKNode *currMenuNode = menusArray[currentMenu];
        for (SKSpriteNode *item in [currMenuNode children]) {
            if (item.alpha == 1.0 && [item containsPoint:[currMenuNode convertPoint:location fromNode:self]]) {
                [selectedTile setTexture:item.texture];
                [paintMenu runAction:[SKAction moveTo:CGPointMake(-self.size.width, 0.0) duration:0.18] completion:^{
                    [paintMenu setHidden:YES];
                }];
                
                //NSLog(@"%@",item.userData);
                paintNode = [SKSpriteNode spriteNodeWithTexture:item.texture];
                [selectedTile setTexture:paintNode.texture];
                if (currentMenu != 0) {
                    [paintNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
                }
                [paintNode setUserData:item.userData];
                
                [paintNode setName:(!currentMenu ? @"Terrain" : (currentMenu == 1 ? @"Structures" : @"Units"))];
                //NSLog(@"item name:%@",paintNode.name);
                //NSLog(@"itemNumber: %@",[paintNode.userData objectForKey:@"itemNumber"]);
                currentTileType = currentMenu;
                paintMode = YES;
                break;
            }
        }
    }
    else if ([terrainTypeMenu containsPoint:location]) {
        //NSLog(@"TERRAIN TYPE CONTAINS POINT");
        if (terrainTypeMenuExtended) {
            for (int i = 0; i < terrainTypesArray.count; i++) {
                SKSpriteNode *typeNode = terrainTypesArray[i];
                
                if ([typeNode containsPoint:[terrainTypeMenu convertPoint:location fromNode:self]]) {
                    [((SKLabelNode*)circleSelectedType.children.firstObject) setText:((SKLabelNode*)typeNode.children.firstObject).text];
                    [circleSelectedType setColor:typeNode.color];
                    terrainType = i;
                }
                
                [typeNode runAction:[SKAction moveToY:0.0 duration:0.17] completion:^{
                    [typeNode setHidden:YES];
                }];
                
            }
            terrainTypeMenuExtended = NO;
        }
        else {
            int counter = -1;
            for (int i = 0; i < terrainTypesArray.count; i++) {
                SKNode *typeNode = terrainTypesArray[i];
                if (terrainType != i) {
                    [typeNode runAction:[SKAction moveToY:37.5*factor*(i-counter) duration:0.2]];
                    [typeNode setHidden:NO];
                }
                else {
                    counter++;
                }
            }
            terrainTypeMenuExtended = YES;
        }
        NSLog(@"TERRAIN TYPE: %d",terrainType);
    }
    else if ([[self childNodeWithName:@"circle1"]containsPoint:location]) {
        if ([self childNodeWithName:@"circle2"].xScale == 1.0) {
            if ([saveIcon containsPoint:[saveIcon.parent convertPoint:location fromNode:self]]) {
                //NSLog(@"SAVE ICON");
                [self showSaveMenu];
            }
            else if ([newIcon containsPoint:[newIcon.parent convertPoint:location fromNode:self]]) {
                //NSLog(@"NEW ICON");
                [self removeAllChildren];
                DesignRoomMenu_New *newMap = [DesignRoomMenu_New sceneWithSize:self.size];
                [self.view presentScene:newMap];
                return;
            }
            else if ([exitIcon containsPoint:[exitIcon.parent convertPoint:location fromNode:self]]) {
                //NSLog(@"EXIT ICON");
                DesignRoomMenu *designRoom = [DesignRoomMenu sceneWithSize:self.size];
                [self.view presentScene:designRoom transition:[SKTransition fadeWithDuration:0.3]];
                return;
            }
        }
        
        if ([self childNodeWithName:@"circle1"].alpha > 0.0) {
            [self runAction:[SKAction group:@[[SKAction runAction:[SKAction group:@[[SKAction scaleTo:200.0/25.0 duration:0.3],[SKAction fadeOutWithDuration:0.1]]] onChildWithName:@"circle1"],
                                              [SKAction runAction:[SKAction group:@[[SKAction scaleTo:1.0 duration:0.3],[SKAction fadeInWithDuration:0.009]]] onChildWithName:@"circle2"]]]];
            [saveIcon runAction:[SKAction sequence:@[[SKAction waitForDuration:0.22],[SKAction scaleTo:1.0 duration:0.09]]]];
            [newIcon runAction:[SKAction sequence:@[[SKAction waitForDuration:0.29],[SKAction scaleTo:1.0 duration:0.09]]]];
            [exitIcon runAction:[SKAction sequence:@[[SKAction waitForDuration:0.36],[SKAction scaleTo:1.0 duration:0.09]]]];
        }
        else {
            [self runAction:[SKAction group:@[[SKAction runAction:[SKAction group:@[[SKAction scaleTo:1.2 duration:0.3],[SKAction fadeInWithDuration:0.3]]] onChildWithName:@"circle1"],
                                              [SKAction runAction:[SKAction group:@[[SKAction scaleTo:25.0/200.0 duration:0.3],[SKAction fadeOutWithDuration:0.3]]] onChildWithName:@"circle2"]]]];
            [saveIcon runAction:[SKAction sequence:@[[SKAction scaleTo:0.0 duration:0.07]]]];
            [newIcon runAction:[SKAction sequence:@[[SKAction waitForDuration:0.05],[SKAction scaleTo:0.0 duration:0.07]]]];
            [exitIcon runAction:[SKAction sequence:@[[SKAction waitForDuration:0.10],[SKAction scaleTo:0.0 duration:0.07]]]];
        }
    }
    else if (!saveMenu.hidden) {
        
        if ([[saveMenu childNodeWithName:@"textBackground"]containsPoint:[saveMenu convertPoint:location fromNode:self]]) {
            if (![keyboard isFirstResponder]) {
                NSLog(@"become first responder");
                [keyboard becomeFirstResponder];
                [keyboard setTextlabel:((SKLabelNode*)[saveMenu childNodeWithName:@"text"])];
            }
        }
        if ([[saveMenu childNodeWithName:@"cancelLabel"]containsPoint:[saveMenu convertPoint:location fromNode:self]]) {
            [saveMenu runAction:[SKAction fadeOutWithDuration:0.25] completion:^{
                [saveMenu setHidden:YES];
            }];
            [keyboard resignFirstResponder];
        }
        else if ([[saveMenu childNodeWithName:@"saveLabel"]containsPoint:[saveMenu convertPoint:location fromNode:self]]) {
            [self keyboardIsReturned];
        }
        
    }
    else if ([selectedTileCard containsPoint:location]) {
        [self updatePaintMenu];
        [paintMenu setHidden:NO];
        [paintMenu runAction:[SKAction moveTo:CGPointMake(0.0, 0.0) duration:0.25]];
    }
    else if ([menuButton containsPoint:location]) {
        //[self saveMap];
        //NSLog(@"MENU BUTTON");
        //rightMenuVisible ? [self hideRightMenu] : [self showRightMenu];
        
    }
    else if ([[self childNodeWithName:@"tool"]containsPoint:location]) {
        //NSLog(@"switch tools");
        paintMode = !paintMode;
        [[self childNodeWithName:@"tool"]runAction:[SKAction colorizeWithColor:(paintMode ? [UIColor greenColor] : [UIColor redColor]) colorBlendFactor:1.0 duration:0.2]];
        [[[[self childNodeWithName:@"tool"]children]firstObject]runAction:[SKAction colorizeWithColor:(!paintMode ? [UIColor greenColor] : [UIColor redColor]) colorBlendFactor:1.0 duration:0.2]];
    }
    else if ([playersMenu containsPoint:location]) {
        //NSLog(@"PLAYERS MENU");
        CGPoint ownerColorMenuLocation = [playersMenu convertPoint:location fromNode:self];
        if ([addPlayerButton containsPoint:ownerColorMenuLocation]) {
            [self newPlayerAdded];
        }
        else {
            for (SKSpriteNode *ownerColor in ownerColorNodes) {
                if (!ownerColor.hidden && [ownerColor containsPoint:ownerColorMenuLocation]) {
                    NSLog(@"selected color");
                    owner = [ownerColor.name intValue];
                    NSLog(@"owner: %d",owner);
                    [paintNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
                    [selectedTileCard runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.00]];
                    [ownerColor setAlpha:1.0];
                    
                    for (SKSpriteNode *ownerCol in ownerColorNodes) {
                        if (![ownerCol isEqual:ownerColor]) {
                            [ownerCol setAlpha:0.35];
                        }
                    }
                    break;
                }
            }
        }
    }
    else {
        CGPoint locationMap = [map convertPoint:location fromNode:self];
        //NSLog(@"location x:%f y:%f",locationMap.x,locationMap.y);
        // MAGIC HAPPENS HERE
        CGFloat tmpy = (locationMap.y/(3/4.0*64.0*[Functions deviceFactor]));
        CGFloat tmpx = ((locationMap.x+(((int)tmpy)%2==0 ? (sqrt(3)/2*64.0*[Functions deviceFactor])/2 : 0))/(sqrt(3)/2*64.0*[Functions deviceFactor]));
        
        //NSLog(@"xf: %.02f, yf: %.02f",tmpx,tmpy);
        int x = (int)(tmpx);
        int y = (int)(tmpy);
        //NSLog(@"x: %d, y: %d",x,y);
        /*
        if (locationMap.x < 0 || locationMap.y < 0 || y >= [mapTiles count] || x >= [[mapTiles firstObject]count]) {
            return;
        }
        */
        if (x < 0 || y < 0 || y >= [mapTiles count] || x >= [[mapTiles firstObject]count]) {
            return;
        }
#pragma mark WORKING_AREA
        BOOL contains;
        contains = [self hexagonWithNode:mapTiles[y][x] containsPoint:locationMap];
        //NSLog(@"1 (%d,%d) CONTAINS: %d",x,y,contains);
        if (y+1 < mapTiles.count && x+(y%2==0 ? 0 : 1) < [mapTiles.firstObject count]) {
            contains = [self hexagonWithNode:mapTiles[y+1][x+(y%2==0 ? 0 : 1)] containsPoint:locationMap];
            //NSLog(@"2 (%d,%d) CONTAINS: %d",x+(y%2==0 ? 0 : 1),y+1,contains);
            if (contains) {
                x = x+(y%2==0 ? 0 : 1);
                y = y+1;
            }
        }
        if (x+1 < [mapTiles.firstObject count]) {
            contains = [self hexagonWithNode:mapTiles[y][x+1] containsPoint:locationMap];
            //NSLog(@"3 (%d,%d) CONTAINS: %d",x+1,y,contains);
            if (contains) {
                x = x+1;
                y = y;
            }
        }
        
        //NSLog(@"DRAWING ON THE MAP");
        //KLE SE RISE MAPA
        SKNode *tile = mapTiles[y][x];
        if (paintMode && paintNode) {
            SKSpriteNode *layer = (SKSpriteNode*)[tile childNodeWithName:paintNode.name];
            SKSpriteNode *nodeTmp = paintNode.copy;
            nodeTmp.position = layer.position;
            [nodeTmp.userData setObject:[NSNumber numberWithInt:(owner-1)] forKey:@"Owner"];
            if (layer) {
                [layer removeFromParent];
                if ([[nodeTmp.userData objectForKey:@"Name"] isEqualToString:@"Plain"]) {
                    //DO CLIFFS --- FUCK THIS
                    //[self createCliffsFromNode:nodeTmp];
                }
            }
            [tile addChild:nodeTmp];
            //ce je teren
            if ([nodeTmp.name isEqualToString:@"Terrain"]) {
                [nodeTmp setZPosition:0.0];
                NSLog(@"terrain: %@",nodeTmp.userData);
            }
            else if ([nodeTmp.name isEqualToString:@"Structures"]) {
                [nodeTmp setZPosition:1.0];
                [nodeTmp setScale:0.7];
                NSLog(@"USER DATA: %@",nodeTmp.userData);
                if ([[nodeTmp.userData objectForKey:@"itemNumber"]intValue] == 10) {
                    playerOwnership[owner-1][0]++;
                }
                else if ([[nodeTmp.userData objectForKey:@"itemNumber"]intValue] == 6 ||
                         [[nodeTmp.userData objectForKey:@"itemNumber"]intValue] == 14 ||
                         [[nodeTmp.userData objectForKey:@"itemNumber"]intValue] == 0) {
                    playerOwnership[owner-1][1]++;
                }
            }
            else if ([nodeTmp.name isEqualToString:@"Units"]) {
                [nodeTmp setZPosition:2.0];
                [nodeTmp setScale:0.7];
                playerOwnership[owner-1][1]++;
            }
        }
        else {
            if (tile.children.count > 1) {
                SKNode *layerToRemove = [tile.children lastObject];
                [layerToRemove removeFromParent];
                
                if ([layerToRemove.name isEqualToString:@"Structures"]) {
                    if ([[layerToRemove.userData objectForKey:@"itemNumber"]intValue] == 10) {
                        playerOwnership[owner-1][0]--;
                    }
                    else if ([[layerToRemove.userData objectForKey:@"itemNumber"]intValue] == 6 ||
                             [[layerToRemove.userData objectForKey:@"itemNumber"]intValue] == 14 ||
                             [[layerToRemove.userData objectForKey:@"itemNumber"]intValue] == 0) {
                        playerOwnership[owner-1][1]--;
                    }
                }
                else if ([layerToRemove.name isEqualToString:@"Units"]) {
                    playerOwnership[owner-1][1]--;
                }
            }
        }
    }
}

-(BOOL)hexagonWithNode:(SKNode*)node containsPoint:(CGPoint)point {
    CGPoint pointLocation = [node convertPoint:point fromNode:map];
    CGFloat hexWidth = (sqrt(3)/2*64.0*[Functions deviceFactor])/2;
    CGFloat r = sqrtf(powf(pointLocation.x, 2)+powf(pointLocation.y, 2));
    //NSLog(@"hexWidth: %f, r: %f",hexWidth,r);
    if (r <= hexWidth) {
        return YES;
    }
    return NO;
}

-(void)newPlayerAdded {
    if (numOfPlayers+1 < ownerColorNodes.count) {
        [ownerColorNodes[++numOfPlayers] setHidden:NO];
    }
    if (numOfPlayers+1 == ownerColorNodes.count) {
        [addPlayerButton removeFromParent];
    }
    else {
        [addPlayerButton runAction:[SKAction moveToY:[ownerColorNodes[numOfPlayers+1]position].y duration:0.05]];
    }
     
    for (int i = 0; i < ownerColorNodes.count; i++) {
        if (i == numOfPlayers) {
            [ownerColorNodes[i] setAlpha:1.0];
        }
        else {
            [ownerColorNodes[i] setAlpha:0.4];
        }
    }
    owner = [ownerColorNodes[numOfPlayers]name].intValue;
    [paintNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
    [selectedTileCard runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
    
    /*
    SKSpriteNode *dialogBg = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:CGSizeMake(500.0, 60.0)];
    [dialogBg setZPosition:99999.0];
    [dialogBg setPosition:CGPointMake(self.size.width/2, dialogBg.size.height/2)];
    [dialogBg setName:@"newPlayerDialog"];
    [dialogBg setAlpha:0.85];
    [self addChild:dialogBg];
    
    SKLabelNode *dialogText = [SKLabelNode labelNodeWithText:@"Put down HQ for the new player."];
    [dialogText setFontSize:35.0];
    [dialogText setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [dialogBg addChild:dialogText];
    
    paintNode = [SKSpriteNode spriteNodeWithTexture:hqStructureNode.texture];
    [selectedTile setTexture:paintNode.texture];
    [paintNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
    [paintNode setUserData:hqStructureNode.userData];
    
    [paintNode setName:@"Structures"];
    paintMode = YES;
     */
}

-(void)removePlayer {
    [playersMenu.children[numOfPlayers] setHidden:YES];
    if (!addPlayerButton.parent) {
        [playersMenu addChild:addPlayerButton];
    }
    [addPlayerButton runAction:[SKAction moveToY:[ownerColorNodes[numOfPlayers]position].y duration:0.05]];
    numOfPlayers--;
    
    for (int i = 0; i < ownerColorNodes.count; i++) {
        if (i == numOfPlayers) {
            [ownerColorNodes[i] setAlpha:1.0];
        }
        else {
            [ownerColorNodes[i] setAlpha:0.4];
        }
    }
    
    owner = [ownerColorNodes[numOfPlayers]name].intValue;
    [paintNode runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
    [selectedTileCard runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.0]];
    
    // zbrise vse strukture od playerja ki je odstranjen
    for (NSArray *row in mapTiles) {
        for (SKNode *tile in row) {
            SKNode *structure = [tile childNodeWithName:@"Structures"];
            if ([[structure.userData objectForKey:@"Owner"]intValue] == owner) {
                [structure removeFromParent];
            }
        }
    }
}

-(void)panHandler:(UIPanGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (terrainTypeMenuVisible) {
    }
    else if (rightMenuVisible && location.x < self.size.width-rightMenuUI.size.width) {
        //[self hideRightMenu];
    }
    else if ([selectedTileCard containsPoint:location] || [playersMenu containsPoint:location]) {
    }
    else if (!paintMenu.hidden && [paintMenu containsPoint:location]) {
        //NSLog(@"MENU PAN");
        SKNode *menuNode = (SKNode*)menusArray[currentMenu];
        if (gesture.state == UIGestureRecognizerStateBegan) {
            panStartXPosition = location.x;
            menuStartX = menuNode.position.x;
            [menuNode.physicsBody setVelocity:CGVectorMake(0.0, 0.0)];
        }
        //NSLog(@"test");
        menuNode.position = CGPointMake(menuStartX+location.x-panStartXPosition, 0.0);
        if (menuNode.position.x > 0.0) {
            menuNode.position = CGPointMake(0.0, 0.0);
        }
        else if (menuNode.position.x < -menuNode.calculateAccumulatedFrame.size.width+self.size.width-100.0) {
            menuNode.position = CGPointMake(-menuNode.calculateAccumulatedFrame.size.width+self.size.width-100.0, 0.0);
        }
        if (gesture.state == UIGestureRecognizerStateEnded) {
            [menuNode.physicsBody applyImpulse:CGVectorMake([gesture velocityInView:self.view].x, 0.0)];
        }
        
    }
    else {
        CGPoint locationMap = [map convertPoint:location fromNode:self];
        CGFloat tmpy = (locationMap.y/(3/4.0*64.0*[Functions deviceFactor]));
        CGFloat tmpx = ((locationMap.x+(((int)tmpy)%2==0 ? (sqrt(3)/2*64.0*[Functions deviceFactor])/2 : 0))/(sqrt(3)/2*64.0*[Functions deviceFactor]));
        
        //NSLog(@"xf: %.02f, yf: %.02f",tmpx,tmpy);
        int x = (int)(tmpx);
        int y = (int)(tmpy);
        //NSLog(@"x: %d, y: %d",x,y);
        /*
         if (locationMap.x < 0 || locationMap.y < 0 || y >= [mapTiles count] || x >= [[mapTiles firstObject]count]) {
         return;
         }
         */
        if (x < 0 || y < 0 || y >= [mapTiles count] || x >= [[mapTiles firstObject]count]) {
            return;
        }
#pragma mark WORKING_AREA_2
        BOOL contains;
        contains = [self hexagonWithNode:mapTiles[y][x] containsPoint:locationMap];
        //NSLog(@"1 (%d,%d) CONTAINS: %d",x,y,contains);
        if (y+1 < mapTiles.count && x+(y%2==0 ? 0 : 1) < [mapTiles.firstObject count]) {
            contains = [self hexagonWithNode:mapTiles[y+1][x+(y%2==0 ? 0 : 1)] containsPoint:locationMap];
            //NSLog(@"2 (%d,%d) CONTAINS: %d",x+(y%2==0 ? 0 : 1),y+1,contains);
            if (contains) {
                x = x+(y%2==0 ? 0 : 1);
                y = y+1;
            }
        }
        if (x+1 < [mapTiles.firstObject count]) {
            contains = [self hexagonWithNode:mapTiles[y][x+1] containsPoint:locationMap];
            //NSLog(@"3 (%d,%d) CONTAINS: %d",x+1,y,contains);
            if (contains) {
                x = x+1;
                y = y;
            }
        }
        
        //NSLog(@"DRAWING ON THE MAP");
        //KLE SE RISE MAPA
        SKNode *tile = mapTiles[y][x];
        if (paintMode && paintNode) {
            SKSpriteNode *layer = (SKSpriteNode*)[tile childNodeWithName:paintNode.name];
            SKSpriteNode *nodeTmp = paintNode.copy;
            nodeTmp.position = layer.position;
            [nodeTmp.userData setObject:[NSNumber numberWithInt:(owner-1)] forKey:@"Owner"];
            if (layer) {
                [layer removeFromParent];
                if ([[nodeTmp.userData objectForKey:@"Name"] isEqualToString:@"Plain"]) {
                    //DO CLIFFS --- FUCK THIS
                    //[self createCliffsFromNode:nodeTmp];
                }
            }
            [tile addChild:nodeTmp];
            //ce je teren
            if ([nodeTmp.name isEqualToString:@"Terrain"]) {
                [nodeTmp setZPosition:0.0];
            }
            else if ([nodeTmp.name isEqualToString:@"Structures"]) {
                [nodeTmp setZPosition:1.0];
                [nodeTmp setScale:0.7];
            }
            else if ([nodeTmp.name isEqualToString:@"Units"]) {
                [nodeTmp setZPosition:2.0];
                [nodeTmp setScale:0.7];
            }
        }
        else {
            if (tile.children.count > 1) {
                SKNode *layerToRemove = [tile.children lastObject];
                [layerToRemove removeFromParent];
            }
        }
    }
}

-(void)panTwoFingerHandler:(UIPanGestureRecognizer*)gesture {
    if (terrainTypeMenuVisible) {
        return;
    }
    // da ni unga nadleznga skakanja ce spustis en prst :|
    if (gesture.numberOfTouches != 2) {
        return;
    }
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (rightMenuVisible && location.x < self.size.width-rightMenuUI.size.width) {
        //[self hideRightMenu];
        return;
    }
    if (gesture.state == UIGestureRecognizerStateBegan) {
        twoFingerPanStartLocation = location;
        mapStartPosition = map.position;
        //[menu runAction:[SKAction moveToX:-100.0 duration:0.09]];
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        //[menu runAction:[SKAction moveToX:0.0 duration:0.14]];
        return;
    }
    [map setPosition:CGPointMake(mapStartPosition.x+location.x-twoFingerPanStartLocation.x, mapStartPosition.y+location.y-twoFingerPanStartLocation.y)];
}
/*
-(void)pinchHandler:(UIPinchGestureRecognizer*)gesture {
    if (terrainTypeMenuVisible) {
        return;
    }
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    CGPoint mapGestureLocation = [map convertPoint:location fromNode:self];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        initialScale = map.xScale;
        [gesture setScale:map.xScale];
        [map setAnchorPoint:CGPointMake(mapGestureLocation.x/mapWidth, mapGestureLocation.y/mapHeight)];
        [map setPosition:location];
    }
    if (map.xScale > 1.0 || map.xScale < 0.3) {
        return;
    }
    [map setScale:gesture.scale];
    [map setPosition:location];
}
*/

-(void)pinchHandler:(UIPinchGestureRecognizer*)gesture {
    if (terrainTypeMenuVisible) {
        return;
    }
    CGPoint gestureLocation =  [map convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    //gestureLocation = CGPointMake(gestureLocation.x+levelWidth*world.anchorPoint.x, gestureLocation.y+levelHeight*world.anchorPoint.y);
    //NSLog(@"gesture world location: %f x %f",gestureLocation.x,gestureLocation.y);
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [gesture setScale:map.xScale];
        startScale = map.xScale;
        startPosition = map.position;
        gestureTargetPosition = location;
        //[world setAnchorPoint:CGPointMake(1.0, 1.0)];
        //[LevelLoader updateGround:world];
        CGPoint anchor = CGPointMake((gestureLocation.x/mapSize*128.0+1.0)/2.0, (gestureLocation.y/mapSize*128.0+1.0)/2.0);
        [map setAnchorPoint:anchor];
    }
    if (gesture.scale > 1.0 || gesture.scale < 0.3 || gesture.numberOfTouches < 2) {
        return;
    }
    
    //NSLog(@"location: %f x %f",anchor.x, anchor.y);
    
    //world.position = CGPointMake(location.x, location.y);
    [map setScale:gesture.scale];
    //map.position = CGPointMake(levelWidth/2.0*(1.0-world.anchorPoint.x*2.0), levelHeight/2.0*(1.0-world.anchorPoint.y*2.0));
    //[world setPosition:CGPointMake(gestureTargetPosition.x-(gestureTargetPosition.x-location.x)/(gesture.scale), gestureTargetPosition.y-(gestureTargetPosition.y-location.y)/(gesture.scale))];
    
}

-(void)edgeSwipeInRightHandler:(UIScreenEdgePanGestureRecognizer*)gesture {
    if (!paintMenu.hidden && gesture.state == UIGestureRecognizerStateRecognized) {
        [paintMenu runAction:[SKAction moveTo:CGPointMake(-self.size.width, 0.0) duration:0.18] completion:^{
            [paintMenu setHidden:YES];
        }];
    }}

-(void)edgeSwipeInLeftHandler:(UIScreenEdgePanGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        //NSLog(@"left edge swipe");
        owner++;
        if (owner > 4) {
            owner = 0;
        }
        for (int i = 1; i < allMenuItems.count; i++) {
            for (SKNode *item in allMenuItems[i]) {
                [item runAction:[SKAction colorizeWithColor:ownerColors[owner] colorBlendFactor:1.0 duration:0.3]];
            }
        }
    }
}

-(void)swipeHandler:(UISwipeGestureRecognizer*)gesture {
    /*
    if (gesture.state != UIGestureRecognizerStateRecognized) {
        return;
    }
    */
    NSLog(@"SWIPE");
    if (numOfPlayers <= 2) {
        return;
    }
    CGPoint location = [playersMenu convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    //for (SKNode *playerColorNode in playersMenu.children) {
    SKNode *lastPlayerColorNode = playersMenu.children[numOfPlayers];
        if ([lastPlayerColorNode containsPoint:location]) {
            SKSpriteNode *removePlayerNode = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(35.0, 7.0)];
            [removePlayerNode setPosition:lastPlayerColorNode.position];
            [removePlayerNode setAlpha:0.0];
            [removePlayerNode runAction:[SKAction fadeInWithDuration:0.3]];
            [lastPlayerColorNode runAction:[SKAction moveByX:-75.0 y:0.0 duration:0.3]];
            [self addChild:removePlayerNode];
            //break;
        }
    //}
}

-(void)longPressHandler:(UILongPressGestureRecognizer*)gesture {
    CGPoint location = [playersMenu convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        //NSLog(@"LONGPRESS ENDED");
        SKNode *removePlayerNode = [playersMenu childNodeWithName:@"removePlayer"];
        if ([removePlayerNode containsPoint:location]) {
            NSLog(@"REMOVE PLAYER");
            [self removePlayer];
            
            [removePlayerNode removeFromParent];
        }
    }
    if (gesture.state != UIGestureRecognizerStateBegan || numOfPlayers <= 2) {
        return;
    }
    
    NSLog(@"LONG PRESS");
    SKNode *lastPlayerColorNode = playersMenu.children[numOfPlayers];
    if ([lastPlayerColorNode containsPoint:location]) {
        SKSpriteNode *remove = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(25.0*factor, 25.0*factor)];
        [remove setName:@"removePlayer"];
        [remove setZPosition:999999.0];
        [remove setPosition:CGPointMake(lastPlayerColorNode.position.x-37.5*factor, lastPlayerColorNode.position.y)];
        [playersMenu addChild:remove];
    }
}

-(NSDictionary*)createDictionaryForMap {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:3];
    [dict setObject:((SKLabelNode*)[saveMenu childNodeWithName:@"text"]).text forKey:@"LevelName"];
    [dict setValue:@20  forKey:@"Income Multiplier"];
    NSMutableArray *structureArray = [NSMutableArray array];
    NSMutableArray *mapArray = [NSMutableArray array];
    [structureArray addObject:@0];
    [structureArray addObject:@0];
    [structureArray addObject:@0];
    [structureArray addObject:@0];
    [structureArray addObject:@0];
    for (int i = 0; i < userMapHeight; i++) {
        [mapArray addObject:[NSMutableArray arrayWithCapacity:userMapWidth]];
    }
    //__block int players = 0;
    __block int counter = 0;
    __block NSMutableArray *offset = [NSMutableArray arrayWithCapacity:mapHeight];
    for (int i = 0; i < userMapHeight; i++) {
        offset[i] = [NSNumber numberWithInt:0];
    }
    __block BOOL begining = YES;
    __block int prevY = 0;
    [map enumerateChildNodesWithName:@"Tile" usingBlock:^(SKNode *tile, BOOL *stop) {
        int y = (int)(tile.position.y/(3/4.0*64.0*[Functions deviceFactor]));
        //NSLog(@"Y: %d (%f) %.03f",y,tile.position.y,tile.position.y/(sqrt(3)/2*64.0*[Functions deviceFactor]));
        int x = (int)((tile.position.x+(y%2==0 ? (sqrt(3)/2*64.0*[Functions deviceFactor])/2 : 0))/(sqrt(3)/2*64.0*[Functions deviceFactor]));
        //NSLog(@"Y: %d , X: %d",y,x);
        NSMutableArray *arrayTmp = [NSMutableArray array];
        if (y != prevY) {
            begining = YES;
        }
        //NSLog(@"TILE %d",counter++);
        for (int i = 0; i < tile.children.count; i++) {
            SKSpriteNode *layer = tile.children[i];
            if (layer.userData) {
                if ([layer.name isEqualToString:@"Terrain"]) {
                    //NSLog(@"number: %@",[layer.userData objectForKey:@"itemNumber"]);
                    [arrayTmp setObject: @[[layer.userData objectForKey:@"itemNumber"],@(0)] atIndexedSubscript:0];
                    if ([[layer.userData objectForKey:@"Name"]isEqualToString:@"Fog"]) {
                        if (begining) {
                            offset[y] = [NSNumber numberWithInt:([offset[y]intValue]+1)];
                        }
                    }
                    else {
                        begining = NO;
                    }
                }
                else if ([layer.name isEqualToString:@"Structures"]) {
                    if (!arrayTmp.count) {
                        [arrayTmp setObject: @[[layer.userData objectForKey:@"itemNumber"],[layer.userData objectForKey:@"Owner"]]atIndexedSubscript:0];
                    }
                    [arrayTmp setObject: @[[layer.userData objectForKey:@"itemNumber"],[layer.userData objectForKey:@"Owner"]]atIndexedSubscript:1];
                    int num = ((NSNumber*)[layer.userData objectForKey:@"itemNumber"]).intValue;
                    //[structureArray replaceObjectAtIndex:num withObject:[structureArray objectAtIndex:num]++ ];
                    switch (num) {
                        case 0:
                            [structureArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:1] intValue] + 1]];
                            break;
                        case 1:
                            [structureArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:1] intValue] + 1]];
                            break;
                        case 4:
                            [structureArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:3] intValue] + 1]];
                            break;
                        case 5:
                            [structureArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:3] intValue] + 1]];
                            break;
                        case 6:
                            [structureArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:0] intValue] + 1]];
                            break;
                        case 7:
                            [structureArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:0] intValue] + 1]];
                            break;
                        case 8:
                            [structureArray replaceObjectAtIndex:4 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:4] intValue] + 1]];
                            break;
                        case 9:
                            [structureArray replaceObjectAtIndex:4 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:4] intValue] + 1]];
                            break;
                        case 10:
                            //players++;
                            break;
                        case 14:
                            [structureArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:2] intValue] + 1]];
                            break;
                        case 15:
                            [structureArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:[[structureArray objectAtIndex:2] intValue] + 1]];
                            break;
                        default:
                            break;
                    }
                    //NSLog(@"ITEM NUMBER: %ld, OWNER: %ld",((NSNumber*)[layer.userData objectForKey:@"itemNumber"]).integerValue,((NSNumber*)[layer.userData objectForKey:@"Owner"]).integerValue);
                }
                else if ([layer.name isEqualToString:@"Units"]) {
                   //TODO ce je unit ga dodej, pa ne pozab da je treba dodat prazen spot za teren in structure ce se nista nrjena
                }
                
            }
        }
        //NSLog(@"X: %d, Y: %d",x,y);
        mapArray[y][x] = arrayTmp;
        prevY = y;
    }];
    
    //int counter = 0;
    NSLog(@"OFFSET");
    for (int i = 0; i < userMapHeight; i++) {
        printf("%d, ",[offset[i]intValue]);
    }
    printf("\n");
    
    int addValue = (int)(mapArray.count/2)-1+((int)(mapArray.count)%2);
    for (int i = 0; i < mapArray.count; i++) {
        //NSMutableArray *mapArrayRow = mapArray[i];
        for (int a = 0; a < addValue; a++) {
            //[mapArrayRow insertObject:[NSArray array] atIndex:0];
            offset[i] = [NSNumber numberWithInt:([offset[i]intValue]+1)];
        }
        if (i%2) {
            addValue--;
        }
    }
    
    NSLog(@"OFFSET(2)");
    for (int i = 0; i < userMapHeight; i++) {
        printf("%d, ",[offset[i]intValue]);
    }
    printf("\n");
    
    [dict setObject:offset forKey:@"LevelOffset"];
    [dict setObject:[NSNumber numberWithInt:numOfPlayers] forKey:@"Players"];
    [dict setObject:mapArray forKey:@"LevelMap"];
    [dict setObject:structureArray forKey:@"Structures"];
    [dict setObject:[self createTextureFromMap] forKey:@"Image"];
    //NSLog(@"SAVE dict: %@", dict);
    return dict;
}

-(void)createCliffsFromNode:(SKSpriteNode*)node {
    CGPoint location = node.parent.position;
    int x = (int)location.x/128;
    int y = (int)location.y/128;
    for (int i = y-1; i <= y+1; i++) {
        for (int j = x-1; j <= x+1; j++) {
            if (y == i && x == j) {
                continue;
            }
            SKSpriteNode *tile = mapTiles[i][j];
            //NSLog(@"TILE: %@",tile);
            [self checkForNearbyGroundWithNode:tile andArrayLocation:CGPointMake(j, i)];
        }
    }
}

-(void)checkForNearbyGroundWithNode:(SKSpriteNode*)node andArrayLocation:(CGPoint)location {
    int counterX = -1,counterY = -1;
    for (int i = location.y-1; i <= location.y+1; i++) {
        for (int j = location.x-1; j <= location.x+1; j++) {
            
            if (counterX == 0 && counterY == 0) {
                counterX++;
                continue;
            }
            SKNode *terrain = ((SKNode*)((SKNode*)mapTiles[i][j]).children[0]);
            if ([[terrain.userData objectForKey:@"isGround"]boolValue]) {
                //NSLog(@"ground");
                if (counterX == -1 && counterY == -1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_7"];
                    [cliff setXScale:-1.0];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == 0 && counterY == -1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_6"];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == 1 && counterY == -1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_7"];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == -1 && counterY == 0) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_5"];
                    [cliff setXScale:-1.0];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == 1 && counterY == 0) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_5"];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == -1 && counterY == 1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_3"];
                    [cliff setXScale:-1.0];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == 0 && counterY == 1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_4"];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
                else if (counterX == 1 && counterY == 1) {
                    SKSpriteNode *cliff = [SKSpriteNode spriteNodeWithImageNamed:@"cliff_3"];
                    [node.children[0] addChild:cliff];
                    //NSLog(@"%d x %d",counterX,counterY);
                }
            }
            
            counterX++;
        }
        counterY++;
        counterX = -1;
        
    }
    SKTexture *nodeTexture = [self.view textureFromNode:node.children[0]];
    [node.children[0] setTexture:nodeTexture];
    [node.children[0] removeAllChildren];
}

-(void)showSaveMenu {
    if ([self checkPlayers] || 1) {
        [keyboard becomeFirstResponder];
        [keyboard setTextlabel:((SKLabelNode*)[saveMenu childNodeWithName:@"text"])];
        [keyboard setMaxLength:((SKSpriteNode*)[saveMenu childNodeWithName:@"textBackground"]).size.width];
        [saveMenu setHidden:NO];
        [saveMenu setAlpha:1.0];
        [saveMenu runAction:[SKAction moveTo:CGPointMake(self.size.width/2, self.size.height/2+60.0*factor) duration:0.25]];
    }
    
}
// preveri ce imajo vsi playerji vse
-(BOOL)checkPlayers {
    for (int i = 0; i < numOfPlayers; i++) {
        for (int j = 0; j < 2; j++) {
            if(playerOwnership[i][j] == 0) {
                return NO;
            }
        }
    }
    return YES;
}

-(void)updatePaintMenu {
    SKNode *structuresMenu = (SKNode*)menusArray[1];
    for (SKNode *item in structuresMenu.children) {
        NSLog(@"item: %@",item);
        if ((playerOwnership[owner-1][0] != 0 || owner == 0) && [[item.userData objectForKey:@"itemNumber"]intValue] == 10) {
            [item setAlpha:0.2];
        }
        else {
            [item setAlpha:1.0];
        }
    }
}

-(void)saveMap {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    /*
    //DELETE ALL FILES IN DOCUMENTS DIRECTORY
    for (NSString *file in [fileManager contentsOfDirectoryAtPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] error:nil]) {
        [fileManager removeItemAtPath:file error:nil];
    }
    */
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"CustomMaps.plist"];
    NSMutableArray *customMapsRootArray;
    NSLog(@"file path: %@",filePath);
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        customMapsRootArray = [NSMutableArray array];
        NSLog(@"new file");
    }
    else {
        customMapsRootArray = [NSMutableArray arrayWithContentsOfFile:filePath];
    }
    //customMapsRootArray = [NSMutableArray array];
    //NSLog(@"CUSTOM MAPS ARRAY: %@",customMapsRootArray);
    NSDictionary *mapDict = [self createDictionaryForMap];
    [customMapsRootArray addObject:mapDict];
     //NSLog(@"CUSTOM MAPS ARRAY after: %@",customMapsRootArray);
    BOOL success = [customMapsRootArray writeToFile:filePath atomically:YES];
    NSLog(@"SUCCESS: %d",success);
}

-(NSData*)createTextureFromMap {
    SKSpriteNode *mapTmp = [map copy];
    [[mapTmp childNodeWithName:@"overlay"] setHidden:YES];
    [mapTmp setScale:(480.0/(mapWidth >= mapHeight ? mapWidth : mapHeight))];
    SKTexture *mapTexture = [self.view textureFromNode:mapTmp];
    NSData *textureData = [NSKeyedArchiver archivedDataWithRootObject:mapTexture];
    return textureData;
}

- (void)keyboardIsReturned {
    [self saveMap];
    [saveMenu runAction:[SKAction fadeOutWithDuration:0.25] completion:^{
        [saveMenu setHidden:YES];
    }];
    [keyboard resignFirstResponder];
}

-(void)didSimulatePhysics {
    /*
    if (((SKNode*)menusArray[currentMenu]).position.x > 0.0) {
        ((SKNode*)menusArray[currentMenu]).position = CGPointMake(0.0, 0.0);
    }
    else if (((SKNode*)menusArray[currentMenu]).position.x < -((SKNode*)menusArray[currentMenu]).calculateAccumulatedFrame.size.width+self.size.width-100.0) {
        ((SKNode*)menusArray[currentMenu]).position = CGPointMake(-((SKNode*)menusArray[currentMenu]).calculateAccumulatedFrame.size.width+self.size.width-100.0, 0.0);
    }
     */
}

-(void)update:(NSTimeInterval)currentTime {
    //NSLog(@"NUMBER OF PLAYERS: %d",numOfPlayers);
    /*
    if (loading) {
        [loading setText:[NSString stringWithFormat:@"%d",(int)loadingValue]];
    }
     */
}

@end
