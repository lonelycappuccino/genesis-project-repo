//
//  Functions.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "Functions.h"

@interface SKScene (Extensions)

- (void)keyboardIsReturned;

@end

@implementation KeyboardView

- (void)insertText:(NSString *)text {
    NSLog(@"TEXT: %@",text);
    if ([text isEqualToString:@"\n"]) {
        [_textlabel.scene keyboardIsReturned];
    }
    if (_textlabel.frame.size.width+5.0*factor <= _maxLength) {
        [_textlabel setText:[NSString stringWithFormat:@"%@%@",_textlabel.text,text]];
    }
}
- (void)deleteBackward {
    NSLog(@"delete");
    if (_textlabel.text.length > 0) {
        [_textlabel setText:[_textlabel.text substringToIndex:_textlabel.text.length-1]];
    }
}
- (BOOL)hasText {
    return YES;
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
@end

@implementation Functions

static CGFloat deviceFactor = 0.0;
static CGFloat tileSize = 0.0;
static CGSize viewSiz;

+(void)init {
    if (tileSize == 0.0) {
        deviceFactor = ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 1.0 : 2.0);
        /*
        if (deviceFactor == 1.0) {
            deviceFactor = [UIScreen mainScreen].nativeScale/2.0;
        }
         */
        tileSize = deviceFactor * 64;
    }
}

+(CGFloat)deviceFactor {
    return deviceFactor;
}

+(CGFloat)tileSize {
    return tileSize;
}

+(void)setViewSize:(CGSize)size {
    viewSiz = size;
    NSLog(@"VIEW SIZE: %f x %f", size.width, size.height);
}

+(CGSize)getViewSize {
    return viewSiz;
}

@end

@implementation  NSObject (MyDeepCopy)
-(id)deepMutableCopy
{
    if ([self isKindOfClass:[NSArray class]]) {
        NSArray *oldArray = (NSArray *)self;
        NSMutableArray *newArray = [NSMutableArray array];
        for (id obj in oldArray) {
            [newArray addObject:[obj deepMutableCopy]];
        }
        return newArray;
    } else if ([self isKindOfClass:[NSDictionary class]]) {
        NSDictionary *oldDict = (NSDictionary *)self;
        NSMutableDictionary *newDict = [NSMutableDictionary dictionary];
        for (id obj in oldDict) {
            [newDict setObject:[oldDict[obj] deepMutableCopy] forKey:obj];
        }
        return newDict;
    } else if ([self isKindOfClass:[NSSet class]]) {
        NSSet *oldSet = (NSSet *)self;
        NSMutableSet *newSet = [NSMutableSet set];
        for (id obj in oldSet) {
            [newSet addObject:[obj deepMutableCopy]];
        }
        return newSet;
#if MAKE_MUTABLE_COPIES_OF_NONCOLLECTION_OBJECTS
    } else if ([self conformsToProtocol:@protocol(NSMutableCopying)]) {
        // e.g. NSString
        return [self mutableCopy];
    } else if ([self conformsToProtocol:@protocol(NSCopying)]) {
        // e.g. NSNumber
        return [self copy];
#endif
    } else {
        return self;
    }
}
@end
