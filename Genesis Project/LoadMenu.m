//
//  LoadMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 25. 09. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "LoadMenu.h"
#import "GameScene.h"
#import "GZIP.h"
#import "MainMenu.h"

@implementation LoadMenu {
    SKSpriteNode *backButton, *mapSelectNode;
    NSMutableArray *savedGamesArray;
    MainMenu *mainMenu;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:40.0/255.0 blue:45.0/255.0 alpha:1.0];
        
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-15.0)];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        
        SKSpriteNode *titleBg = [SKSpriteNode spriteNodeWithImageNamed:@"titleBg"];
        [titleBg setPosition:CGPointMake(self.size.width/2, self.size.height-titleBg.size.height/2)];
        [titleBg setZPosition:1.0];
        [self addChild:titleBg];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:NSLocalizedString(@"Load",nil)];
        [titleLabel setFontSize:65.0];
        [titleLabel setZPosition:1.0];
        [titleLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [titleBg addChild:titleLabel];

        mapSelectNode = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenuBg"];
        [mapSelectNode setPosition:CGPointMake(self.size.width/2, self.size.height/2-32.0)];
        [mapSelectNode setZPosition:1.0];
        [mapSelectNode setXScale:1.4];
        [self addChild:mapSelectNode];
        
        savedGamesArray = [NSMutableArray arrayWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"savedGames.save"]];
        //NSLog(@"saveData: %@",savedGamesArray);
        int counter = 0;
        for (NSDictionary *savedGame in savedGamesArray) {
            //NSLog(@"LEVEL: %@",level);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[savedGame objectForKey:@"Name"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor darkGrayColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setZPosition:1.0];
            //[levelLabel setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-90.0-50.0*((SKNode*)levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]).children.count)];
            //[mapSelectNode addChild:levelLabel];
            //[levelLabel setUserData:[NSMutableDictionary dictionaryWithObject:@(counter) forKey:@"levelNumber"]];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_element"];
            [levelHolder setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-105.0-70.0*counter)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:savedGame]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            [mapSelectNode addChild:levelHolder];
            counter++;
        }
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
    
    mainMenu = [MainMenu sceneWithSize:self.size];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    
    if ([backButton containsPoint:location]) {
        [self.view presentScene:mainMenu transition:[SKTransition crossFadeWithDuration:0.3]];
    }
    else if ([mapSelectNode containsPoint:location]) {
        for (SKNode *button in mapSelectNode.children) {
            if ([button containsPoint:[mapSelectNode convertPoint:location fromNode:self]]) {
                [self loadLevel:button.userData];
                break;
            }
        }
    }
}

-(void)loadLevel:(NSDictionary*)levelDict {
    NSData *data =[[levelDict objectForKey:@"saveData"]gunzippedData];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:[GameScene class] forClassName:@"GameScene"];
    GameScene *savedScene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [GameScene loadGame:savedScene];
    [self.view presentScene:savedScene];
}

@end
