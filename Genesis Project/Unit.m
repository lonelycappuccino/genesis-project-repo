//
//  Unit.m
//  Genesis Project
//
//  Created by Klemen Košir on 17/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "Unit.h"
#import "Functions.h"
#import "LevelLoader.h"
#import "GameScene.h"
#import "Player.h"

@interface PathFindNode : NSObject {
@public
    int nodeX, nodeY;
    int cost;
    int fuelNeeded;
    PathFindNode *parentNode;
}
+(id)node;
@end

@implementation PathFindNode

+(id)node {
    return [PathFindNode new];
}

@end


@implementation Unit {
    NSArray *costMap;
    int unitType,counter;
    SKNode *unitMenu, *hpBar;
    SKAction *unitMenuAction;
    SKSpriteNode *hpBarValue;
    __strong Unit *loadedUnit;
}

static GameScene *gameScene = nil;

-(void)createUnitWithDictionary:(NSDictionary*)dict {
    _unitName = [dict objectForKey:@"Name"];
    _primaryAmmo = ((NSNumber*)[dict objectForKey:@"PrimaryAmmo"]).intValue;
    _primaryWeapon = [dict objectForKey:@"PrimaryWeapon"];
    _primaryWeaponDamage = [dict objectForKey:@"Dmg"];
    _weaponRange = ((NSNumber*)[dict objectForKey:@"Range"]).intValue;
    _secondaryWeapon = [dict objectForKey:@"SecondaryWeapon"];
    _secondaryWeaponDamage = [dict objectForKey:@"SecondaryDmg"];
    _health = ((NSNumber*)[dict objectForKey:@"HP"]).intValue;
    _maxHealth = _health;
    _movement = ((NSNumber*)[dict objectForKey:@"Movement"]).intValue;
    _maxMovement = _movement;
    _fuel = ((NSNumber*)[dict objectForKey:@"Fuel"]).intValue;
    _vision = ((NSNumber*)[dict objectForKey:@"Vision"]).intValue;
    _armor = ((NSNumber*)[dict objectForKey:@"Armor"]).intValue;
    _type = ((NSNumber*)[dict objectForKey:@"Type"]).intValue;
    _movementType = ((NSNumber*)[dict objectForKey:@"MovementType"]).intValue;
    _invisible = ((NSNumber*)[dict objectForKey:@"Invisible"]).boolValue;
    _special = ((NSString*)[dict objectForKey:@"Special"]);
    _canMove = YES;
    _canAttack = YES;
    NSLog(@"is invisible? %d",_invisible);
    SKTexture *texture = [SKTexture textureWithImageNamed:_unitName];
    [texture setUsesMipmaps:YES];
    [self setTexture:texture];
    [self setSize:texture.size];
    
    unitType = 0;
    /*
    unitMenu = [SKNode node];
    int buttonPositionsX[] = {45*[Functions deviceFactor],0,-45*[Functions deviceFactor],0};
    int buttonPositionsY[] = {0,-45*[Functions deviceFactor],0,45*[Functions deviceFactor]};
    for (int i = 0; i < 3; i++) {
        SKSpriteNode *menuButton = [SKSpriteNode spriteNodeWithImageNamed:@"unitMenuPart"];
        [menuButton setZRotation:M_PI/2*-i];
        [menuButton setName:[NSString stringWithFormat:@"button%d",i]];
        [menuButton setPosition:CGPointMake(buttonPositionsX[i], buttonPositionsY[i])];
        [unitMenu addChild:menuButton];
    }
    unitMenuAction = [SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.5],
                                                                                  [SKAction scaleTo:1.0 duration:1.5]]]];
    [unitMenuAction setTimingMode:SKActionTimingEaseInEaseOut];
    //[unitMenuAction setSpeed:0.0];
    //[unitMenu runAction:unitMenuAction withKey:@"menuAction"];
    [unitMenu setScale:0.0];
    [unitMenu setZPosition:10.0];
    [self setZPosition:9999.0];*/
    
    SKSpriteNode *hpBarBG = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.9 alpha:0.3] size:CGSizeMake(80.0, 7.0)];
    [hpBarBG setZPosition:0.0];
    hpBarValue = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:hpBarBG.size];
    [hpBarValue setAnchorPoint:CGPointMake(0.0, 0.5)];
    [hpBarValue setPosition:CGPointMake(-hpBarBG.size.width/2, 0.0)];
    [hpBarValue setZPosition:1.0];
    hpBar = [SKNode node];
    [hpBar addChild:hpBarBG];
    [hpBar addChild:hpBarValue];
    [hpBar setPosition:CGPointMake(0.0, 27.0*[Functions deviceFactor])];
    [self addChild:hpBar];
    [hpBar setAlpha:0.0];
    
}

-(int)getMovement {
    return (_fuel >= _movement ? _movement : _fuel);
}

-(int)getWeaponRange {
    if (_primaryAmmo == 0) {
        return 1;
    }
    return _weaponRange;
}
-(void)updateCostMapWithMap: (NSArray*) levelCostMap {
    costMap = levelCostMap;
}

-(void)moveUnitFromStart:(NSArray *)path {
    counter = 0;
    [self moveUnit:path];
}

-(void)moveUnit:(NSArray*)path {
    PathFindNode *node = path[counter];
    PathFindNode *lastNode = path[path.count-1];
    SKNode *tileNode = [[LevelLoader loader] getLevelTilesNodesArray][lastNode->nodeY][lastNode->nodeX];
    _unitPositionOnWorld = tileNode.position;
    self.unitPosition = CGPointMake(lastNode->nodeX, lastNode->nodeY);
    counter++;
    [self runAction:[SKAction moveTo:((SKNode*)[[LevelLoader loader] getLevelTilesNodesArray][node->nodeY][node->nodeX]).position duration:0.4] completion:^{
        if (path.count-1 >= counter) {
            [self moveUnit:path];
        }
        else {
            [self setCanMove:NO];
            
            /*GameScene *gameScene = ((GameScene*)[LevelLoader loader].gameScene);
            [gameScene setSelectedUnit:self];
            [gameScene checkStructuresForAction];
            [gameScene unitAttack];*/
            
        }
    }];
}
/*
-(void)showUnitMenu {
    [self addChild:unitMenu];
    [unitMenu runAction:[SKAction scaleTo:1.0 duration:0.2] completion:^{
        [unitMenu runAction:unitMenuAction withKey:@"menuAction"];
    }];
}

-(void)hideUnitMenu {
    [unitMenu runAction:[SKAction scaleTo:0.0 duration:0.1] completion:^{
        [unitMenu removeFromParent];
    }];
}*/

-(BOOL)spaceIsBlocked:(int)x :(int)y
{
	//general-purpose method to return whether a space is blocked
    //NSLog(@"%d",((NSNumber*)costMap[y][x][_movementType]).intValue);
	if(((NSNumber*)costMap[y][x][_movementType]).intValue > _fuel) {
		return YES;
    }
	else {
		return NO;
    }
}

-(PathFindNode*)nodeInArray:(NSMutableArray*)a withX:(int)x Y:(int)y
{
	//Quickie method to find a given node in the array with a specific x,y value
	NSEnumerator *e = [a objectEnumerator];
	PathFindNode *n;
    
	while((n = [e nextObject])) {
		if((n->nodeX == x) && (n->nodeY == y)) {
			return n;
		}
	}
    
	return nil;
}
-(PathFindNode*)lowestCostNodeInArray:(NSMutableArray*)a
{
	//Finds the node in a given array which has the lowest cost
	PathFindNode *n, *lowest;
	lowest = nil;
	NSEnumerator *e = [a objectEnumerator];
    
	while((n = [e nextObject])) {
		if(lowest == nil) {
			lowest = n;
		}
		else {
			if(n->cost < lowest->cost) {
				lowest = n;
			}
		}
	}
	return lowest;
}

-(NSArray*)findPath:(int)startX :(int)startY :(int)endX :(int)endY {
	//find path function. takes a starting point and end point and performs the A-Star algorithm
	//to find a path, if possible. Once a path is found it can be traced by following the last
	//node's parent nodes back to the start
    
    
    if((startX == endX) && (startY == endY))
        return nil; //make sure we're not already there
    
    
	//int x,y;
	int newX,newY;
	int currentX,currentY;
	NSMutableArray *openList, *closedList;
    
    int movement = [self getMovement];
    
	openList = [NSMutableArray array]; //array to hold open nodes
	closedList = [NSMutableArray array]; //array to hold closed nodes
    
	PathFindNode *currentNode = nil;
	PathFindNode *aNode = nil;
    
	//create our initial 'starting node', where we begin our search
	PathFindNode *startNode = [PathFindNode node];
	startNode->nodeX = startX;
	startNode->nodeY = startY;
	startNode->parentNode = nil;
	startNode->cost = 0;
	//add it to the open list to be examined
	[openList addObject: startNode];

	while([openList count]) {
		//while there are nodes to be examined...
        
		//get the lowest cost node so far:
		currentNode = [self lowestCostNodeInArray: openList];
        
        //optimization if the current path is further away then we can move the path cannot be created
        if ((currentNode->cost) > movement) {
            return nil;
        }
        //NSLog(@"currentNode: %d x %d",currentNode->nodeX,currentNode->nodeY);
		if((currentNode->nodeX == endX) && (currentNode->nodeY == endY)) {
			//if the lowest cost node is the end node, we've found a path
            
			//********** PATH FOUND ********************
            
			//***************************************** //
            //NSLog(@"PATH:");
            //NSLog(@"FINISH: %d x %d",currentNode->nodeX,currentNode->nodeY);
            NSMutableArray *pathResult = [NSMutableArray array];
            [pathResult addObject:currentNode];
			aNode = currentNode->parentNode;
			while(aNode->parentNode != nil) {
				//NSLog(@"%d x %d",aNode->nodeX,aNode->nodeY);
                [pathResult insertObject:aNode atIndex:0];
				aNode = aNode->parentNode;
            }
            //NSLog(@"START: %d x %d",aNode->nodeX,aNode->nodeY);
            //***** CHECK IF PATH IS TOO LONG ***** //
			return pathResult;
			//***************************************** //
		}
        else {
            //...otherwise, examine this node.
            //remove it from open list, add it to closed:
            [closedList addObject: currentNode];
            [openList removeObject: currentNode];
            
            //lets keep track of our coordinates:
            currentX = currentNode->nodeX;
            currentY = currentNode->nodeY;
            
            NSArray* neighbors = [self getNeighborsForTile:CGPointMake(currentX, currentY)];
            //check neighboring tiles
            for (int i = 0; i < neighbors.count; i++) {
                newX = [((NSValue*)neighbors[i]) CGPointValue].x;
                newY = [((NSValue*)neighbors[i]) CGPointValue].y;
                //if the node isn't in the open list...
                if(![self nodeInArray: openList withX:newX Y:newY]) {
                    //and its not in the closed list...
                    if(![self nodeInArray: closedList withX:newX Y:newY]) {
                        //and the space isn't blocked
                        if(![self spaceIsBlocked:newX :newY]) {
                            //if the tile is reachable with the given (unit) movement in the unitCostMap
                            //NSLog(@"%d",((NSNumber*)costMap[newY][newX][_movementType]).intValue);
                            if ((currentNode->cost + ((NSNumber*)costMap[newY][newX][_movementType]).intValue) <= movement) {
                                //add it to posible arrays and add it to open list
                                aNode = [PathFindNode node];
                                aNode->nodeX = newX;
                                aNode->nodeY = newY;
                                aNode->parentNode = currentNode;
                                aNode->cost = currentNode->cost + ((NSNumber*)costMap[newY][newX][_movementType]).intValue;
                                [openList addObject: aNode];
                            }
                        }
                    }
                }
            }
		}
	}
	//**** NO PATH FOUND *****
    NSLog(@"!!! no path found !!! x: %d y: %d",endX,endY);
    return nil;
}

-(int)checkPath:(int)endX :(int)endY with:(NSArray*)costArray {
    costMap = costArray;
    //NSLog(@"%@ %d", costMap[endY][endX][_movementType], _movementType);
    if ([costMap[endY][endX][_movementType]integerValue] == 9999) {
        return 0;
    }
    //NSLog(@"checkPATH: %d x %d",endX,endY);
    NSArray *path = [self findPath:(int)_unitPosition.y :(int)_unitPosition.x :endY :endX];
    //NSLog(@"Path Length: %ld",path.count);
    return (int)path.count;
}

-(NSArray*)findNodesInMovementRange {
    
    int movement = [self getMovement];
    
    int newX,newY;
    int currentX,currentY;
    NSMutableArray *openList, *closedList, *posibleMoves;
        
    openList = [NSMutableArray array]; //array to hold open nodes
    closedList = [NSMutableArray array]; //array to hold closed nodes
    posibleMoves = [NSMutableArray array]; //array to hold posible moves for unit
    
    PathFindNode *currentNode = nil;
    PathFindNode *aNode = nil;
    
    //create our initial 'starting node', where we begin our search
    PathFindNode *startNode = [PathFindNode node];
    startNode->nodeX = _unitPosition.x;
    startNode->nodeY = _unitPosition.y;
    startNode->parentNode = nil;
    startNode->cost = 0;
    //add it to the open list to be examined
    [openList addObject: startNode];
    
    while([openList count]) {
        //while there are nodes to be examined...
        
        //get the lowest cost node so far:
        currentNode = [self lowestCostNodeInArray: openList];
        //...otherwise, examine this node.
        //remove it from open list, add it to closed:
        [closedList addObject: currentNode];
        [openList removeObject: currentNode];
        
        //lets keep track of our coordinates:
        currentX = currentNode->nodeX;
        currentY = currentNode->nodeY;
        
        NSArray* neighbors = [self getNeighborsForTile:CGPointMake(currentX, currentY)];
        //check neighboring tiles
        for (int i = 0; i < neighbors.count; i++) {
            newX = [((NSValue*)neighbors[i]) CGPointValue].x;
            newY = [((NSValue*)neighbors[i]) CGPointValue].y;
            //if the node isn't in the open list...
            if(![self nodeInArray: openList withX:newX Y:newY]) {
                //and its not in the closed list...
                if(![self nodeInArray: closedList withX:newX Y:newY]) {
                    //and the space isn't blocked
                    if(![self spaceIsBlocked:newX :newY]) {
                        //if the tile is reachable with the given (unit) movement in the unitCostMap                    
                        if ((currentNode->cost + ((NSNumber*)costMap[newY][newX][_movementType]).intValue) <= movement) {
                            //add it to posible arrays and add it to open list
                            aNode = [PathFindNode node];
                            aNode->nodeX = newX;
                            aNode->nodeY = newY;
                            aNode->parentNode = currentNode;
                            aNode->cost = currentNode->cost + ((NSNumber*)costMap[newY][newX][_movementType]).intValue;
                            [openList addObject: aNode];
                            [posibleMoves addObject:[NSValue valueWithCGPoint:CGPointMake(aNode->nodeY, aNode->nodeX)]];
                        }
                    }
                }
            }
        }
    }
    //**** RETURN THE POSIBLE MOVES *****
    return posibleMoves;
}
/*
 
 [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)],
 [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)],
 [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)],
 [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)],
 [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)],
 [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)],
 */

-(BOOL)tileIsInArray:(CGPoint)tilePosition {
    
    NSArray *terrainMap = [[LevelLoader loader] getLevelTerrainMap];
    if (tilePosition.y < terrainMap.count && tilePosition.y >= 0 && tilePosition.x < ((NSArray*)terrainMap[(int)tilePosition.y]).count && tilePosition.x >= 0) {
        //NSLog(@"%@",terrainMap[(int)tilePosition.x][(int)tilePosition.y]);
        //NSLog(@"%@",terrainMap[(int)tilePosition.y][(int)tilePosition.x]);
        if (terrainMap[(int)tilePosition.y][(int)tilePosition.x] != [NSNull null]) {
            return YES;
        }
    }
    
    return NO;
}

-(NSArray*)getNeighborsForTile:(CGPoint)tilePosition {
    NSMutableArray *neighbors = [NSMutableArray array];
    
    //check if neighbors are on the map and valid places
    //TODO optimize this if needed
    if ([self tileIsInArray:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInArray:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInArray:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInArray:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInArray:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]];
    }
    
    if ([self tileIsInArray:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]];
    }
    return neighbors;
}


-(void)setCanMove:(BOOL)canMove {
    _canMove = canMove;
    if (canMove) {
        _movement = _maxMovement;
        [self runAction:[SKAction colorizeWithColor:_unitColor colorBlendFactor:0.5 duration:0.0]];
        //NSLog(@"COLOR: %@",_unitColor);
    }
    else {
        [self runAction:[SKAction colorizeWithColorBlendFactor:0.0 duration:0.0]];
        //NSLog(@"decolor");
    }
}


-(void)setHealth:(int)health {
    _health = health;
    if (_health <= 0) {
        [self removeFromParent];
        [[GameScene game]unitDiedInCombat:self];
    }
    if (_health != _maxHealth && hpBar.alpha == 0.0) {
        [hpBar runAction:[SKAction fadeInWithDuration:0.05]];
    }
    [hpBarValue runAction:[SKAction resizeToWidth:80.0*(health/(CGFloat)_maxHealth) duration:0.2]];
    if (_health < _maxHealth*0.2) {
        [hpBarValue runAction:[SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:1.0 duration:0.1]];
    }
    else if (_health < _maxHealth*0.5) {
        [hpBarValue runAction:[SKAction colorizeWithColor:[UIColor orangeColor] colorBlendFactor:1.0 duration:0.1]];
    }
    
    
}

-(CGFloat)attackDamageOnUnit:(Unit*)attackedUnit {
    NSLog(@"type:%d, primary: %d, secondary: %d", attackedUnit.type,[_primaryWeaponDamage[attackedUnit.type]intValue],[_secondaryWeaponDamage[attackedUnit.type]intValue]);
    if ([_primaryWeaponDamage[attackedUnit.type]intValue] != 0) {
        return (CGFloat)([_primaryWeaponDamage[attackedUnit.type]intValue] - attackedUnit.armor)*(_health/(CGFloat)_maxHealth);
    }
    else {
        /*
         //TODO da se steje tut defence od terena oz zgradbe na kateri stoji enota
        //problem rata ce je structure tut gor (ne sme gledat od terraina defence)
        NSArray *c = (NSArray*)[[LevelLoader loader] getLevelTilesNodesArray];
        int d =(int) attackedUnit.unitPosition.y;
        int e = (int) attackedUnit.unitPosition.x;
        Structure *a = c[d][e][1];
        int b = [[a.userData objectForKey:@"ExtraDefence"]intValue];
        NSLog(@"buiiu %d",b);*/
        return (CGFloat)([_secondaryWeaponDamage[attackedUnit.type]intValue] - attackedUnit.armor)*(_health/(CGFloat)_maxHealth);
    }
}

+(void)unit:(Unit*)firstUnit attacksUnit:(Unit*)secondUnit {
    //NSLog(@"attack: %f",[firstUnit attackDamageOnUnit:secondUnit]);
    firstUnit.canMove = NO;
    firstUnit.canAttack = NO;
    secondUnit.health -= [firstUnit attackDamageOnUnit:secondUnit];
    if (secondUnit.health > 0) {
        firstUnit.health -= [secondUnit attackDamageOnUnit:firstUnit];
    }
}

-(void)setUnitPosition:(CGPoint)unitPosition {
    _unitPosition = unitPosition;
    if (loadedUnit) {
        [loadedUnit setUnitPosition:unitPosition];
    }
}

-(void)loadUnit:(Unit*)unit {
    //NSLog(@"unit... %@",unit);
    loadedUnit = unit;
    [unit setPosition:self.position];
    [unit setUnitPosition:self.unitPosition];
    [unit removeFromParent];
}

-(Unit*)unloadUnit {
    Unit *tmpUnit = loadedUnit;
    loadedUnit = nil;
    return tmpUnit;
}

-(Unit*)getLoadedUnit {
    return loadedUnit;
}

-(BOOL)hasLoadedUnit {
    if (loadedUnit) {
        return YES;
    }
    return NO;
}

-(BOOL)tile: (int) tileNum isBlockedOnPath: (NSArray*)path {
    //get the neighbors
    PathFindNode *node = path[tileNum];
    NSArray* neighbors = [self getNeighborsForTile:CGPointMake(node->nodeX,node->nodeY)];
    //check if the neighbors have enemy units that are in the fog
    for (int i = 0; i < neighbors.count; i++) {
        CGPoint posTemp = [neighbors[i] CGPointValue];
        if ([[GameScene game] tileHasUnits:posTemp]) {
            return YES;
        }
    }
    return NO;
}

-(NSArray*)isAmbushedOnWayToX:(int)endX Y:(int)endY with:(NSArray*)levelCostMap {
    //TODO check each path tile if the unit is ambushed and return the path to the ambush
    costMap = levelCostMap;
    NSArray *path = [self findPath:_unitPosition.x :_unitPosition.y :endX :endY];
    if (!path) {
        NSLog(@"NO PATH");
        return  path;
    }
    for (int i = 0; i < path.count; i++) {
        PathFindNode *node = path[i];
        //NSLog(@"path part %d : (%d,%d)",i,node->nodeX,node->nodeY);
        if ([self tile:i isBlockedOnPath:path]) {
            NSMutableArray *pathTemp = [NSMutableArray array];
            [pathTemp addObject:path[0]];
            for (int j = 1; j <= i; j++) {
                [pathTemp addObject:path[j]];
            }
            _canAttack = NO;
            counter = 0;
            return pathTemp;
        }
    }
    
    return  path;
}

@end
