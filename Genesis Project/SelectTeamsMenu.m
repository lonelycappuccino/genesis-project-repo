//
//  SelectTeamsMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "SelectTeamsMenu.h"
#import "SetRulesMenu.h"
#import "Functions.h"
#import "GameScene.h"

@implementation SelectTeamsMenu {
    SetRulesMenu_iPad *setRulesMenu;
    SKNode *teamsNode;
    SKNode *createCorpMenu;
    BOOL createCorpMenuVisible;
    KeyboardView *keyboard;
    NSMutableArray *teams; //teams nodes
    SKNode *colorChooser;
    NSDictionary *levelDictionary;
    SKSpriteNode *selectTeamMenu;
    NSMutableArray *teamsArray; //teams to choose
    SelectTeamsMenu_selector *selector;
    SKShapeNode *doneButton, *backButton;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        teamsArray = [NSMutableArray arrayWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"teams.data"]];
        self.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:40.0/255.0 blue:45.0/255.0 alpha:1.0];
        
        SKSpriteNode *titleBg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.98 alpha:0.9] size:CGSizeMake(self.size.width, 50.0*factor)];
        [titleBg setPosition:CGPointMake(self.size.width/2, self.size.height-titleBg.size.height/2)];
        [titleBg setZPosition:1.0];
        [self addChild:titleBg];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:@"Guilds"];
        [titleLabel setFontSize:32.0*factor];
        [titleLabel setFontColor:[UIColor blackColor]];
        [titleLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [titleLabel setZPosition:1.0];
        [titleBg addChild:titleLabel];
        
        doneButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        doneButton.position = CGPointMake(self.size.width-doneButton.frame.size.width/2-5.0*factor, self.size.height-doneButton.frame.size.height/2-5.0*factor);
        [doneButton setFillColor:[UIColor clearColor]];
        [doneButton setStrokeColor:[UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:1.0]];
        [doneButton setLineWidth:0.7*factor];
        [doneButton setZPosition:100.0];
        [self addChild:doneButton];
        
        [doneButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *doneLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        doneLabel.fontColor = [UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:1.0];
        doneLabel.fontSize = 12.0*[Functions deviceFactor];
        doneLabel.text = @"done";
        doneLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [doneLabel setZPosition:100.1];
        [doneButton addChild:doneLabel];
        /*
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7*[Functions deviceFactor])];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        */
        backButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        backButton.position = CGPointMake(backButton.frame.size.width/2+5.0*factor, self.size.height-backButton.frame.size.height/2-5.0*factor);
        [backButton setFillColor:[UIColor clearColor]];
        [backButton setStrokeColor:[UIColor redColor]];
        [backButton setLineWidth:0.7*factor];
        [backButton setZPosition:100.0];
        [self addChild:backButton];
        
        [backButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *backLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        backLabel.fontColor = [UIColor redColor];
        backLabel.fontSize = 12.0*[Functions deviceFactor];
        backLabel.text = @"back";
        backLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [backLabel setZPosition:100.1];
        [backButton addChild:backLabel];
        
        setRulesMenu = [SetRulesMenu_iPad sceneWithSize:self.size];
        selector = [SelectTeamsMenu_selector sceneWithSize:self.size];
        [selector setMenu:self];
        createCorpMenuVisible = NO;
        keyboard = [[KeyboardView alloc]init];
        
        SKLabelNode *tipLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [tipLabel setText:@"Tap on the team"];
        [tipLabel setFontSize:12.0*factor];
        [tipLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [tipLabel setZPosition:100.0];
        [tipLabel setPosition:CGPointMake(self.size.width/2, 10.0*factor)];
        [self addChild:tipLabel];
    }
    return self;
}

-(void)createMenuForMap:(NSDictionary*)levelDict {
    levelDictionary = levelDict;
    _numberOfPlayers = [[levelDictionary objectForKey:@"Players"]intValue];
    teamsNode = [SKNode node];
    teams = [NSMutableArray array];
    NSArray *colors = @[[UIColor redColor],[UIColor blueColor],[UIColor greenColor],[UIColor yellowColor]];
    for (int i = 0; i < _numberOfPlayers; i++) {
        SKNode *teamContainer = [SKNode node];
        SKSpriteNode *bgNode = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_playerBg"];
        [bgNode setZPosition:0.0];
        [teamContainer addChild:bgNode];
        
        SKSpriteNode *playerIndicator = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [playerIndicator setPosition:CGPointMake(bgNode.size.width/2, bgNode.size.height/2)];
        [playerIndicator setColor:[UIColor lightGrayColor]];
        [playerIndicator setColorBlendFactor:1.0];
        [playerIndicator setZPosition:1.0];
        [playerIndicator setName:@"playerType"];
        [teamContainer addChild:playerIndicator];
        
        SKSpriteNode *playerType = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_human"];
        [playerType setName:@"type"];
        [playerType setZPosition:2.0];
        [playerType setPosition:playerIndicator.position];
        [teamContainer addChild:playerType];
        NSMutableDictionary *teamDict = [NSMutableDictionary dictionary];
        [teamDict setValue:[NSNumber numberWithBool:YES] forKey:@"playerTypeIsHuman"];
        
        SKLabelNode *teamName = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [teamName setFontColor:[UIColor darkGrayColor]];
        [teamName setFontSize:15.0*factor];
        [teamName setText:[NSString stringWithFormat:@"Team %d",i+1]];
        [teamName setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [teamName setPosition:CGPointMake(-22.5*factor, 5.0*factor)];
        [teamName setZPosition:1.0];
        [teamName setName:@"Name"];
        [teamContainer addChild:teamName];
        [teamDict setValue:teamName.text forKey:@"Name"];
        [teamContainer setUserData:teamDict];
        
        SKSpriteNode *logoNode = [SKSpriteNode spriteNodeWithColor:[UIColor purpleColor] size:CGSizeMake(50.0*factor, 50.0*factor)];
        [logoNode setPosition:CGPointMake(-bgNode.size.width/2+logoNode.size.width/2-5.0*factor, bgNode.size.height/2-logoNode.size.height/2+5.0*factor)];
        [logoNode setZPosition:1.0];
        [logoNode setName:@"Mark"];
        [teamContainer addChild:logoNode];
        
        SKSpriteNode *color = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [color setName:@"Color"];
        [color setColor:colors[i]];
        [color setColorBlendFactor:1.0];
        [color setZPosition:1.0];
        [color setPosition:CGPointMake(bgNode.size.width/2-color.size.width/2+10.0*factor, -bgNode.size.height/2+color.size.height/2-10.0*factor)];
        [teamContainer addChild:color];
        
        [teamContainer setPosition:CGPointMake(-115.0*factor+230.0*(i > 1 ? i-2 : i)*factor, (i > 1 ? -115.0*factor : 0.0))];
        [teams addObject:teamContainer];
        [teamsNode addChild:teamContainer];
    }
    [teamsNode setPosition:CGPointMake(self.size.width/2, (_numberOfPlayers > 2 ? self.size.height/2+115.0 : self.size.height/2))];
    [self addChild:teamsNode];
    
    NSArray *colorChooserColors = @[[UIColor redColor],[UIColor greenColor],[UIColor blueColor],[UIColor purpleColor],[UIColor yellowColor],[UIColor whiteColor],[UIColor grayColor]];
    colorChooser = [SKNode node];
    for (int i = 0; i < colorChooserColors.count; i++) {
        SKSpriteNode *colorCircle = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [colorCircle setColorBlendFactor:1.0];
        [colorCircle setColor:colorChooserColors[i]];
        [colorCircle setAnchorPoint:CGPointMake(0.0, 1.6)];
        [colorCircle setZRotation:2*M_PI/colorChooserColors.count*i];
        [colorCircle setZPosition:1.0];
        [colorChooser addChild:colorCircle];
    }
    [colorChooser setZPosition:1000000.0];
    [colorChooser setZRotation:-M_PI/2];
    [colorChooser setHidden:YES];
    [colorChooser setScale:0.0];
    [colorChooser setAlpha:0.0];
    //[self addChild:colorChooser];
    /*
    SKSpriteNode *createCorpButton = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.9 alpha:1.0] size:CGSizeMake(200.0, 60.0)];
    [createCorpButton setPosition:CGPointMake(130.0, 60.0)];
    [createCorpButton setName:@"createCorpButton"];
    [self addChild:createCorpButton];
    
    SKLabelNode *createCorpLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [createCorpLabel setFontColor:[UIColor darkGrayColor]];
    [createCorpLabel setFontSize:30.0];
    [createCorpLabel setText:@"Create Corp"];
    [createCorpLabel setPosition:CGPointMake(0.0, -12.0)];
    [createCorpButton addChild:createCorpLabel];
    */
    //[self createCorpMenu];
    //[self createSelectTeamMenu];
}

-(void)createSelectTeamMenu {
    selectTeamMenu = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(900.0, 700.0)];
    [selectTeamMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [selectTeamMenu setZPosition:99999.0];
    [self addChild:selectTeamMenu];
    int counter = 0;
    for (NSDictionary *teamDict in teamsArray) {
        SKSpriteNode *teamBG = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:CGSizeMake(200.0, 200.0)];
        [teamBG setPosition:CGPointMake(-selectTeamMenu.size.width/2+20.0+teamBG.size.width/2+(20.0+teamBG.size.width)*counter, selectTeamMenu.size.height/2-20.0-teamBG.size.height/2)];
        [selectTeamMenu addChild:teamBG];
        counter++;
    }
}
/*
-(void)createCorpMenu {
    createCorpMenu = [SKNode node];
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.9 alpha:1.0] size:CGSizeMake(750.0, 500.0)];
    [createCorpMenu addChild:bg];
    
    //SKSpriteNode *logo = [SKSpriteNode spriteNodeWithColor:[UIColor lightGrayColor] size:CGSizeMake(200.0, 200.0)];
    SKSpriteNode *logo = [SKSpriteNode spriteNodeWithImageNamed:@"logo_0"];
    [logo setName:@"primary"];
    [logo setPosition:CGPointMake(-bg.size.width/2+logo.size.width/2+40.0, 100.0)];
    SKSpriteNode *logoBG = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:logo.size];
    [logoBG setPosition:logo.position];
    [logoBG setName:@"secondary"];
    [createCorpMenu addChild:logoBG];
    [createCorpMenu addChild:logo];
    
    SKLabelNode *corporationNameTitleLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [corporationNameTitleLabel setFontSize:30.0];
    [corporationNameTitleLabel setFontColor:[UIColor grayColor]];
    [corporationNameTitleLabel setText:@"Corporation Name"];
    [corporationNameTitleLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [corporationNameTitleLabel setPosition:CGPointMake(-70.0, 130.0)];
    [createCorpMenu addChild:corporationNameTitleLabel];
    
    SKSpriteNode *inputFrame = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.85 alpha:1.0] size:CGSizeMake(300.0, 50.0)];
    [inputFrame setPosition:CGPointMake(80.0, 90.0)];
    [inputFrame setName:@"inputFrame"];
    [createCorpMenu addChild:inputFrame];
    
    SKLabelNode *inputTextLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [inputTextLabel setFontColor:[UIColor blackColor]];
    [inputTextLabel setFontSize:45.0];
    [inputTextLabel setText:@"LameCorp"];
    [inputTextLabel setPosition:CGPointMake(inputFrame.position.x, inputFrame.position.y-15.0)];
    [inputTextLabel setName:@"inputLabel"];
    [createCorpMenu addChild:inputTextLabel];

    SKLabelNode *primaryColorLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [primaryColorLabel setFontColor:[UIColor grayColor]];
    [primaryColorLabel setFontSize:30.0];
    [primaryColorLabel setText:@"Primary Color"];
    [primaryColorLabel setPosition:CGPointMake(0.0, -50.0)];
    [createCorpMenu addChild:primaryColorLabel];
    
    SKLabelNode *secondaryColorLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [secondaryColorLabel setFontColor:[UIColor grayColor]];
    [secondaryColorLabel setFontSize:30.0];
    [secondaryColorLabel setText:@"Secondary Color"];
    [secondaryColorLabel setPosition:CGPointMake(0.0, -150.0)];
    [createCorpMenu addChild:secondaryColorLabel];
    /*
    NSArray *colors = @[[UIColor blueColor],
                        [UIColor redColor],
                        [UIColor greenColor],
                        [UIColor colorWithRed:0.0 green:1.0 blue:239.0/255.0 alpha:1.0],
                        [UIColor orangeColor],
                        [UIColor colorWithRed:50.0/255.0 green:205.0/255.0 blue:50.0/255.0 alpha:1.0],
                        [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0],
                        [UIColor purpleColor],
                        [UIColor colorWithRed:233.0/255.0 green:191.0/255.0 blue:0.0 alpha:1.0],
                        [UIColor colorWithRed:89.0/255.0 green:76.0/255.0 blue:28.0/255.0 alpha:1.0],
                        [UIColor colorWithRed:224.0/255.0 green:176.0/255.0 blue:1.0 alpha:1.0],
                        [UIColor whiteColor]];
    for (int j = 0; j < 2; j++) {
        
        for (int i = 0; i < colors.count; i++) {
            SKShapeNode *circle = [SKShapeNode shapeNodeWithCircleOfRadius:25.0];
            [circle setFillColor:colors[i]];
            *//*
            if (j > 0) {
                CGFloat hue, sat, bright;
                [colors[i] getHue:&hue saturation:&sat brightness:&bright alpha:nil];
                [circle setFillColor:[UIColor colorWithHue:hue saturation:sat-0.1 brightness:bright-0.1 alpha:1.0]];
            }
             */
        /*
            [circle setLineWidth:0.0];
            if ([colors[i] isEqual:[UIColor whiteColor]]) {
                [circle setStrokeColor:[UIColor darkGrayColor]];
                [circle setLineWidth:1.0];
            }
            [circle setPosition:CGPointMake(-bg.size.width/2+35.0+60.0*i, -85.0-100.0*j)];
            [circle setName:[NSString stringWithFormat:@"color%d",j]];
            [createCorpMenu addChild:circle];
        }
    
    }
    */
    /*
    SKSpriteNode *hueGradient1 = [SKSpriteNode spriteNodeWithImageNamed:@"hueGradient"];
    [hueGradient1 setPosition:CGPointMake(0.0, -80.0)];
    [hueGradient1 setName:@"hue1"];
    [createCorpMenu addChild:hueGradient1];
    
    [createCorpMenu setPosition:CGPointMake(self.size.width/2, -self.size.height/2)];
    [createCorpMenu setZPosition:10000.0];
    [self addChild:createCorpMenu];
}
*/
-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
    [view addSubview:keyboard];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
    
    UIPanGestureRecognizer *slide = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(slideHandler:)];
    [view addGestureRecognizer:slide];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([backButton containsPoint:location]) {
        [self.view presentScene:_previousMenu transition:[SKTransition crossFadeWithDuration:0.3]];
        return;
    }
    if ([doneButton containsPoint:location]) {
        NSLog(@"start loading");
        [self removeAllChildren];
        
        SKSpriteNode *dim = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:self.size];
        [dim setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:dim];
        [dim setZPosition:9999999.0];
        
        SKLabelNode *loadinglabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [loadinglabel setFontColor:[UIColor whiteColor]];
        [loadinglabel setFontSize:25.0*factor];
        [loadinglabel setPosition:CGPointMake(0.0, 75.0*factor)];
        [loadinglabel setText:@"LOADING"];
        [dim addChild:loadinglabel];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10000000), dispatch_get_main_queue(), ^{
            NSLog(@"load scene");
            NSMutableArray *players = [NSMutableArray arrayWithCapacity:teamsArray.count];
            for (SKNode *team in teams) {
                NSMutableDictionary *teamDict = [NSMutableDictionary dictionaryWithDictionary:team.userData];
                [teamDict setValue:((SKSpriteNode*)[team childNodeWithName:@"Color"]).color forKey:@"Color"];
                [players addObject:teamDict];
            }
            GameScene *gameScene = [GameScene gameSceneWithSize:self.size];
            [gameScene initGameWithDict:levelDictionary andSettings:nil forPlayers:players];
            [self.view presentScene:gameScene transition:[SKTransition crossFadeWithDuration:0.3]];
            NSLog(@"present scene");
        });
        return;
    }
    /*
    if ([[self childNodeWithName:@"createCorpButton"]containsPoint:location]) {
        //NSLog(@"CREATE CORP BUTTON PRESSED");
        [createCorpMenu runAction:[SKAction moveToY:(createCorpMenuVisible ? -self.size.height/2 : self.size.height/2) duration:0.3]];
        createCorpMenuVisible = !createCorpMenuVisible;
    }*/
    if (createCorpMenuVisible) {
        location = [createCorpMenu convertPoint:location fromNode:self];
        
        if ([[createCorpMenu childNodeWithName:@"inputFrame"]containsPoint:location]) {
            if (![keyboard isFirstResponder]) {
                //BOOL success = [keyboard becomeFirstResponder];
                //NSLog(@"become first responder: %d",success);
            }
            else {
                [keyboard resignFirstResponder];
            }
            [keyboard setTextlabel:((SKLabelNode*)[createCorpMenu childNodeWithName:@"inputLabel"])];
            return;
        }
        
        if ([[createCorpMenu childNodeWithName:@"hue1"]containsPoint:location]) {
            CGPoint location = [[createCorpMenu childNodeWithName:@"hue1"] convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
            CGFloat hue = ([createCorpMenu childNodeWithName:@"hue1"].frame.size.width/2+location.x)/[createCorpMenu childNodeWithName:@"hue1"].frame.size.width;
            //NSLog(@"SLIDE, HUE: %f, locationY: %f",hue,location.x);
            [[createCorpMenu childNodeWithName:@"primary"]runAction:[SKAction colorizeWithColor:[UIColor colorWithHue:hue saturation:1.0 brightness:1.0 alpha:1.0] colorBlendFactor:1.0 duration:0.0]];
        }
        
        /*
        [createCorpMenu enumerateChildNodesWithName:@"color0" usingBlock:^(SKNode *node, BOOL *stop) {
            SKShapeNode *circle = (SKShapeNode*)node;
            if ([circle containsPoint:location]) {
                NSLog(@"COLOR: %@",circle.fillColor);
                [[createCorpMenu childNodeWithName:@"primary"]runAction:[SKAction colorizeWithColor:circle.fillColor colorBlendFactor:1.0 duration:0.3]];
                *stop = YES;
            }
        }];
        [createCorpMenu enumerateChildNodesWithName:@"color1" usingBlock:^(SKNode *node, BOOL *stop) {
            SKShapeNode *circle = (SKShapeNode*)node;
            if ([circle containsPoint:location]) {
                NSLog(@"COLOR: %@",circle.fillColor);
                [[createCorpMenu childNodeWithName:@"secondary"]runAction:[SKAction colorizeWithColor:circle.fillColor colorBlendFactor:1.0 duration:0.3]];
                *stop = YES;
            }
        }];
         */
        return;
    }
    /*
    if ([doneButton containsPoint:location]) {
        NSDictionary *dictionary = [self createDictionaryForLevelAndPlayers];
    }*/
    
    if (!colorChooser.isHidden && [colorChooser containsPoint:[colorChooser.parent convertPoint:location fromNode:self]]) {
        for (SKSpriteNode *colorCircle in colorChooser.children) {
            if ([colorCircle containsPoint:[colorCircle.parent convertPoint:location fromNode:self]] && colorCircle.alpha == 1.0) {
                [((SKSpriteNode*)[colorChooser.parent childNodeWithName:@"Color"])setColor:colorCircle.color];
                [colorChooser runAction:[SKAction group:@[[SKAction scaleTo:0.0 duration:0.1],[SKAction fadeOutWithDuration:0.08]]] completion:^{
                    [colorChooser removeAllActions];
                    [colorChooser setHidden:YES];
                    [colorChooser removeFromParent];
                }];
                return;
            }
        }
    }
    else {
        for (SKNode *teamContainer in teams) {
            CGPoint locationInContainer = [teamContainer convertPoint:location fromNode:self];
            if ([teamContainer containsPoint:[teamContainer.parent convertPoint:location fromNode:self]]) {
                //NSLog(@"on team container");
                if ([[teamContainer childNodeWithName:@"Color"]containsPoint:locationInContainer] && ![colorChooser.parent isEqual:teamContainer]) {
                    if (colorChooser.parent) {
                        [colorChooser removeFromParent];
                    }
                    //NSLog(@"tap color");
                    [colorChooser setPosition:[teamContainer childNodeWithName:@"Color"].position];
                    [colorChooser setHidden:NO];
                    [colorChooser runAction:[SKAction group:@[[SKAction scaleTo:1.0 duration:0.2],[SKAction fadeInWithDuration:0.08]]] completion:^{
                        [colorChooser runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.98 duration:1.0],[SKAction scaleTo:1.0 duration:0.9]]]]];
                    }];
                    
                    for (SKSpriteNode *circle in colorChooser.children) {
                        [circle setAlpha:1.0];
                        for (SKNode *team in teams) {
                            if ([((SKSpriteNode*)[team childNodeWithName:@"Color"]).color isEqual:circle.color] && ![team isEqual:teamContainer]) {
                                [circle setAlpha:0.2];
                                break;
                            }
                        }
                    }
                    
                    [teamContainer addChild:colorChooser];
                    return;
                }
                else if ([[teamContainer childNodeWithName:@"playerType"]containsPoint:locationInContainer]) {
                    //NSLog(@"PLAYER TYPE: HUMAN / AI");
                    BOOL type = ![[teamContainer.userData objectForKey:@"playerTypeIsHuman"]boolValue];
                    if (type) {
                        [((SKSpriteNode*)[teamContainer childNodeWithName:@"type"]) setTexture:[SKTexture textureWithImageNamed:@"playersMenu_human"]];
                    }
                    else {
                        [((SKSpriteNode*)[teamContainer childNodeWithName:@"type"]) setTexture:[SKTexture textureWithImageNamed:@"playersMenu_ai"]];
                    }
                    [((SKSpriteNode*)[teamContainer childNodeWithName:@"type"])setSize:((SKSpriteNode*)[teamContainer childNodeWithName:@"type"]).texture.size];
                    [teamContainer.userData setObject:[NSNumber numberWithBool:type] forKey:@"playerTypeIsHuman"];
                }
                else {
                    [selector passTeam:teamContainer];
                    [self.view presentScene:selector transition:[SKTransition moveInWithDirection:SKTransitionDirectionUp duration:0.2]];
                }
                break;
            }
        }
    }
    [colorChooser runAction:[SKAction group:@[[SKAction scaleTo:0.0 duration:0.1],[SKAction fadeOutWithDuration:0.08]]] completion:^{
        [colorChooser removeAllActions];
        [colorChooser setHidden:YES];
        [colorChooser removeFromParent];
    }];
}

-(NSDictionary*)createDictionaryForLevelAndPlayers {
    /*NSMutableArray *players = [NSMutableArray array];
    for (int i = 0; i < _numberOfPlayers; i++) {
        SKNode *teamContainer = teams[i];
        NSMutableDictionary *playerDict = [NSMutableDictionary dictionary];
    }*/
    return nil;
}

-(void)setTeam:(NSDictionary*)teamDict {
    
}

-(void)slideHandler:(UIPanGestureRecognizer*)gesture {
    CGPoint location = [createCorpMenu convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    if ([[createCorpMenu childNodeWithName:@"hue1"]containsPoint:location]) {
        location = [[createCorpMenu childNodeWithName:@"hue1"]convertPoint:location fromNode:createCorpMenu];
        CGFloat hue = ([createCorpMenu childNodeWithName:@"hue1"].frame.size.width/2+location.x)/[createCorpMenu childNodeWithName:@"hue1"].frame.size.width;
        //NSLog(@"SLIDE, HUE: %f, locationY: %f",hue,location.x);
        [[createCorpMenu childNodeWithName:@"primary"]runAction:[SKAction colorizeWithColor:[UIColor colorWithHue:hue saturation:1.0 brightness:1.0 alpha:1.0] colorBlendFactor:1.0 duration:0.0]];
    }
}

@end
