//
//  LevelLoader.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "LevelLoader.h"
#import "Functions.h"

@implementation LevelLoader {
    NSDictionary *gameDataDict;
    int levelNumber;
    NSMutableArray *levelTileNodes;
    NSArray *levelData;
    NSArray *terrainArray;
    SKSpriteNode *map;
    NSDictionary *levelDict;
    SKNode *gridOverlay;
    BOOL hasFog;
}

static LevelLoader *sharedInstance = nil;

+(LevelLoader*)loader {
    if (!sharedInstance) {
        sharedInstance = [[LevelLoader alloc]init];
    }
    return sharedInstance;
}

+(void)resetLevelLoader {
    sharedInstance = [[LevelLoader alloc]init];
}

-(instancetype)init {
    if (self = [super init]) {
        gameDataDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"GameData" ofType:@"plist"]];
        _levelArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"LevelData" ofType:@"plist"]];
        terrainArray = [gameDataDict objectForKey:@"Terrain"];
        _structuresArray = [gameDataDict objectForKey:@"Structures"];
        _unitsArray = [gameDataDict objectForKey:@"Units"];
    }
    return self;
}


-(SKSpriteNode*)createLevelWithDict:(NSDictionary*)levelDictionary andSettings:(NSDictionary*)gameSettings forPlayers:(NSArray*)players {
    
    levelDict = levelDictionary;
    levelNumber = (int)[_levelArray indexOfObject:levelDictionary];
    levelData = [levelDict objectForKey:@"LevelMap"];
    NSMutableArray *allStructuresOnMapArray = [NSMutableArray array];
    levelTileNodes = [NSMutableArray array];
    
    int sizeX = sqrt(3) / 2 * 64 * 2;
    int sizeY = 3.0 / 4.0 * 64 * 2;
    //TODO levelData[0] is not reliable coz of empty array cells, to get the width calculate the largest array in levelData[] WITHOUT the empty arrays
    _maxWidthOfMap = 0;
    for (NSArray *row in (NSArray*)levelData) {
        int counter = 0;
        for (NSArray *col in row) {
            //TODO ce ma mapa lukne umes pol lahko pride do napacne cifre (recimo da je _ _ _ 3 3 3 _ 3 3 mapa = 6 sirina, to bi zracunal 5)
            if (col.count != 0) {
                counter ++;
            }
        }
        if (counter > _maxWidthOfMap) {
            _maxWidthOfMap = counter;
        }
    }
    //TODO world size only works for rect maps, for other ones the size needs to be recalculated
    SKSpriteNode *world = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake((_maxWidthOfMap + 1) * sizeX, (levelData.count +1)*sizeY)];
    //NSLog(@"%d %d  aaa  %f %f", sizeX, sizeY, world.size.width, world.size.height);
    
    BOOL showGrid = [[gameSettings objectForKey:@"showGrid"]boolValue];
    
    gridOverlay = [SKNode node];
    
    NSArray *levelOffset = [levelDictionary objectForKey:@"LevelOffset"];
    NSLog(@"OFFSET: %@",levelOffset);
    if (levelOffset) {
        NSMutableArray *levelDataMut = [levelData deepMutableCopy];
        int counter = 0;
        for (NSNumber *offsetValue in levelOffset) {
            for (int i = 0; i < offsetValue.intValue; i++) {
                [levelDataMut[counter] insertObject:@[] atIndex:0];
            }
            counter++;
        }
        levelData = [NSArray arrayWithArray:levelDataMut];
    }
    
    int coll = 0, row = 0;
    for (NSArray *levelDataRow in levelData) {
        NSMutableArray *levelTileNodesRow = [NSMutableArray array];
        [levelTileNodes addObject:levelTileNodesRow];
        for (NSArray *values in levelDataRow) {
            int numOfValues = (int)values.count;
            NSLog(@"VALUES: %@",values);
            SKNode *tile;
            hasFog = YES;
            if (numOfValues > 0) {
                tile = [SKNode node];
                [tile setName:@"tile"];
                double xPos = (sizeX * coll + row * sizeX/2) - (_maxWidthOfMap -1.2) * sizeX ;
                double yPos = sizeY * row - levelData.count * sizeY * 0.5 + sizeY * 0.5;

                tile.position = CGPointMake(xPos, yPos);
                
                if (numOfValues >= 1) {
                    NSString *name, *textureName;
                    int tileNum;
                    int rotation = -1;
                    if ([values[0] isKindOfClass:[NSArray class]]) {
                        name = [terrainArray[((NSNumber*)values[0][0]).intValue] objectForKey:@"Name"];
                        tileNum = ((NSNumber*)values[0][0]).intValue;
                        
                        int textureValue = [values[0][1] intValue];
                        
                        rotation = textureValue/100;
                        int textureNum = textureValue-(rotation*100);
                        
                        textureName = [NSString stringWithFormat:@"plainf_%d",textureNum];
                    }
                    else {
                        name = [terrainArray[((NSNumber*)values[0]).intValue] objectForKey:@"Name"];
                        tileNum = ((NSNumber*)values[0]).intValue;
                        textureName = name;
                    }
                    
                    SKSpriteNode *node;
                    
                    
                    //NSLog(@"ROT: %d",rotation);
                    
                    if ([name isEqualToString:@"Plain"]) {
                        //NSLog(@"tex: %@",textureName);
                        node = [SKSpriteNode spriteNodeWithImageNamed:textureName];
                        [node.texture setUsesMipmaps:YES];
                        [node setZPosition:1.0];
                        
                        if (rotation != -1) {
                            [node setZRotation:(M_PI/3.0)*(rotation-1)];
                        }
                        
                        
                        
                    }
                    //-- TO DO --//
                    else if ([name isEqualToString:@"Forest"]) {
                        node = [SKSpriteNode spriteNodeWithImageNamed:@"plain"];
                        [node.texture setUsesMipmaps:YES];
                        
                        NSString *nextName, *upperName,*upperBackName;
                        if (coll+1 < levelDataRow.count) {
                            nextName = [terrainArray[((NSNumber*)((NSArray*)levelDataRow[coll+1])[0][0]).intValue] objectForKey:@"Name"];
                        }
                        if (row+1 < levelData.count) {
                            if (coll < ((NSArray*)levelData[row+1]).count) {
                                upperName = [terrainArray[((NSNumber*)((NSArray*)((NSArray*)levelData[row+1])[coll])[0][0]).intValue] objectForKey:@"Name"];
                            }
                            if (coll-1 > 0) {
                                NSArray *tmpArray = (NSArray*)((NSArray*)levelData[row+1])[coll-1];
                                if (tmpArray.count > 0) {
                                    upperBackName = [terrainArray[((NSNumber*)(tmpArray[0][0])).intValue] objectForKey:@"Name"];
                                }
                            }
                        }
                        //NSLog(@"nextName: %@ | upperName: %@",nextName,upperName);
                        int maxTrees = 8;
                        int minTrees = 4;
                        for (int i = 0; i < ([nextName isEqualToString:name] || [upperName isEqualToString:name] || [upperBackName isEqualToString:name] ? maxTrees : minTrees); i++) {
                            SKSpriteNode *tree = [SKSpriteNode spriteNodeWithImageNamed:@"forest_tree_3"];
                            [tree.texture setUsesMipmaps:YES];
                            //[tree setScale:0.5];
                            //[extra setScale:0.5];
                            //[tree setPosition:CGPointMake(-64.0+tree.size.width/2+(arc4random()%(int)(128-tree.size.width)), -64.0+tree.size.height/2+(arc4random()%(int)(128-tree.size.height)))];
                            
                            int angle = arc4random_uniform(360);
                            int radius = arc4random_uniform(25*[Functions deviceFactor]);
                            CGFloat x = radius * cos(angle);
                            CGFloat y = radius * sin(angle);
                            [tree setPosition:CGPointMake(x, y)];
                            
                            /*
                            [tree setPosition:CGPointMake(-64.0+tree.size.width/2+(arc4random_uniform((int)(128-tree.size.width))), -64.0+tree.size.height/2+(arc4random_uniform((int)(128-tree.size.height))))];
                            
                            if ([nextName isEqualToString:name]) {
                                [tree setPosition:CGPointMake(-64.0+tree.size.width/2+(arc4random_uniform(128)), tree.position.y)];
                            }
                            if ([upperName isEqualToString:name]) {
                                [tree setPosition:CGPointMake(-64.0+(arc4random_uniform(128)), -64.0+tree.size.height/2+(arc4random_uniform(128)))];
                            }
                            if ([upperBackName isEqualToString:name]) {
                                [tree setPosition:CGPointMake(tree.size.width/2+(arc4random_uniform(64)), -64.0+tree.size.height/2+(arc4random_uniform(128)))];
                            }
                            if ([upperName isEqualToString:name] && [upperBackName isEqualToString:name]) {
                                [tree setPosition:CGPointMake(tree.position.x, -64.0+tree.size.height/2+(arc4random_uniform(128)))];
                            }
                             */
                            [tree setZPosition:91.0+(128.0-tree.position.y)/128.0];
                            [node addChild:tree];
                        }
                    }
                    else {
                        //NSLog(@"TERRAIN: %@",terrainArray);
                        node = [SKSpriteNode spriteNodeWithImageNamed:name];
                        [node.texture setUsesMipmaps:YES];
                    }
                    
                    if (tileNum == 6 || tileNum == 5) {
                        [node setZPosition:1.1];
                    }
                    else if (tileNum == 0) {
                        [node setZPosition:1.2];
                    }
                    else if (tileNum == 7) {
                        [node setZPosition:1.21];
                    }
                    else if (tileNum == 9) {
                        [node setZPosition:1.4];
                    }
                    else if (tileNum == 1 || tileNum == 8 || tileNum == 2 || tileNum == 4) {
                        [node setZPosition:1.5];
                    }
                    else {
                        [node setZPosition:1.4];
                    }
                    
                    [node setName:@"terrain"];
                    [node setUserData:(NSMutableDictionary*)terrainArray[((NSNumber*)values[0][0]).intValue]];
                    [tile addChild:node];
                    
                    //[node setPosition:CGPointMake(node.position.x, node.position.y-node.size.height*0.2)];
                    //[node setYScale:0.8];
                }
                
                if (numOfValues >= 2 && ((NSArray*)values[1]).count > 0) {
                    Structure *node = [Structure initWithDictionary:_structuresArray[((NSNumber*)values[1][0]).intValue] andArray:_unitsArray];
                    [tile addChild:node];
                    int ownerNum = ((NSNumber*)values[1][1]).intValue;
                    [node setOwner:ownerNum];
                    if (ownerNum != -1) {
                        [node runAction:[SKAction colorizeWithColor:((Player*)players[ownerNum]).playerColor colorBlendFactor:1.0 duration:0.0]];
                        [players[ownerNum] addStructure:node];
                        [players[ownerNum]setMoneyPerTurn:[players[ownerNum]moneyPerTurn]+node.revenue];
                    }
                    [allStructuresOnMapArray addObject:node];
                    
                }
                
                if (numOfValues == 3) {
                    Unit *node = [Unit new];
                    //TODO popravi _unitsArray da lahko nrdis use enote (ne samo ground)
                    [node createUnitWithDictionary:_unitsArray[0][((NSNumber*)values[2][0]).intValue]];
                    [node setName:@"unit"];
                    [node setUserData:_unitsArray[0][((NSNumber*)values[2][0]).intValue]];
                    node.position = tile.position;
                    [world addChild:node];
                    [node setUnitPosition:CGPointMake(row, coll)];
                    [node setUnitPositionOnWorld:tile.position];
                    [node setScale:0.7];
                    int ownerNum = ((NSNumber*)values[2][1]).intValue;
                    [node runAction:[SKAction colorizeWithColor:((Player*)players[ownerNum]).playerColor colorBlendFactor:1.0 duration:0.0]];
                    [node setOwner:ownerNum];
                    [players[ownerNum] addUnit:node forPlayer:ownerNum];
                }

                NSMutableDictionary *tileDict = [NSMutableDictionary dictionaryWithObject:NSStringFromCGPoint(CGPointMake(coll, row)) forKey:@"tilePosition"];
                [tile setUserData:tileDict];
                //[tile setZPosition:levelData.count-1-row];
                [tile setZPosition:1.0];
                [world addChild:tile];
                [levelTileNodesRow addObject:tile];
            }
            else {
                tile = [SKNode node];
                [tile setName:@"tile"];
                double xPos = (sizeX * coll + row * sizeX/2) - (_maxWidthOfMap -1.2) * sizeX ;
                double yPos = sizeY * row - levelData.count * sizeY * 0.5 + sizeY * 0.5;
                //NSLog(@"%d %f",levelData.count, levelData.count * sizeY * 0.5);
                tile.position = CGPointMake(xPos, yPos);
                SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed:@"Fog"];
                [node.texture setUsesMipmaps:YES];
                node.hidden = YES;
                hasFog = NO;
                [node setName:@"terrain"];
                [node setUserData:(NSMutableDictionary*)terrainArray[10]];
                [tile addChild:node];
                NSMutableDictionary *tileDict = [NSMutableDictionary dictionaryWithObject:NSStringFromCGPoint(CGPointMake(coll, row)) forKey:@"tilePosition"];
                [tile setUserData:tileDict];
                //[tile setZPosition:levelData.count-1-row];
                [tile setZPosition:1.0];
                [world addChild:tile];
                [levelTileNodesRow addObject:tile];
            }
#pragma mark showGrid
            //NSLog(@"DRAW GRID");
            SKSpriteNode *tileBorder = [SKSpriteNode spriteNodeWithImageNamed:@"grid"];
            [tileBorder.texture setUsesMipmaps:YES];
            [tileBorder setColor:[UIColor blackColor]];
            [tileBorder setColorBlendFactor:1.0];
            tileBorder.position = tile.position;
            [gridOverlay addChild:tileBorder];
            //[tileBorder setAlpha:0.045];
            [tileBorder setScale:0.965];
            [tileBorder setZPosition:1.0];
            //[tileBorder setYScale:0.77];
            if (!hasFog) {
                tileBorder.hidden = YES;
            }
            
            coll++;
        }
        row++;
        coll = 0;
    }
    [Structure setAllStructuresOnMap:allStructuresOnMapArray];
    
    [gridOverlay setName:@"gridOverlay"];
    [gridOverlay setZPosition:90.0];
    [gridOverlay setAlpha:0.04];
    [world addChild:gridOverlay];
    if (!showGrid) {
        [gridOverlay setHidden:YES];
    }
    
    //SKTexture *gridTexture = [_view textureFromNode:gridOverlay];
    
    //SKEffectNode *effectGridOverlay = [SKEffectNode node];
    //[effectGridOverlay addChild:gridOverlay];
    //[effectGridOverlay setShouldEnableEffects:YES];
    //[effectGridOverlay setZPosition:999.0];
    //[effectGridOverlay setShouldRasterize:YES];
    /*
    CGFloat alpha = 0.15;
    CGFloat rgba[4] = {0.0, 0.0, 0.0, alpha};
    CIFilter *colorMatrix = [CIFilter filterWithName:@"CIColorMatrix"];
    [colorMatrix setDefaults];
    [colorMatrix setValue:[CIVector vectorWithValues:rgba count:4] forKey:@"inputAVector"];
    */
    //[effectGridOverlay setFilter:colorMatrix];
    
    //[world addChild:effectGridOverlay];
    //[world addChild:gridOverlay];
    
    /*
    SKSpriteNode *gridFromTexture = [SKSpriteNode spriteNodeWithTexture:gridTexture];
    [gridFromTexture setZPosition:999.0];
    [gridFromTexture setAnchorPoint:CGPointMake(0.0, 0.5)];
    double xPos = -(_maxWidthOfMap -1.2) * sizeX - 32.0*[Functions deviceFactor];
    [gridFromTexture setPosition:CGPointMake(xPos, 0.0)];
    [gridFromTexture setAlpha:0.15];
    [world addChild:gridFromTexture];
     */
    
    _incomeMultiplier = [[levelDict objectForKey:@"Income Multiplier"]intValue];
    map = world;
    return world;
}

-(SKNode*)getGridOverlay {
    return gridOverlay;
}

-(NSArray*)getTerrainDefence {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *tempDict in terrainArray) {
        [tempArray addObject:[tempDict objectForKey:@"ExtraDefence"]];
    }
    return tempArray;
}

-(NSArray*)getLevelTilesNodesArray {
    return levelTileNodes;
}
/*
-(void)updateGround:(SKSpriteNode*)node {
    //NSDictionary *levelDict = [gameDataDict objectForKey:@"Levels"][levelNumber];
    //NSArray *levelData = [levelDict objectForKey:@"LevelData"];
    CGPoint point = node.anchorPoint;
 
    int row = 0, counter = 0;
    NSArray *children = node.children;
    for (NSArray *levelDataRow in levelData) {
        for (int coll = 0; coll < levelDataRow.count; coll++) {
            ((SKSpriteNode*)children[counter]).position = CGPointMake(-64.0*[Functions deviceFactor]*(levelDataRow.count-1)*point.x+64.0*coll*[Functions deviceFactor], -64.0*[Functions deviceFactor]*(levelData.count-1)*point.y+64.0*row*[Functions deviceFactor]);
            counter++;
        }
        row++;
    }
}


-(SKTexture*)createGroundTexture {
    //NSDictionary *levelDict = [gameDataDict objectForKey:@"Levels"][levelNumber];
    //NSArray *levelData = [levelDict objectForKey:@"LevelData"];
    //NSArray *terrainArray = [gameDataDict objectForKey:@"Terrain"];
 
    SKNode *ground = [SKNode node];
 
    int coll = 0, row = 0;
    for (NSArray *levelDataRow in levelData) {
        for (NSNumber *value in levelDataRow) {
            SKSpriteNode *terrainNode = [SKSpriteNode spriteNodeWithImageNamed:[terrainArray[value.intValue] objectForKey:@"TerrainName"]];
            terrainNode.position = CGPointMake(64.0*coll*[Functions deviceFactor], 64.0*row*[Functions deviceFactor]);
            [ground addChild:terrainNode];
            coll++;
            //NSLog(@"row: %d | coll: %d",row,coll);
        }
        row++;
        coll = 0;
    }
 
    SKTexture *groundTexture = [[SKView new]textureFromNode:ground];
    //[groundTexture setUsesMipmaps:YES];
    [groundTexture setFilteringMode:SKTextureFilteringLinear];
    return groundTexture;
}
*/
-(NSArray*)getLevelTerrainMap {
    //spremenjeno zarad custom map
    return [NSArray arrayWithArray:levelData];
}

-(NSMutableArray*)getLevelCostMap {
    NSMutableArray *costMap = [NSMutableArray array];
    for (NSArray *row in levelData) {
        NSMutableArray *rowMap = [NSMutableArray array];
        for (NSArray *tile in row) {
            if (tile.count >= 2) {
                if (((NSArray*)tile[1]).count) {
                    [rowMap addObject:[NSMutableArray arrayWithArray:[[gameDataDict objectForKey:@"Structures"][((NSNumber*)tile[1][0]).intValue]objectForKey:@"MovementCost"]]];
                }
                else if (tile.count == 3) {
                    [rowMap addObject:[NSMutableArray arrayWithArray:[[gameDataDict objectForKey:@"Terrain"][((NSNumber*)tile[0][0]).intValue]objectForKey:@"MovementCost"]]];
                }
            }
            else if (tile.count == 1) {
                [rowMap addObject:[NSMutableArray arrayWithArray:[[gameDataDict objectForKey:@"Terrain"][((NSNumber*)tile[0][0]).intValue]objectForKey:@"MovementCost"]]];
            }
            else {
                [rowMap addObject:[NSMutableArray arrayWithArray:[[gameDataDict objectForKey:@"Terrain"][10]objectForKey:@"MovementCost"]]];
            }
        }
        [costMap addObject:rowMap];
    }
    return costMap;
}


-(void)saveAllLevelImages {
    NSLog(@"saving all level images..");
    for (int i = 0; i < _levelArray.count; i++) {
        [self saveLevelImage:i];
    }
    NSLog(@"images saved!");
}

-(void)saveLevelImage:(int)levelNum {
    SKSpriteNode *level = [self createLevelWithDict:_levelArray[levelNum] andSettings:nil forPlayers:nil];
    //NSLog(@"level size: %@",level);
    [level setScale:500.0/(level.size.width <= level.size.height ? level.size.height : level.size.width)];
    SKTexture *levelTexture = [_view textureFromNode:level];
    NSLog(@"TEXTURE: %@",levelTexture);
    [self saveTexture:levelTexture name:[levelDict objectForKey:@"LevelName"]];
}

-(void)saveTexture:(SKTexture*)texture name:(NSString*)levelName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    /*
     //DELETE ALL FILES IN DOCUMENTS DIRECTORY
     for (NSString *file in [fileManager contentsOfDirectoryAtPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] error:nil]) {
     [fileManager removeItemAtPath:file error:nil];
     }
    */
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"screenshots.plist"];
    NSMutableDictionary *screenshotsDict;
    NSLog(@"file path: %@",filePath);
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        screenshotsDict = [NSMutableDictionary dictionary];
        NSLog(@"new file");
    }
    else {
        screenshotsDict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    }
    //SKTexture *texture1 = [SKTexture textureWithImageNamed:@"Sea"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:texture];
    [screenshotsDict setObject:data forKey:levelName];
    BOOL success = [screenshotsDict writeToFile:filePath atomically:YES];
    NSLog(@"SUCCESS: %d",success);
}

-(BOOL)createUnit:(int)unitNum ofType:(int)unitType on:(SKNode*)tile forPlayer:(Player*)player {
    //Do nothing if the player lacks the funds to buy the unit
    if (player.money < ((NSNumber*)[((NSDictionary*)_unitsArray[unitType][unitNum]) objectForKey:@"Cost"]).intValue) {
        return NO;
    }
    
    //Create the unit
    Unit *node = [Unit new];
    [node createUnitWithDictionary:_unitsArray[unitType][unitNum]];
    node.owner = player.playerNumber;
    [node setName:@"unit"];
    [node setUserData:_unitsArray[unitType][unitNum]];
    
    //Reduce player money
    player.money -= ((NSNumber*)[_unitsArray[unitType][unitNum] objectForKey:@"Cost"]).intValue;
    
    //Place the unit on the map
    node.position = tile.parent.position;
    node.zPosition = 100.0;
    [map addChild:node];
    CGPoint tilePosition = CGPointFromString([tile.parent.userData objectForKey:@"tilePosition"]);
    [node setUnitPosition:CGPointMake(tilePosition.x, tilePosition.y)];
    [node setUnitPositionOnWorld:tile.parent.position];
    
    //Color the unit and give the player the unit
    [node setUnitColor:player.playerColor];
    [player addUnit:node forPlayer:player.playerNumber];
    
    //Unit is built
    return YES;
}

@end
