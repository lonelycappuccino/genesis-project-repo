//
//  SettingsMenu.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "SettingsMenu.h"
#import "Functions.h"

@implementation SettingsMenu {
    SKLabelNode *gridLabel, *fogLabel;
    NSMutableDictionary *gameSettingsDictionary;
    NSDictionary *settingsDict;
    SKSpriteNode *backButton;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [UIColor orangeColor];
        //LOAD SETTINGS FROM FILE
        settingsDict = [NSDictionary dictionaryWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"gameSettings.plist"]];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setFontColor:[UIColor darkGrayColor]];
        [titleLabel setFontSize:27.5*factor];
        [titleLabel setText:@"SETTINGS"];
        [titleLabel setPosition:CGPointMake(self.size.width/2, self.size.height-40.0*factor)];
        [self addChild:titleLabel];
        
        SKLabelNode *audioLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [audioLabel setFontColor:[UIColor darkGrayColor]];
        [audioLabel setFontSize:20.0*factor];
        [audioLabel setText:@"Audio"];
        [audioLabel setPosition:CGPointMake(self.size.width/4, self.size.height/4*3)];
        [self addChild:audioLabel];
        
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7.5*factor)];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        
        gridLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [gridLabel setFontColor:[UIColor darkGrayColor]];
        [gridLabel setFontSize:20.0*factor];
        if ([[settingsDict objectForKey:@"showGrid"]boolValue]) {
            [gridLabel setText:@"Show grid"];
        }
        else {
            [gridLabel setText:@"Don't show grid"];
        }
        [gridLabel setPosition:CGPointMake(self.size.width/4, self.size.height/3*2)];
        [self addChild:gridLabel];
        
        fogLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [fogLabel setFontColor:[UIColor darkGrayColor]];
        [fogLabel setFontSize:20.0*factor];
        if ([[settingsDict objectForKey:@"showFog"]boolValue]) {
            [fogLabel setText:@"Show fog"];
        }
        else {
            [fogLabel setText:@"Don't show fog"];
        }
        [fogLabel setPosition:CGPointMake(self.size.width/4, self.size.height/2)];
        [self addChild:fogLabel];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *gesture in view.gestureRecognizers) {
        [view removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([gridLabel containsPoint:location]) {
        if ([[settingsDict objectForKey:@"showGrid"]boolValue]) {
            [gridLabel setText:@"Don't show grid"];
        }
        else {
            [gridLabel setText:@"Show grid"];
        }
        [settingsDict setValue:[NSNumber numberWithBool:![[settingsDict objectForKey:@"showGrid"]boolValue]] forKey:@"showGrid"];
        [self saveSettings];
    }
    else if ([fogLabel containsPoint:location]) {
        if ([[settingsDict objectForKey:@"showFog"]boolValue]) {
            [fogLabel setText:@"Don't show fog"];
        }
        else {
            [fogLabel setText:@"Show fog"];
        }
        [settingsDict setValue:[NSNumber numberWithBool:![[settingsDict objectForKey:@"showFog"]boolValue]] forKey:@"showFog"];
        [self saveSettings];
    }
    else if ([backButton containsPoint:location]) {
        [self.view presentScene:_mainMenu];
    }
}

-(void)saveSettings {
    if([settingsDict writeToFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"gameSettings.plist"] atomically:YES]) {
        NSLog(@"settings saved!");
    }
    else {
        NSLog(@"error while saving");
    }
}

-(void)update:(CFTimeInterval)currentTime {
    
}

@end
