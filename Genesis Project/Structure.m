//
//  Structure.m
//  Genesis Project
//
//  Created by Klemen Košir on 17/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "Structure.h"
#import "Functions.h"
#import "Player.h"
#import "LevelLoader.h"
#import "GameScene.h"

@implementation Structure

static __strong NSMutableArray *players = nil;
static __strong NSMutableArray *allStructuresOnMap = nil;

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:players forKey:@"players"];
    [aCoder encodeObject:allStructuresOnMap forKey:@"allStructuresOnMap"];
    [aCoder encodeBool:_isUsable forKey:@"isUsable"];
    [aCoder encodeObject:_menu forKey:@"menu"];
    [aCoder encodeInt:_owner forKey:@"owner"];
    [aCoder encodeObject:_unitsArray forKey:@"unitsArray"];
    [aCoder encodeObject:_structureName forKey:@"structureName"];
    [aCoder encodeObject:_structureDictionary forKey:@"structureDictionary"];
    [aCoder encodeInt:_revenue forKey:@"revenue"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        players = [aDecoder decodeObjectForKey:@"players"];
        allStructuresOnMap = [aDecoder decodeObjectForKey:@"allStructuresOnMap"];
        _isUsable = [aDecoder decodeBoolForKey:@"isUsable"];
        _menu = [aDecoder decodeObjectForKey:@"menu"];
        _owner = [aDecoder decodeIntForKey:@"owner"];
        _unitsArray = [aDecoder decodeObjectForKey:@"unitsArray"];
        _structureName = [aDecoder decodeObjectForKey:@"structureName"];
        _structureDictionary = [aDecoder decodeObjectForKey:@"structureDictionary"];
        _revenue = [aDecoder decodeIntForKey:@"revenue"];
    }
    return self;
}

+(void)setPlayers:(NSMutableArray*)playersArray {
    players = playersArray;
}

+(void)setAllStructuresOnMap:(NSMutableArray*)array {
    allStructuresOnMap = array;
}

+(NSMutableArray*)getAllStructuresOnMap {
    return allStructuresOnMap;
}

+(Structure*)initWithDictionary:(NSDictionary*)structureDictionary andArray:(NSArray*)unitsArray {
    Structure *structure = [Structure new];
    [structure createWithDictionaries:structureDictionary and:unitsArray];
    return structure;
}

-(void)createWithDictionaries:(NSDictionary*)structureDict and:(NSArray*)units {
    [self setScale:1.0];
    _structureDictionary = structureDict;
    _structureName = [structureDict objectForKey:@"Name"];
    SKTexture *structureTexture = [SKTexture textureWithImageNamed:[_structureDictionary objectForKey:@"Texture"]];
    [structureTexture setUsesMipmaps:YES];
    [self setTexture:structureTexture];
    [self setSize:structureTexture.size];
    
    _isUsable = [[_structureDictionary objectForKey:@"Usable"]boolValue];
    if (_isUsable) {
        NSString *name = [_structureDictionary objectForKey:@"Name"];
        NSMutableArray *unitsMutArray;
        if ([name isEqualToString:@"Factory"]) {
            unitsMutArray = [NSMutableArray arrayWithArray:units[0]];
        }
        else if ([name isEqualToString:@"Airport"]) {
            unitsMutArray = [NSMutableArray arrayWithArray:units[1]];
        }
        else if ([name isEqualToString:@"Seaport"]) {
            unitsMutArray = [NSMutableArray arrayWithArray:units[2]];
        }
        _unitsArray = unitsMutArray;
    }
    [self setScale:0.6];
    [self setZPosition:2.0];
    [self setUserData:[_structureDictionary mutableCopy]];
    
    [self setRevenue:[[_structureDictionary objectForKey:@"GeneratesRevenue"]intValue]*[LevelLoader loader].incomeMultiplier];
}

-(SKNode*)getMenuForPlayer:(int)playerNum withPosition:(CGPoint)pos {
    if (!_unitsArray) {
        return nil;
    }
    
    CGFloat hue;
    [[players[playerNum]playerColor]getHue:&hue saturation:nil brightness:nil alpha:nil];
    
    SKNode *menu = [SKNode node];
    
    int counter = 1, counterAll = 0;
    int itemCounter = 0;
    CGFloat distance = 75.0*factor;
    CGFloat angle = 0.0;
    
    NSLog(@"pos: %f x %f",pos.x,pos.y);
    
    //GameScene *gameScene = [GameScene game];
    
    SKShapeNode *item = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(64.0*factor, 64.0*factor)];
    [item setStrokeColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.6]];
    [item setLineWidth:1.5*factor];
    //[item setFillColor:[UIColor colorWithWhite:0.98 alpha:0.4]];
    [item setName:@"factory"];
    [item setAntialiased:YES];
    [menu addChild:item];
    
    SKNode *items = [SKNode node];
    [items setName:@"items"];
    
    for (NSMutableDictionary *unitItem in _unitsArray) {
        SKShapeNode *item = [SKShapeNode shapeNodeWithEllipseOfSize:CGSizeMake(64.0*factor, 64.0*factor)];
        [item setStrokeColor:[UIColor whiteColor]];
        [item setLineWidth:0.75*factor];
        [item setFillColor:[UIColor colorWithWhite:0.98 alpha:0.4]];
        [item setName:[unitItem objectForKey:@"Name"]];
        NSMutableDictionary *unitDict = [NSMutableDictionary dictionaryWithDictionary:unitItem];
        [unitDict setObject:[NSNumber numberWithInt:itemCounter++] forKey:@"unitNumber"];
        [item setUserData:unitDict];
        
        UIImage *texture = [UIImage imageNamed:[unitItem objectForKey:@"Name"]];
        if (texture) {
            SKSpriteNode *itemUnit = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImage:texture]];
            [itemUnit setSize:CGSizeMake(64.0*factor, 64.0*factor)];
            [item addChild:itemUnit];
        }
        
        NSString *priceString = [[unitItem objectForKey:@"Cost"]stringValue];
        
        SKSpriteNode *priceHolder = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.0 alpha:0.4] size:CGSizeMake(9.0*priceString.length, 16.0)];
        
        SKLabelNode *priceLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        [priceLabel setText:priceString];
        [priceLabel setFontColor:[UIColor whiteColor]];
        [priceLabel setFontSize:15.0];
        [priceLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [priceHolder addChild:priceLabel];
        [priceHolder setPosition:CGPointMake(0.0, -27*factor)];
        [priceHolder setZPosition:10.0];
        [item addChild:priceHolder];
        
        
        if (counterAll/6.0 == counter) {
            distance += 67.5*factor-(67.5*factor*0.04*(counter-1));
            counterAll = 0;
            counter++;
            angle = 0.0;
        }
        
        CGFloat scale = ((67.5*factor-(67.5*factor*0.075*(counter-1)))/(67.5*factor));
        
        CGFloat x;
        CGFloat y;
        
        while (1) {
            x = cosf(angle)*distance;
            y = sinf(angle)*distance;
            
            if (x+pos.x > 32.0*factor && y+pos.y > 32.0*factor && x+pos.x < viewSize.width-32.0*factor && y+pos.y < viewSize.height-32.0*factor) {
                break;
            }
            
            /*
            if (x+pos.x > 0.0 && y+pos.y > 0.0 &&
                x+pos.x < viewSize.width && y+pos.y < viewSize.height) {
                break;
            }
            */
            angle -= (2*M_PI)/(6.0*counter);
            counterAll++;
            
            if (counterAll/6.0 == counter) {
                NSLog(@"distance");
                distance += 67.5*factor-(67.5*factor*0.04*(counter-1));
                counterAll = 0;
                counter++;
                angle = 0.0;
            }
            
            NSLog(@"LOOP");
        }
        NSLog(@"x: %f y: %f",x,y);
        
        item.position = CGPointMake(x, y);
        [item setScale:0.0];
        
        [items addChild:item];
        [item runAction:[SKAction scaleTo:scale duration:0.18]];
        
        angle -= (2*M_PI)/(6.0*counter);
        
        
        counterAll++;
    }
    [menu addChild:items];
    
    [menu setUserData:[NSMutableDictionary dictionaryWithObject:self forKey:@"owner"]];
    [menu setZPosition:9999999.0];
    [menu setName:@"structureMenu"];
    NSLog(@"show menu");
    return menu;
}

/*
-(SKNode*)getMenuForPlayer:(int)playerNum {
    if (!_unitsArray) {
        return nil;
    }
    CGFloat hue;
    [[players[playerNum]playerColor]getHue:&hue saturation:nil brightness:nil alpha:nil];
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:[LevelLoader loader].gameScene.size];
    [bg setColorBlendFactor:1.0];
    [bg setColor:[UIColor colorWithHue:hue saturation:0.1 brightness:0.7 alpha:1.0]];
    
    SKCropNode *menu = [SKCropNode node];
    [menu setMaskNode:bg.copy];
    [menu addChild:bg];
    [menu setName:@"structureMenu"];
    
    SKLabelNode *menuLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [menuLabel setText:[_structureDictionary objectForKey:@"Name"]];
    [menuLabel setFontColor:[UIColor darkGrayColor]];
    [menuLabel setFontSize:30.0*factor];
    [menuLabel setPosition:CGPointMake(0.0, bg.size.height/2-30.0*factor)];
    [menu addChild:menuLabel];
    
    SKNode *items = [SKNode node];
    NSMutableArray *itemsArray = [NSMutableArray array];
    int counter = 1;
    CGFloat xPos = -bg.size.width/2+75.0*factor, yPos = 80.0*factor;
    for (NSMutableDictionary *unitItem in _unitsArray) {
        SKSpriteNode *item = [SKSpriteNode spriteNodeWithImageNamed:[unitItem objectForKey:@"Name"]];
        [item setName:[unitItem objectForKey:@"Name"]];
        NSMutableDictionary *unitDict = [NSMutableDictionary dictionaryWithDictionary:unitItem];
        [unitDict setObject:[NSNumber numberWithInt:counter-1] forKey:@"unitNumber"];
        [item setUserData:unitDict];
        [item setScale:1.0];
        if (counter % 2 == 0) {
            [item setPosition:CGPointMake(xPos, yPos-140.0*factor)];
            xPos += 115.0*factor;
        }
        else {
            [item setPosition:CGPointMake(xPos, yPos)];
        }
        if (((NSNumber*)[unitItem objectForKey:@"Cost"]).intValue > ((Player*)players[playerNum]).money) {
            [item setAlpha:0.3];
            //NSLog(@"ITEM COST: %d",((NSNumber*)[_unitsArray[counter]objectForKey:@"Cost"]).intValue);
        }
        SKLabelNode *price = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [price setFontColor:[UIColor whiteColor]];
        [price setFontSize:15.0*factor];
        [price setText:[[unitItem objectForKey:@"Cost"]stringValue]];
        [price setPosition:CGPointMake(0.0, -item.size.height/2-20.0*factor)];
        [price setZPosition:1.0];
        [item addChild:price];
        
        SKSpriteNode *infoButton = [SKSpriteNode spriteNodeWithImageNamed:@"playersMenu_circle"];
        [infoButton setScale:0.75];
        [infoButton setColor:[UIColor blueColor]];
        [infoButton setColorBlendFactor:0.8];
        [infoButton setPosition:CGPointMake(item.size.width/2, item.size.height/2)];
        [infoButton setZPosition:3.0];
        [infoButton setName:@"info"];
        [item addChild:infoButton];
        
        [item setZPosition:1.0];
        [items addChild:item];
        [itemsArray addObject:item];
        counter++;
    }
    
    items.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(100.0, 100.0)];
    items.physicsBody.affectedByGravity = NO;
    items.physicsBody.allowsRotation = NO;
    items.physicsBody.collisionBitMask = 0;
    items.physicsBody.mass = 1.0;
    items.physicsBody.linearDamping = 7.5;
    
    [menu addChild:items];
    [items setName:@"items"];
    //[items setUserData:[NSMutableDictionary dictionaryWithObject:itemsArray forKey:@"itemsArray"]];
    
    SKSpriteNode *exitButton = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
    [exitButton setPosition:CGPointMake(bg.size.width/2-25.0*factor, bg.size.height/2-25.0*factor)];
    [exitButton setName:@"exit"];
    [exitButton setZPosition:1.0];
    [menu addChild:exitButton];
    
    SKLabelNode *playerMoney = [SKLabelNode labelNodeWithText:[NSString stringWithFormat:@"Money: %d",[players[playerNum]money]]];
    [playerMoney setPosition:CGPointMake(0.0, -bg.size.height/2+20.0*factor)];
    [playerMoney setZPosition:1.0];
    [menu addChild:playerMoney];
    
    [menu setUserData:[NSMutableDictionary dictionaryWithObject:self forKey:@"owner"]];
    [menu setZPosition:9999999.0];
    
    
    
    return menu;
}
 */
/*
-(void)StructureDestroyed {
    
}
*/
-(void)repairStructure:(int)playerNum {
    [self createWithDictionaries:((NSDictionary*)[LevelLoader loader].structuresArray[[[_structureDictionary objectForKey:@"NodeAfterChange"]intValue]]) and:[LevelLoader loader].unitsArray];
    NSLog(@"Structure repaired");
    if (![_structureName  isEqual: @"Bridge"]) {
        _owner = playerNum;
        [((Player*)players[playerNum]).structures addObject:self];
        [self runAction:[SKAction colorizeWithColor:((Player*)players[playerNum]).playerColor colorBlendFactor:1.0 duration:0.2]];
    }
}

-(void)capturing:(int)playerNum {
    if ([_structureName isEqual:@"HQ"]) {
        SKLabelNode *winnerTitle = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [winnerTitle setFontColor:[UIColor greenColor]];
        [winnerTitle setFontSize:70.0];
        [winnerTitle setText:@"YOU WIN!"];
        [winnerTitle setZPosition:99999999.0];
        SKScene *gameScene = [LevelLoader loader].gameScene;
        [winnerTitle setPosition:CGPointMake(gameScene.size.width/2, gameScene.size.height/2)];
        [gameScene addChild:winnerTitle];
    }
    if (_owner != -1) {
        [((Player*)players[_owner]) removeStructure:self];
    }
    _owner = ((Player*)players[playerNum]).playerNumber;
    [((Player*)players[playerNum]) addStructure:self];
    [self runAction:[SKAction colorizeWithColor:((Player*)players[playerNum]).playerColor colorBlendFactor:1.0 duration:0.2]];
    NSLog(@"BUILDING CAPTURED");
}
/*
-(void)selected {
    NSLog(@"SELECTED STRUCTURE");
    NSLog(@"scene size:%f x %f",self.scene.size.width,self.scene.size.height);
}
*/
@end
