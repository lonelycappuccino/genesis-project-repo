//
//  GameScene.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "GameScene.h"
#import "LevelLoader.h"
#import "Player.h"
#import "Functions.h"
#import "Structure.h"
#import "MainMenu.h"
#import "SettingsMenu.h"
#import "GZIP.h"

@interface SKNode (MyAdditions) <NSCoding>

-(BOOL)containsPointInCircle:(CGPoint)point;

@end

@implementation SKNode (MyAdditions)
/*
-(void)encodeWithCoder:(NSCoder *)aCoder {
    if ([self isKindOfClass:[SKSpriteNode class]]) {
        [(SKSpriteNode*)self setTexture:nil];
    }
    [aCoder encodeObject:self];
}
/*
-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [self init]) {
        
    }
    return self;
}
*/
-(BOOL)containsPointInCircle:(CGPoint)point{
    CGFloat d = sqrtf(powf(point.x-self.position.x, 2)+powf(point.y-self.position.y, 2));
    if (self.frame.size.width/2 >= d) {
        return YES;
    }
    return NO;
}

@end

@implementation GameScene {
    /*
    int numberOfFingers;
    CGFloat prevTouchesDistance;
    NSMutableArray *touchesArray, ;
    BOOL isZoomed, rightMenuVisible,playerInfoCardExpanded,unitIsAttacking;
    int returnedValue;
    SKNode *quickInfoNode;
    NSMutableArray *quickInfoNodes;*/
    
    NSMutableArray *structureToChange;
    
    BOOL waitingForNextPlayer;
    BOOL structureMenuVisible;
    //BOOL hasLoadedUnit;
    //BOOL unloadingUnit;
    BOOL unitInfoVisible;
    BOOL didLoadNextPlayer;
    
    CGFloat levelWidth, levelHeight;
    CGFloat startScale;
    CGFloat structureMenuStartX;
    CGPoint gesturePanStart;
    CGPoint prevTouchLocation,startPosition,gestureTargetPosition;
    
    int levelNumber;
    int playerOnTurn;
    int turn;
    
    NSArray *players;
    NSArray *levelMap;
    NSArray *levelCostMap;
    NSDictionary *levelDictionary;
    NSMutableArray *loadableUnits;
    NSMutableArray *attackableUnits;
    NSMutableArray *highlightArea;
    NSMutableArray *fogTiles;
    
    SKAction *pinchAction;
    SKLabelNode *turnLabel;
    SKNode *playerInfoCard;
    SKSpriteNode *saveIcon, *settingsIcon, *exitIcon;
    SKSpriteNode *world, *map;//, *rightMenuUI;
    SKShapeNode *nextTurnButton;
    
    __strong Unit *selectedUnit;
    
    NSDictionary *gameSettingsDict;
    
    SKNode *gridOverlay, *sideMenu;
    SKEffectNode *gridOverlayNode;
    BOOL sideMenuVisible;
    
    NSTimeInterval playTime, playStartTime;
}

static __strong GameScene *sharedInstance = nil;

+(GameScene*)game {
    return sharedInstance;
}
+(GameScene*)gameSceneWithSize:(CGSize)size {
    sharedInstance = [GameScene sceneWithSize:size];
    return sharedInstance;
}

+(void)reset {
    sharedInstance = nil;
}

+(NSArray*)getLevelTiles {
    return [[LevelLoader loader] getLevelTilesNodesArray];
}

+(void)loadGame:(GameScene*)loadedGame {
    sharedInstance = loadedGame;
}
#pragma mark TO-DO_SAVE/LOAD
-(void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeBool:waitingForNextPlayer forKey:@"waitingForNextPlayer"];
    [aCoder encodeBool:structureMenuVisible forKey:@"structureMenuVisible"];
    
    [aCoder encodeBool:unitInfoVisible forKey:@"unitInfoVisible"];
    [aCoder encodeBool:didLoadNextPlayer forKey:@"didLoadNextPlayer"];
    
    [aCoder encodeFloat:levelWidth forKey:@"levelWidth"];
    [aCoder encodeFloat:levelHeight forKey:@"levelHeight"];
    
    [aCoder encodeFloat:startScale forKey:@"startScale"];
    [aCoder encodeFloat:structureMenuStartX forKey:@"structureMenuStartX"];
    [aCoder encodeCGPoint:gesturePanStart forKey:@"gesturePanStart"];
    [aCoder encodeCGPoint:prevTouchLocation forKey:@"prevTouchLocation"];
    [aCoder encodeCGPoint:startPosition forKey:@"startPosition"];
    [aCoder encodeCGPoint:gestureTargetPosition forKey:@"gestureTargetPosition"];
    
    [aCoder encodeInt:levelNumber forKey:@"levelNumber"];
    [aCoder encodeInt:playerOnTurn forKey:@"playerOnTurn"];
    [aCoder encodeInt:turn forKey:@"turn"];
    
    [aCoder encodeObject:players forKey:@"players"];
    [aCoder encodeObject:levelMap forKey:@"levelMap"];
    [aCoder encodeObject:levelCostMap forKey:@"levelCostMap"];
    [aCoder encodeObject:levelDictionary forKey:@"levelDictionary"];
    [aCoder encodeObject:loadableUnits forKey:@"loadableUnits"];
    [aCoder encodeObject:structureToChange forKey:@"structureToChange"];
    [aCoder encodeObject:attackableUnits forKey:@"attackableUnits"];
    [aCoder encodeObject:highlightArea forKey:@"highlightArea"];
    [aCoder encodeObject:fogTiles forKey:@"fogTiles"];
    
    [aCoder encodeObject:pinchAction forKey:@"pinchAction"];
    [aCoder encodeObject:turnLabel forKey:@"turnLabel"];
    [aCoder encodeObject:playerInfoCard forKey:@"playerInfoCard"];
    [aCoder encodeObject:nextTurnButton forKey:@"nextTurnButton"];
    [aCoder encodeObject:saveIcon forKey:@"saveIcon"];
    [aCoder encodeObject:settingsIcon forKey:@"settingsIcon"];
    [aCoder encodeObject:exitIcon forKey:@"exitIcon"];
    [aCoder encodeObject:world forKey:@"world"];
    [aCoder encodeObject:map forKey:@"map"];
    
    [aCoder encodeObject:selectedUnit forKey:@"selectedUnit"];
    
    [aCoder encodeObject:gameSettingsDict forKey:@"gameSettingsDict"];
    [aCoder encodeObject:gridOverlay forKey:@"gridOverlay"];
    [aCoder encodeObject:gridOverlayNode forKey:@"gridOverlayNode"];
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        waitingForNextPlayer = [aDecoder decodeBoolForKey:@"waitingForNextPlayer"];
        structureMenuVisible = [aDecoder decodeBoolForKey:@"structureMenuVisible"];

        unitInfoVisible = [aDecoder decodeBoolForKey:@"unitInfoVisible"];
        didLoadNextPlayer = [aDecoder decodeBoolForKey:@"didLoadNextPlayer"];
        
        levelWidth = [aDecoder decodeFloatForKey:@"levelWidth"];
        levelHeight = [aDecoder decodeFloatForKey:@"levelHeight"];
        startScale = [aDecoder decodeFloatForKey:@"startScale"];
        structureMenuStartX = [aDecoder decodeFloatForKey:@"structureMenuStartX"];
        gesturePanStart = [aDecoder decodeCGPointForKey:@"gesturePanStart"];
        prevTouchLocation = [aDecoder decodeCGPointForKey:@"prevTouchLocation"];
        startPosition = [aDecoder decodeCGPointForKey:@"startPosition"];
        gestureTargetPosition = [aDecoder decodeCGPointForKey:@"gestureTargetPosition"];
        
        levelNumber = [aDecoder decodeIntForKey:@"levelNumber"];
        playerOnTurn = [aDecoder decodeIntForKey:@"playerOnTurn"];
        turn = [aDecoder decodeIntForKey:@"turn"];
        
        players = [aDecoder decodeObjectForKey:@"players"];
        levelMap = [aDecoder decodeObjectForKey:@"levelMap"];
        levelCostMap = [aDecoder decodeObjectForKey:@"levelCostMap"];
        levelDictionary = [aDecoder decodeObjectForKey:@"levelDictionary"];
        loadableUnits = [aDecoder decodeObjectForKey:@"loadableUnits"];
        structureToChange = [aDecoder decodeObjectForKey:@"structureToChange"];
        attackableUnits = [aDecoder decodeObjectForKey:@"attackableUnits"];
        highlightArea = [aDecoder decodeObjectForKey:@"highlightArea"];
        fogTiles = [aDecoder decodeObjectForKey:@"fogTiles"];
        
        pinchAction = [aDecoder decodeObjectForKey:@"pinchAction"];
        turnLabel = [aDecoder decodeObjectForKey:@"turnLabel"];
        playerInfoCard = [aDecoder decodeObjectForKey:@"playerInfoCard"];
        nextTurnButton = [aDecoder decodeObjectForKey:@"nextTurnButton"];
        saveIcon = [aDecoder decodeObjectForKey:@"saveIcon"];
        settingsIcon = [aDecoder decodeObjectForKey:@"settingsIcon"];
        exitIcon = [aDecoder decodeObjectForKey:@"exitIcon"];
        world = [aDecoder decodeObjectForKey:@"world"];
        map = [aDecoder decodeObjectForKey:@"map"];
        
        selectedUnit = [aDecoder decodeObjectForKey:@"selectedUnit"];
        gameSettingsDict = [aDecoder decodeObjectForKey:@"gameSettingsDict"];
        
        gridOverlay = [aDecoder decodeObjectForKey:@"gridOverlay"];
        gridOverlayNode = [aDecoder decodeObjectForKey:@"gridOverlayNode"];
    }
    return self;
}

-(void)initGameWithDict:(NSDictionary*)levelDict andSettings:(NSDictionary*)gameSettings forPlayers:(NSArray*)playersArray {
    gameSettingsDict = [NSDictionary dictionaryWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"gameSettings.plist"]];
    
    fogTiles = [NSMutableArray array];
    
    levelDictionary = levelDict;
    levelNumber = 5;
    turn = 1;
    waitingForNextPlayer = NO;
    structureMenuVisible = NO;
    
    //numberOfFingers = 0;
    //touchesArray = [NSMutableArray array];
    //isZoomed = NO;
    //unitIsAttacking = NO;
    //unloadingUnit = NO;
    
    players = [self createPlayersFromArray:playersArray];
    playerOnTurn = 0;
    [self deselectUnit];
    
    self.backgroundColor = [UIColor grayColor];
    [[LevelLoader loader]setGameScene:self];
    
    map = [[LevelLoader loader] createLevelWithDict:levelDict andSettings:gameSettingsDict forPlayers: players];
    
    levelMap = [[LevelLoader loader] getLevelTerrainMap];
    levelCostMap = [[LevelLoader loader] getLevelCostMap];
    _terrainDefence = [[LevelLoader loader] getTerrainDefence];
    
    [self updatePlayersAfterStart];
    
    levelWidth = map.size.width;
    levelHeight = map.size.height;
    world = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:map.size];
    [world addChild:map];
    world.position = CGPointMake(self.size.width/2, self.size.height/2);
    [world setScale:0.75];
    [self addChild:world];
    
    self.physicsWorld.gravity = CGVectorMake(0.0, 0.0);
    world.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(100.0, 100.0)];
    world.physicsBody.affectedByGravity = NO;
    [world.physicsBody setLinearDamping:8.0];
    [world.physicsBody setMass:1.0];
    [world.physicsBody setFriction:1.0];
    [world.physicsBody setAllowsRotation:NO];
    
    [self setPlayerPosition];
    
    //[self createRightMenu];
    
    //returnedValue = -1;
    
    [self createUI];
    //[self createQuickInfoNode];
    //quickInfoNodes = [NSMutableArray array];
    //playerInfoCardExpanded = NO;
    
    for (Player *player in players) {
        [player createLevelArrayWithLevelMap:[NSMutableArray arrayWithArray:levelMap]];
    }
    [self updateMapForPlayer:players[playerOnTurn]];
    
}

-(void)setPlayerPosition {
    for (Player *player in players) {
        for (Structure *structure in player.structures) {
            if ([structure.structureName isEqualToString:@"HQ"]) {
                CGPoint tmpPos = [structure convertPoint:structure.position toNode:world];
                player.cameraPosition = CGPointMake(self.size.width/2-tmpPos.x*world.xScale, self.size.height/2-tmpPos.y*world.yScale);
                if (player.playerNumber == 0) {
                    world.position = player.cameraPosition;
                }
                //NSLog(@"camera position: %f x %f", player.cameraPosition.x,player.cameraPosition.y);
                //NSLog(@"structure owner: %d (%d)", structure.owner,player.playerNumber);
                break;
            }
        }
    }
}

-(void)createUI {
    playerInfoCard = [SKNode node];
    
    SKSpriteNode *infoNode = [SKSpriteNode spriteNodeWithImageNamed:@"playerHUD_bg_1"];
    [infoNode setXScale:0.75];
    
    SKSpriteNode *playerColor = [SKSpriteNode spriteNodeWithImageNamed:@"playerHUD_color"];
    [playerColor setAnchorPoint:CGPointMake(1.0, 0.5)];
    [playerColor setName:@"playerColor"];
    [playerColor setColorBlendFactor:1.0];
    [playerColor setColor:[players[playerOnTurn]playerColor]];
    [playerColor setPosition:CGPointMake(-infoNode.size.width/2+1.0, 0.0)];
    
    [playerInfoCard addChild:playerColor];
    [playerInfoCard addChild:infoNode];
    [playerInfoCard setPosition:CGPointMake(62.0*[Functions deviceFactor], self.size.height-19.0*[Functions deviceFactor])];
    //[playerInfoCard setPosition:CGPointMake(62.0*[Functions deviceFactor]+160.0, self.size.height-19.0*[Functions deviceFactor])];
    [playerInfoCard setZPosition:999999.0];
    [self addChild:playerInfoCard];
    
    SKLabelNode *playerMoneyLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [playerMoneyLabel setText:[NSString stringWithFormat:@"%d",((Player*)players[playerOnTurn]).money]];
    [playerMoneyLabel setName:@"money"];
    [playerMoneyLabel setFontColor:[UIColor whiteColor]];
    [playerMoneyLabel setFontSize:20.0*[Functions deviceFactor]],
    [playerMoneyLabel setFontName:@"Arial"];
    [playerMoneyLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [playerMoneyLabel setPosition:CGPointMake(-35.0*[Functions deviceFactor], -5.0*[Functions deviceFactor])];
    [playerMoneyLabel setZPosition:10.0];
    [playerInfoCard addChild:playerMoneyLabel];
    
    SKLabelNode *perTurnMoneyLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [perTurnMoneyLabel setText:[NSString stringWithFormat:@"per turn %d",[players[playerOnTurn]moneyPerTurn]]];
    [perTurnMoneyLabel setName:@"perTurnMoney"];
    [perTurnMoneyLabel setFontColor:[UIColor whiteColor]];
    [perTurnMoneyLabel setFontSize:7.0*[Functions deviceFactor]],
    [perTurnMoneyLabel setFontName:@"Arial"];
    [perTurnMoneyLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [perTurnMoneyLabel setPosition:CGPointMake(-30.0*[Functions deviceFactor], -13.0*[Functions deviceFactor])];
    [perTurnMoneyLabel setZPosition:10.0];
    [playerInfoCard addChild:perTurnMoneyLabel];
    
    for (Player *player in players) {
        [player setMoneyLabel:((SKLabelNode*)[playerInfoCard childNodeWithName:@"money"])];
    }
    
    nextTurnButton = [SKShapeNode shapeNodeWithCircleOfRadius:20.0*factor];
    [nextTurnButton setFillColor:[UIColor colorWithRed:0.0 green:150.0/255.0 blue:145.0/255.0 alpha:0.9]];
    [nextTurnButton setStrokeColor:[UIColor colorWithRed:0.0 green:100.0/255.0 blue:95.0/255.0 alpha:0.98]];
    [nextTurnButton setLineWidth:0.5*factor];
    [nextTurnButton setPosition:CGPointMake(self.size.width-nextTurnButton.frame.size.width/2-5.0*factor, self.size.height-nextTurnButton.frame.size.height/2-5.0*factor)];
    [nextTurnButton setZPosition:99999.0];
    [self addChild:nextTurnButton];
    
    SKSpriteNode *nextTurnButtonIcon = [SKSpriteNode spriteNodeWithImageNamed:@"nextTurnIcon"];
    //[nextTurnButtonIcon setPosition:CGPointMake(self.size.width-nextTurnButtonIcon.size.width/2-15.0, self.size.height-nextTurnButtonIcon.size.height/2-15.0)];
    //[nextTurnButtonIcon setScale:1.2];
    [nextTurnButtonIcon setPosition:CGPointMake(1.0*factor, 0.0)];
    [nextTurnButtonIcon setAlpha:0.3];
    [nextTurnButtonIcon runAction:[SKAction colorizeWithColor:[UIColor blackColor] colorBlendFactor:1.0 duration:0.0]];
    [nextTurnButtonIcon setZPosition:99999.1];
    [nextTurnButton addChild:nextTurnButtonIcon];
    
    turnLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [turnLabel setFontColor:[UIColor colorWithWhite:1.0 alpha:1.0]];
    [turnLabel setFontSize:16.0*[Functions deviceFactor]];
    [turnLabel setZPosition:99999.1];
    [turnLabel setText:@(turn).stringValue];
    [turnLabel setPosition:CGPointMake(-2.0*factor, 0.0)];
    [turnLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [turnLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [nextTurnButton addChild:turnLabel];
    /*
    SKSpriteNode *circle1 = [SKSpriteNode spriteNodeWithImageNamed:@"pauseCircle_small"];
    //[circle1 setPosition:CGPointMake(self.size.width-circle1.size.width/2-10.0, self.size.height-circle1.size.height/2-10.0)];
    [circle1 setPosition:CGPointMake(self.size.width-circle1.size.width/2-10.0, circle1.size.height/2+10.0)];
    [circle1 setName:@"circle1"];
    [circle1 setZPosition:99999.0];
    [self addChild:circle1];
    
    SKSpriteNode *circle2 = [SKSpriteNode spriteNodeWithImageNamed:@"pauseCircle_large"];
    [circle2 setPosition:circle1.position];
    [circle2 setName:@"circle2"];
    [circle2 setScale:circle1.size.width/circle2.size.width];
    [circle2 setAlpha:0.0];
    [circle2 setZPosition:99999.0];
    [self addChild:circle2];
    
    SKSpriteNode *pauseIcon = [SKSpriteNode spriteNodeWithImageNamed:@"pauseIcon"];
    [pauseIcon setPosition:circle1.position];
    [pauseIcon setScale:0.9];
    [pauseIcon setZPosition:99999.1];
    [self addChild:pauseIcon];
    
    saveIcon = [SKSpriteNode spriteNodeWithImageNamed:@"saveIcon"];
    [saveIcon setPosition:CGPointMake(-5.0*factor, 80.0*factor)];
    [circle2 addChild:saveIcon];
    
    settingsIcon = [SKSpriteNode spriteNodeWithImageNamed:@"settingsIcon"];
    [settingsIcon setPosition:CGPointMake(-55.0*factor, 55.0*factor)];
    [circle2 addChild:settingsIcon];
    
    exitIcon = [SKSpriteNode spriteNodeWithImageNamed:@"exitIcon"];
    [exitIcon setPosition:CGPointMake(-80.0*factor, 5.0*factor)];
    [circle2 addChild:exitIcon];
    */
    [self createSideMenu];
}

-(void)createSideMenu {
    sideMenu = [SKNode node];
    
    SKShapeNode *sideMenuBG = [SKShapeNode shapeNodeWithRect:CGRectMake(-1.0, -1.0, 160.0*factor, self.size.height+2.0)];
    [sideMenuBG setFillColor:[UIColor colorWithWhite:0.10 alpha:0.97]];
    [sideMenuBG setStrokeColor:[UIColor whiteColor]];
    [sideMenuBG setLineWidth:0.5];
    [sideMenuBG setAntialiased:NO];
    [sideMenu addChild:sideMenuBG];
    
    SKLabelNode *sideMenuGuildName = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [sideMenuGuildName setFontSize:17.0*factor];
    [sideMenuGuildName setFontColor:[UIColor whiteColor]];
    [sideMenuGuildName setPosition:CGPointMake(10.0*factor, self.size.height - 10.0*factor)];
    [sideMenuGuildName setVerticalAlignmentMode:SKLabelVerticalAlignmentModeTop];
    [sideMenuGuildName setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [sideMenuGuildName setText:[players[playerOnTurn] playerName]];
    [sideMenuGuildName setName:@"GuildName"];
    [sideMenu addChild:sideMenuGuildName];
    
    SKLabelNode *sideMenuStatLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [sideMenuStatLabel setFontSize:13.0*factor];
    [sideMenuStatLabel setFontColor:[UIColor whiteColor]];
    [sideMenuStatLabel setPosition:CGPointMake(10.0*factor, self.size.height - 50.0*factor)];
    [sideMenuStatLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeTop];
    [sideMenuStatLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [sideMenuStatLabel setText:@"Stats"];
    [sideMenu addChild:sideMenuStatLabel];
    
    SKLabelNode *playTimeLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [playTimeLabel setFontSize:13.0*factor];
    [playTimeLabel setFontColor:[UIColor whiteColor]];
    [playTimeLabel setPosition:CGPointMake(10.0*factor, self.size.height - 65.0*factor)];
    [playTimeLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeTop];
    [playTimeLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [playTimeLabel setText:@"Play Time"];
    [playTimeLabel setName:@"playTimeLabel"];
    [sideMenu addChild:playTimeLabel];
    
    saveIcon = [SKSpriteNode spriteNodeWithImageNamed:@"saveIcon"];
    [saveIcon setPosition:CGPointMake(saveIcon.size.width/2 + 20.0*factor, 105.0*factor)];
    [sideMenu addChild:saveIcon];
    
    settingsIcon = [SKSpriteNode spriteNodeWithImageNamed:@"settingsIcon"];
    [settingsIcon setPosition:CGPointMake(-settingsIcon.size.width/2+140.0*factor, 105.0*factor)];
    [sideMenu addChild:settingsIcon];
    
    exitIcon = [SKSpriteNode spriteNodeWithImageNamed:@"exitIcon"];
    [exitIcon setPosition:CGPointMake(exitIcon.size.width/2+20.0*factor, exitIcon.size.height/2+15.0*factor)];
    [sideMenu addChild:exitIcon];
    
    [sideMenu setPosition:CGPointMake(-170.0*factor, 0.0)];
    [sideMenu setHidden:YES];
    [sideMenu setZPosition:999999.0];
    [self addChild:sideMenu];
    
    sideMenuVisible = NO;
}

-(void)updatePlayersAfterStart {
    int mapWidth = 0;
    for (NSArray *row in levelMap) {
        if(row.count > mapWidth){
            mapWidth = (int)row.count;
        }
    }
    for (Player *player in players) {
        [player addUnitArray];
        player.levelMap = levelMap;
        NSMutableArray* trueDeepCopyArray = [NSKeyedUnarchiver unarchiveObjectWithData:
                                      [NSKeyedArchiver archivedDataWithRootObject:levelCostMap]];
        player.levelCostMap = trueDeepCopyArray;
    }
}

-(NSArray*)createPlayersFromArray:(NSArray*)pArray {
    //int numberOfPlayers = [[levelDictionary objectForKey:@"Players"]intValue];
    NSLog(@"Players: %@",pArray);
    NSMutableArray *playersArray = [NSMutableArray array];
    //NSArray *colors = @[[UIColor redColor],[UIColor blueColor],[UIColor greenColor],[UIColor yellowColor]];
    //NSArray *names = @[@"FutureCorp", @"LameCorp",@"OtherCorp",@"UnknownCorp"];
    int counter = 0;
    for (NSDictionary *playerDict in pArray) {
        Player *player = [Player new];
        [player setPlayerNumber:counter];
        [player setPlayerColor:[playerDict objectForKey:@"Color"]];
        [player setPlayerName:[playerDict objectForKey:@"Name"]];
        [playersArray addObject:player];
        player.computer = ![[playerDict objectForKey:@"playerTypeIsHuman"]boolValue];
        /*if (i == 1) {
            //YES = AI player, NO = human player
            player.computer = YES;
            //the higher the number the better the AI plays: optimal values 0-100 (may be broken for the rest)
            [player chooseAIPersonalityForDifficulty:100];
        }*/
        counter++;
    }
    [Player setPlayers:playersArray];
    [Structure setPlayers:playersArray];
    return playersArray;
}

-(void)refreshGameScene {
    gameSettingsDict = [NSDictionary dictionaryWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"gameSettings.plist"]];
    SKNode *overlay = [map childNodeWithName:@"gridOverlay"];
    BOOL gridVisible = [[gameSettingsDict objectForKey:@"showGrid"]boolValue];
    if (gridVisible) {
        [overlay setHidden:NO];
        /*
        for (SKSpriteNode *tileBorder in overlay.children) {
            [tileBorder setColor:[UIColor blackColor]];
            [tileBorder setColorBlendFactor:1.0];
        }
         */
    }
    else {
        [overlay setHidden:YES];
    }
}

-(void)didMoveToView:(SKView *)view {
    NSLog(@"DID MOVE TO GAME SCENE %@",map);
    [self refreshGameScene];
    
    if (playStartTime == 0) {
        playStartTime = [NSDate timeIntervalSinceReferenceDate];
    }
    /*
    __block CGFloat startTime = 0.0;
    [self runAction:[SKAction customActionWithDuration:0.7 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        if (startTime == 0) {
            startTime = elapsedTime;
        }
        CGFloat timeDiff = elapsedTime-startTime;
        NSLog(@"timeDiff: %f",timeDiff);
        if (timeDiff >= 0.005) {
            [self setAnchorPoint:CGPointMake(self.anchorPoint.x+timeDiff/5.0, 0.0)];
            startTime = elapsedTime;
        }
    }]];
    */
    for (UIGestureRecognizer *gestureRecognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:gestureRecognizer];
    }
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchHandler:)];
    pinch.delegate = self;
    [view addGestureRecognizer:pinch];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapHandler:)];
    singleTap.delegate = self;
    [singleTap setNumberOfTapsRequired:1];
    [view addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapHandler:)];
    doubleTap.delegate = self;
    [doubleTap setNumberOfTapsRequired:2];
    [view addGestureRecognizer:doubleTap];
    /*
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftSwipeHandler:)];
    [leftSwipe setDelegate:self];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [leftSwipe setNumberOfTouchesRequired:1];
    [view addGestureRecognizer:leftSwipe];
    */
    UIScreenEdgePanGestureRecognizer *edgePan = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(swipeFromLeftEdge:)];
    [edgePan setDelegate:self];
    [edgePan setEdges:UIRectEdgeLeft];
    [view addGestureRecognizer:edgePan];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panHandler:)];
    pan.delegate = self;
    [pan setMaximumNumberOfTouches:1];
    [pan setMinimumNumberOfTouches:1];
    //POSSIBLE FIX FOR TOUCH
    [pan requireGestureRecognizerToFail:singleTap];
    
    [view addGestureRecognizer:pan];
    /*
    UILongPressGestureRecognizer *longPress =[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressHandler:)];
    [longPress setDelegate:self];
    [longPress setMinimumPressDuration:0.3];
    [longPress setAllowableMovement:5.0];
    [view addGestureRecognizer:longPress];*/
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) ||
        ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])) {
        return YES;
    }
    return NO;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}
//Long press handler
/*
-(void)longPressHandler:(UILongPressGestureRecognizer*)gesture {
    NSLog(@"TEST 1");
    if (gesture.state == UIGestureRecognizerStateBegan) {
        CGPoint gestureLocation = [map convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
        for (Unit *unit in (((Player*)players[playerOnTurn]).units[playerOnTurn])) {
            if (CGRectContainsPoint(unit.frame, [unit.parent convertPoint:gestureLocation fromNode:map])) {
                [unit selected];
            }
        }
        for (Structure *structure in [players[playerOnTurn] structures]) {
            if (CGRectContainsPoint(structure.frame, [structure.parent convertPoint:gestureLocation fromNode:map])) {
                [structure selected];
                
                SKNode *structureMenu = [structure getMenuForPlayer:playerOnTurn];
                [self addChild:structureMenu];
                [structureMenu runAction:[SKAction moveToY:0.0 duration:0.25]];
            }
        }
    }
}*/

-(void)pinchHandler:(UIPinchGestureRecognizer*)gesture {
    CGPoint gestureLocation =  [world convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    //gestureLocation = CGPointMake(gestureLocation.x+levelWidth*world.anchorPoint.x, gestureLocation.y+levelHeight*world.anchorPoint.y);
    //NSLog(@"gesture world location: %f x %f",gestureLocation.x,gestureLocation.y);
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [gesture setScale:world.xScale];
        startScale = world.xScale;
        startPosition = world.position;
        gestureTargetPosition = location;
        //[world setAnchorPoint:CGPointMake(1.0, 1.0)];
        //[LevelLoader updateGround:world];
        CGPoint anchor = CGPointMake((gestureLocation.x/levelWidth+1.0)/2.0, (gestureLocation.y/levelHeight+1.0)/2.0);
        [world setAnchorPoint:anchor];
    }
    if (gesture.scale > 1.0 || gesture.scale < 0.5 || gesture.numberOfTouches < 2) {
        return;
    }
    
    //NSLog(@"location: %f x %f",anchor.x, anchor.y);
    
    world.position = CGPointMake(location.x, location.y);
    [world setScale:gesture.scale];
    map.position = CGPointMake(levelWidth/2.0*(1.0-world.anchorPoint.x*2.0), levelHeight/2.0*(1.0-world.anchorPoint.y*2.0));
    //[world setPosition:CGPointMake(gestureTargetPosition.x-(gestureTargetPosition.x-location.x)/(gesture.scale), gestureTargetPosition.y-(gestureTargetPosition.y-location.y)/(gesture.scale))];
    
}

-(void)singleTapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"start of singletaphandler");
    //check if any part of the HUD was pressed
    CGPoint gestureLocation = [self convertPointFromView:[gesture locationInView:self.view]];
    if (waitingForNextPlayer && didLoadNextPlayer) {
        waitingForNextPlayer = NO;
        [[self childNodeWithName:@"dim"]removeFromParent];
        didLoadNextPlayer = NO;
        return;
    }
    else if (waitingForNextPlayer && !didLoadNextPlayer) {
        return;
    }
    
    if (unitInfoVisible) {
        [[self childNodeWithName:@"unitInfo"]runAction:[SKAction fadeOutWithDuration:0.15]completion:^{
            [[self childNodeWithName:@"unitInfo"]removeFromParent];
            unitInfoVisible = NO;
        }];
        return;
    }
    
    if ([nextTurnButton containsPoint:[nextTurnButton.parent convertPoint:gestureLocation fromNode:self]]) {
        //POSSIBLE FIX FOR END TURN
        NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(endTurn) object:nil];
        [thread start];
        //[self endTurn];
        return;
    }
    
    //or if the structure menu cover everything
#pragma mark structureMenu1
    if (structureMenuVisible) {
        /*
        [[[[self childNodeWithName:@"structureMenu"].userData objectForKey:@"owner"]parent] setZPosition:5.0];
        
        [[self childNodeWithName:@"structureMenu"]runAction:[SKAction group:@[[SKAction scaleTo:0.0 duration:0.12],[SKAction fadeOutWithDuration:0.11]]] completion:^{
            [[self childNodeWithName:@"structureMenu"]removeFromParent];
        }];
        
        [[self childNodeWithName:@"structureMenuBG"]runAction:[SKAction fadeOutWithDuration:0.12] completion:^{
            [[self childNodeWithName:@"structureMenuBG"]removeFromParent];
        }];
        
        structureMenuVisible = NO;
        return;
        */
        NSLog(@"structure menu");
        SKNode *structureMenu = [self childNodeWithName:@"structureMenu"];
        //NSLog(@"%@",structureMenu);
        CGPoint locationInMenu = [structureMenu convertPoint:gestureLocation fromNode:self];
        /*
        if ([[structureMenu childNodeWithName:@"exit"] containsPoint:locationInMenu]) {
            NSLog(@"EXIT");
            [[self childNodeWithName:@"structureMenu"]runAction:[SKAction moveToX:-self.size.width/2 duration:0.15] completion:^{
                [[self childNodeWithName:@"structureMenu"]removeFromParent];
            }];
            structureMenuVisible = NO;
            return;
        }
         */
        if ([[structureMenu childNodeWithName:@"items"] containsPoint:locationInMenu]) {
            //NSLog(@"STRUCTURE MENU");
            SKNode *itemSelectMenu = [structureMenu childNodeWithName:@"items"];
            gestureLocation = [itemSelectMenu convertPoint:gestureLocation fromNode:self];
            for (SKSpriteNode *item in itemSelectMenu.children) {
                if ([item containsPoint:gestureLocation]) {
                    /*
                    if ([[item childNodeWithName:@"info"]containsPointInCircle:[item convertPoint:gestureLocation fromNode:itemSelectMenu]]) {
                        NSLog(@"INFO");
#pragma mark showUnitInfo
                        // -- za nardit -- //
                        [self showUnitInfoForItem:item];
                        break;
                    }
                     */
                    NSString *name = ((Structure*)[structureMenu.userData objectForKey:@"owner"]).structureName;
                    int type;
                    //NSLog(@"item: %@",item.userData);
                    if ([name isEqualToString:@"Factory"]) {
                        type = 0;
                    }
                    else if ([name isEqualToString:@"Airport"]) {
                        type = 1;
                    }
                    else if ([name isEqualToString:@"Seaport"]) {
                        type = 2;
                    }

                    [self createUnit:((NSNumber*)[item.userData objectForKey:@"unitNumber"]).intValue ofType:type on:[structureMenu.userData objectForKey:@"owner"]];
                    /*
                    [[[[self childNodeWithName:@"structureMenu"].userData objectForKey:@"owner"]parent] setZPosition:5.0];
                    
                    [[self childNodeWithName:@"structureMenu"]runAction:[SKAction group:@[[SKAction scaleTo:0.0 duration:0.12],[SKAction fadeOutWithDuration:0.11]]] completion:^{
                        [[self childNodeWithName:@"structureMenu"]removeFromParent];
                    }];
                    
                    [[self childNodeWithName:@"structureMenuBG"]runAction:[SKAction fadeOutWithDuration:0.12] completion:^{
                        [[self childNodeWithName:@"structureMenuBG"]removeFromParent];
                    }];
                    structureMenuVisible = NO;
                     */
                    break;
                }
            }
        }
        
        [[[self childNodeWithName:@"structureMenu"].userData objectForKey:@"owner"] setZPosition:2.0];
        
        [[self childNodeWithName:@"structureMenu"]runAction:[SKAction group:@[[SKAction scaleTo:0.0 duration:0.12],[SKAction fadeOutWithDuration:0.11]]] completion:^{
            [[self childNodeWithName:@"structureMenu"]removeFromParent];
        }];
        
        [[self childNodeWithName:@"structureMenuBG"]runAction:[SKAction fadeOutWithDuration:0.12] completion:^{
            [[self childNodeWithName:@"structureMenuBG"]removeFromParent];
        }];
        structureMenuVisible = NO;
        
        return;
    }

    //sideMenu visible
    if (sideMenuVisible) {
        if ([sideMenu containsPoint:gestureLocation]) {
            if ([saveIcon containsPoint:[saveIcon.parent convertPoint:gestureLocation fromNode:self]]) {
                //NSLog(@"SAVE ICON");
                [self saveGame:@"test 1"];
            }
            else if ([settingsIcon containsPoint:[settingsIcon.parent convertPoint:gestureLocation fromNode:self]]) {
                //NSLog(@"SETTINGS ICON");
                SettingsMenu *settingsMenu = [SettingsMenu sceneWithSize:self.size];
                [settingsMenu setMainMenu:self];
                [self.view presentScene:settingsMenu];
            }
            else if ([exitIcon containsPoint:[exitIcon.parent convertPoint:gestureLocation fromNode:self]]) {
                //NSLog(@"EXIT ICON");
                MainMenu *mainMenu = [MainMenu sceneWithSize:self.size];
                [LevelLoader resetLevelLoader];
                [GameScene reset];
                [self.view presentScene:mainMenu];
            }
            
        }
        else {
            [sideMenu runAction:[SKAction moveToX:-170.0*factor duration:0.07] completion:^{
                [sideMenu setHidden:YES];
                sideMenuVisible = NO;
            }];
            [playerInfoCard runAction:[SKAction moveByX:-159.0*factor y:0.0 duration:0.12]];
        }
        return;
    }
    
    if ([playerInfoCard containsPoint:gestureLocation]) {
        if (!sideMenuVisible) {
            [sideMenu setHidden:NO];
            [sideMenu runAction:[SKAction moveToX:-1.0 duration:0.07]];
            sideMenuVisible = YES;
            [playerInfoCard runAction:[SKAction moveByX:159.0*factor y:0.0 duration:0.075]];
        }
        else {
            [sideMenu runAction:[SKAction moveToX:-170.0*factor duration:0.07] completion:^{
                [sideMenu setHidden:YES];
                sideMenuVisible = NO;
            }];
            [playerInfoCard runAction:[SKAction moveByX:-159.0*factor y:0.0 duration:0.12]];
        }
        return;
    }
    
    //else check unit actions
    CGPoint location = [map convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    
    //Repair or capture
    if (structureToChange.count) {
        for (Structure *building in structureToChange) {
            if ([[building childNodeWithName:@"capture"] containsPoint:[building convertPoint:location fromNode:map]]) {
                NSLog(@"capture structure");
                [building capturing:playerOnTurn];
            }
            else if ([[building childNodeWithName:@"repair"] containsPoint:[building convertPoint:location fromNode:map]]) {
                NSLog(@"repair structure");
                [building repairStructure:playerOnTurn];
            }
            [[building childNodeWithName:@"repair"]runAction:[SKAction scaleTo:0.0 duration:0.1] completion:^{
                [[building childNodeWithName:@"repair"]removeFromParent];
            }];
            [[building childNodeWithName:@"capture"]runAction:[SKAction scaleTo:0.0 duration:0.1] completion:^{
                [[building childNodeWithName:@"capture"]removeFromParent];
            }];
            [selectedUnit setCanMove:NO];
        }
    }
    structureToChange = nil;
    
    //Check if unit is deselecting itself
    if (selectedUnit) {
        if (CGRectContainsPoint(selectedUnit.frame, [selectedUnit.parent convertPoint:location fromNode:map])) {
            [self deselectUnit];
            return;
        }
    }
    //Loading and related stuff
    /*
    //Loading
    if (loadableUnits) {
        NSLog(@"LOADABLE UNITS");
        for (Unit *unit in loadableUnits) {
            if ([[unit childNodeWithName:@"loadUnit"] containsPoint:[unit convertPoint:location fromNode:map]]) {
                NSLog(@"LOAD UNIT: %@",selectedUnit);
                [unit loadUnit:selectedUnit];
                NSLog(@"UNIT: %@ loaded onto: %@",selectedUnit,unit);
                SKSpriteNode *loadedUnit = (SKSpriteNode*)[unit childNodeWithName:@"loadUnit"];
                [loadedUnit setPosition:CGPointMake(unit.size.width/2-30.0, unit.size.height/2-30.0)];
                [loadedUnit setScale:0.5];
            }
            else {
                [[unit childNodeWithName:@"loadUnit"]removeFromParent];
            }
        }
    }
    loadableUnits = nil;
    
    if ([[selectedUnit childNodeWithName:@"loadUnit"]containsPoint:[selectedUnit convertPoint:location fromNode:map]]) {
        NSLog(@"UNLOAD");
        
        [self highlightUnitExtraction:selectedUnit.getLoadedUnit];
        unloadingUnit = YES;
        return;
    }
     if (selectedUnit.hasLoadedUnit) {
     [[selectedUnit childNodeWithName:@"loadUnit"]setScale:0.5];
    }
    else {
        [[selectedUnit childNodeWithName:@"loadUnit"]removeFromParent];
    }
    
    //Unloading
    if (unloadingUnit) {
        NSLog(@"unloading unit (selected unit: %@)",selectedUnit);
        for (SKNode *tile in highlightArea) {
            if ([tile containsPoint:[tile.parent convertPoint:location fromNode:map]]) {
                Unit *unloadedUnit = [selectedUnit unloadUnit];
                NSLog(@"unloaded unit: %@",unloadedUnit);
                [unloadedUnit setPosition:tile.parent.position];
                [unloadedUnit setUnitPosition:CGPointFromString([tile.parent.userData objectForKey:@"tilePosition"])];
                [map addChild:unloadedUnit];
            }
            [tile removeFromParent];
        }
        [[selectedUnit childNodeWithName:@"loadUnit"]removeFromParent];
        unloadingUnit = NO;
        [self deselectUnit];
        return;
    }*/
    
    if (attackableUnits) {
        CGPoint tmpLocation = [self convertPointFromView:[gesture locationInView:self.view]];
        if ([selectedUnit containsPoint:[selectedUnit.parent convertPoint:tmpLocation fromNode:self]]) {
            [self deselectUnit];
            return;
        }
        for (Unit *unit in attackableUnits) {
            if ([unit containsPoint:[unit.parent convertPoint:tmpLocation fromNode:self]]) {
                //TODO animate the attacking
                [Unit unit:selectedUnit attacksUnit:unit];
                [self deselectUnit];
                return;
            }
        }
        for (Unit *unitTmp in attackableUnits) {
            [[unitTmp childNodeWithName:@"crosshair"]removeFromParent];
        }
        if (!highlightArea) {
            [self deselectUnit];
            return;
        }
        
    }
    
    if (highlightArea) {
        BOOL highlightedAreaSelected = NO;
        CGPoint pos;
        for (SKNode *highlightedTiles in highlightArea) {
            //TODO if hex contains touch location, not highlightedTiles.frame
            if (CGRectContainsPoint(highlightedTiles.frame, [map convertPoint:location toNode:highlightedTiles])) {
                highlightedAreaSelected = YES;
                pos = CGPointFromString([[highlightedTiles.parent userData] objectForKey:@"tilePosition"]);
                break;
            }
        }
        if (highlightedAreaSelected) {
            [self moveUnit:selectedUnit ToX:pos.x Y:pos.y];
        }
        for (SKNode *tile in highlightArea) {
            [tile removeFromParent];
        }
        highlightArea = nil;
        NSLog(@"highlightedArea");
        return;
    }
    //TODO optimize (check _level in player what is in this tile (for buildings and units)
    if (!selectedUnit) {
        
        for (Unit *unit in [players[playerOnTurn] friendlyUnits]) {
            //NSLog(@"check for unit");
            if (CGRectContainsPoint(unit.frame, [unit.parent convertPoint:location fromNode:map])) {
                if (unit.canAttack) {
                    [self unitCanAttack:unit];
                }
                
                if (!unit.canMove) {
                    return;
                }
                //NSLog(@"contains");
                selectedUnit = unit;
                //NSLog(@"UNIT HEALTH: %d",unit.health);
                
                if (selectedUnit.hasLoadedUnit) {
                    [[selectedUnit childNodeWithName:@"loadUnit"] setScale:1.4];
                }
                else {
                    
                    //[self unitCanLoad:unit];
                }
                [self highlightUnitMovementTilesForUnit:unit];
                [self checkStructuresForAction];
                //[self unitAttack];
                NSLog(@"sth unit selected");
                return;
            }
        }
        
        for (Structure *structure in [players[playerOnTurn] structures]) {
            if (CGRectContainsPoint(structure.frame, [structure.parent convertPoint:location fromNode:map])) {
                //[structure selected];
#pragma mark structureMenu2
                CGPoint structurePos = [self convertPoint:structure.parent.position fromNode:map];
                SKNode *structureMenu = [structure getMenuForPlayer:playerOnTurn withPosition:structurePos];
                if (structureMenu) {
                    [structureMenu setScale:0.0];
                    [structureMenu setPosition:structurePos];
                    //[structureMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
                    //[structureMenu runAction:[SKAction moveTo:CGPointMake(self.size.width/2-32.0*factor, self.size.height/2) duration:0.2]];
                    [structureMenu runAction:[SKAction scaleTo:1.0 duration:0.2]];
                    
                    
                    SKSpriteNode *bgMenu = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithHue:0.0 saturation:0.0 brightness:0.0 alpha:0.45] size:self.size];
                    [bgMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
                    [bgMenu setAlpha:0.0];
                    [bgMenu runAction:[SKAction fadeInWithDuration:0.17]];
                    
                    /*
                    SKSpriteNode *circle = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(128.0, 128.0)];
                    [circle setPosition:structurePos];
                    [circle setZPosition:structureMenu.zPosition-0.05];
                    [self addChild:circle];
                    */
                    [bgMenu setZPosition:structureMenu.zPosition-0.1];
                    [bgMenu setName:@"structureMenuBG"];
                    [self addChild:bgMenu];

                    
                    [self addChild:structureMenu];
                    NSLog(@"strucutre z: %f",structure.parent.zPosition);
                    [structure setZPosition:structureMenu.zPosition-0.01];
                    
                    //[structureMenu setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
                    structureMenuVisible = YES;
                }
                NSLog(@"sth building selected");
                return;
            }
        }
    }
    [self deselectUnit];
}

-(void)showUnitInfoForItem:(SKSpriteNode*)item {
    SKSpriteNode *infoBG = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(300.0*factor, 275.0*factor)];
    [infoBG setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [infoBG setZPosition:99999999.0];
    [infoBG setName:@"unitInfo"];
    [infoBG setAlpha:0.0];
    [infoBG runAction:[SKAction fadeInWithDuration:0.2]];
    [self addChild:infoBG];
    
    SKLabelNode *itemName = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [itemName setFontColor:[UIColor lightGrayColor]];
    [itemName setFontSize:20.0*factor];
    [itemName setText:item.name];
    [itemName setPosition:CGPointMake(0.0, infoBG.size.height/2-25.0*factor)];
    [infoBG addChild:itemName];
    
    NSString *description = [item.userData objectForKey:@"Description"];
    NSArray *lines = [description componentsSeparatedByString:@"|n"];
    NSLog(@"lines: %@",lines);
    for (int i = 0; ; i++) {
        SKLabelNode *itemDescription = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [itemDescription setFontColor:[UIColor whiteColor]];
        [itemDescription setFontSize:15.0*factor];
        [itemDescription setPosition:CGPointMake(10.0*factor-infoBG.size.width/2, 50.0*factor-25.0*factor*i)];
        [itemDescription setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [itemDescription setText:lines[i]];
        [infoBG addChild:itemDescription];
        if (i+1 >= lines.count) {
            break;
        }
    }
    unitInfoVisible = YES;
}

-(void)highlightUnitMovementTilesForUnit: (Unit*)unitToCheck {
    highlightArea = [NSMutableArray array];
    SKSpriteNode *highlightNode = [SKSpriteNode spriteNodeWithImageNamed:@"highlight_hex"];
    [highlightNode setZPosition:100.0];
    [highlightNode setAlpha:0.0];
    [highlightNode runAction:[SKAction fadeAlphaTo:0.4 duration:0.1]];
    [highlightNode runAction:[SKAction colorizeWithColor:[UIColor greenColor] colorBlendFactor:1.0 duration:0.0]];
    
    [self updateMapForPlayer:players[playerOnTurn]];
    //get array of tiles in range of the current unit
    NSArray *posibleMoves = [players[playerOnTurn] findNodesInMovementRangeForUnit: unitToCheck];
    
    //highlight the possible locations
    for (SKNode* tile in posibleMoves) {
        SKSpriteNode *highlightNodeTmp = highlightNode.copy;
        [highlightNodeTmp setAlpha:0.2];
        [highlightNodeTmp setName:@"highlight"];
        [tile addChild:highlightNodeTmp];
        [highlightArea addObject:highlightNodeTmp];
    }
}

   
// -- NOVA BOLJ UNIVERZALNA METODA ZA HIGHLIGH -- //
/*
-(void)highlightTilesWithRange:(int)range andColor:(UIColor*)color forUnit:(Unit*)unit {
    returnedValue = 0;
    NSArray *levelArray = [[LevelLoader loader] getLevelTilesNodesArray];
    int tmp = 0;
    highlightArea = [NSMutableArray array];
    SKSpriteNode *highlightNode = [SKSpriteNode spriteNodeWithImageNamed:@"tileHighlight"];
    [highlightNode setZPosition:100.0];
    [highlightNode setAlpha:0.0];
    [highlightNode runAction:[SKAction fadeInWithDuration:0.1]];
    
    int levelArrayCount = (int)levelArray.count;
    int levelArrayFirstObjectCount = (int)((NSArray*)[levelArray firstObject]).count;
    //NSArray *units = ((Player*)players[playerOnTurn]).units;
    for (int i = 0; i <= range*2; i++) {
        for (int j = 0; j < 1+2*tmp; j++) {
            int x = (int)unit.unitPosition.y-range+i;
            int y = (int)unit.unitPosition.x-tmp+j;
            if (x < 0 || x >= levelArrayCount ||
                y < 0 || y >= levelArrayFirstObjectCount) {
                continue;
            }
            SKNode *tile = levelArray[x][y];
            if ([tile containsPoint:unit.unitPositionOnWorld]) {
                continue;
            }
            if (![unit checkPath:x :y with:levelCostMap]) {
                continue;
            }
            
            SKSpriteNode *highlightNodeTmp = highlightNode.copy;
            [highlightNodeTmp runAction:[SKAction colorizeWithColor:color colorBlendFactor:1.0 duration:0.0]];
            BOOL hasUnit = NO;
            for (int i = 0; i < players.count; i++) {
                //NSLog(@"%d - %d",i,playerOnTurn);
                *//*
                 if (i == playerOnTurn) {
                 continue;
                 }
                 *//*
                //NSLog(@"2");
                for (SKNode *node in ((Player*)players[i]).units[i]) {
                    //NSLog(@"UNIT: %fx%f | TILE: %fx%f",node.position.x,node.position.y,tile.position.x,tile.position.y);
                    if ([node containsPoint:tile.position]) {
                        //[highlightNodeTmp runAction:[SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:1.0 duration:0.0]];
                        //NSLog(@"UNIT LBLALBALAL");
                        hasUnit = YES;
                        break;
                    }
                }
                if (hasUnit) {
                    break;
                }
            }
            if (hasUnit) {
                continue;
            }
            
            [highlightNodeTmp setAlpha:0.2];
            [tile addChild:highlightNodeTmp];
            [highlightArea addObject:highlightNodeTmp];
        }
        if (i >= range) {
            tmp--;
        }
        else {
            tmp++;
        }
    }
}
*/
//HighlightUnitAction
/*
-(void)highlightUnitAction {
    int movement = [selectedUnit getMovement];
    int range = [selectedUnit getWeaponRange];
    returnedValue = 0;
    NSArray *levelArray = [[LevelLoader loader] getLevelTilesNodesArray];
    int tmp = 0;
    highlightArea = [NSMutableArray array];
    SKSpriteNode *highlightNode = [SKSpriteNode spriteNodeWithImageNamed:@"tileHighlight"];
    [highlightNode setZPosition:100.0];
    [highlightNode setAlpha:0.0];
    [highlightNode runAction:[SKAction fadeInWithDuration:0.1]];
    
    int levelArrayCount = (int)levelArray.count;
    int levelArrayFirstObjectCount = (int)((NSArray*)[levelArray firstObject]).count;
    //NSArray *units = ((Player*)players[playerOnTurn]).units;
    for (int i = 0; i <= movement*2; i++) {
        for (int j = 0; j < 1+2*tmp; j++) {
            int x = (int)selectedUnit.unitPosition.y-movement+i;
            int y = (int)selectedUnit.unitPosition.x-tmp+j;
            if (x < 0 || x >= levelArrayCount ||
                y < 0 || y >= levelArrayFirstObjectCount) {
                continue;
            }
            SKNode *tile = levelArray[x][y];
            if ([tile containsPoint:selectedUnit.unitPositionOnWorld]) {
                continue;
            }
            if (![selectedUnit checkPath:x :y with:levelCostMap]) {
                continue;
            }
            
            SKSpriteNode *highlightNodeTmp = highlightNode.copy;
            [highlightNodeTmp runAction:[SKAction colorizeWithColor:[UIColor blueColor] colorBlendFactor:1.0 duration:0.0]];
            
             for (int i = 0; i < players.count; i++) {
                 NSLog(@"%d - %d",i,playerOnTurn);
                 if (i == playerOnTurn) {
                     continue;
                 }
                 NSLog(@"2");
                 for (SKNode *node in ((Player*)players[i]).units[i]) {
                     NSLog(@"UNIT: %fx%f | TILE: %fx%f",node.position.x,node.position.y,tile.position.x,tile.position.y);
                     if ([node containsPoint:tile.position]) {
                         [highlightNodeTmp runAction:[SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:1.0 duration:0.0]];
                         NSLog(@"UNIT LBLALBALAL");
                         break;
                     }
                 }
                 
             }
            for (SKNode *node in [Structure getAllStructuresOnMap]) {
                NSLog(@"cough cough");
                if ([tile containsPoint:node.parent.position]) {
                    NSLog(@"STRUCTURE");
                    [highlightNodeTmp runAction:[SKAction colorizeWithColor:[UIColor yellowColor] colorBlendFactor:1.0 duration:0.0]];
                    break;
                }
            }
            
            [highlightNodeTmp setAlpha:0.2];
            [tile addChild:highlightNodeTmp];
            [highlightArea addObject:highlightNodeTmp];
        }
        if (i >= movement) {
            tmp--;
        }
        else {
            tmp++;
        }
    }
}
*/

-(void)doubleTapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"DOUBLE TAP");
    //CGPoint gestureLocation = [self convertPointFromView:[gesture locationInView:self.view]];
    if (waitingForNextPlayer) {
        waitingForNextPlayer = NO;
        [[self childNodeWithName:@"dim"]removeFromParent];
        return;
    }
}

-(BOOL)createUnit:(int)unitNum ofType:(int)unitType on:(SKNode*)tile{
    return [[LevelLoader loader] createUnit:unitNum ofType:unitType on:tile forPlayer:players[playerOnTurn]];
}

-(void)moveUnit:(Unit*)unitToMove ToX:(int)endX Y:(int)endY {
    //then check if he can reach the desired location unambushed
    NSArray* path = [unitToMove isAmbushedOnWayToX:endX Y:endY with:((Player*)players[playerOnTurn]).levelCostMap];
    //if he can leave the spot (should always be able to move but just in case
    if (path) {
        [unitToMove moveUnitFromStart:path];
        [self updateMapForPlayer:players[playerOnTurn]];
    }
    
    if (unitToMove.canAttack) {
        if([self unitCanAttack:unitToMove]){
            //enable the desired action buttons on map or something
        }
    }
}

-(BOOL)tileHasUnits:(CGPoint)tilePos {
    for(int i = 0; i < players.count; i++){
        if (i != playerOnTurn) {
            for (Unit *unit in ((Player*)players[i]).friendlyUnits) {
                //TODO replace unit.alpha with unitCanAmbushPlayer when implementing teams (friendly units cannot ambush, only hostile units can)
                if (CGPointEqualToPoint(unit.unitPosition, tilePos) && unit.alpha != 1.0) {
                    return YES;
                }
            }
        }
    }
    return NO;
}
/*
-(BOOL)unitCanLoad: (Unit*)unitToCheck {
    
    loadableUnits = [NSMutableArray array];
    CGPoint unitPosition = unitToCheck.unitPosition;
    //check neighbors if the unitToCheck can load into any other units
    NSArray *neighbors = [self getNeighborsForTile:unitPosition];
    for (int i = 0; i < neighbors.count; i++) {
        for (Unit *unit in ((Player*)players[playerOnTurn]).friendlyUnits) {
            if (![unit isEqual:selectedUnit] && ([unit.unitName isEqualToString:@"APC"] ||
                                                 [unit.unitName isEqualToString:@"Transporter"])) {
                CGPoint pos = [((NSValue*)neighbors[i]) CGPointValue];
                if (unit.unitPosition.x == pos.x && unit.unitPosition.y == pos.y) {
                    SKSpriteNode *loadIcon = [SKSpriteNode spriteNodeWithImageNamed:@"unitLoad"];
                    [loadIcon setZPosition:10.0];
                    [loadIcon setScale:1.4];
                    [loadIcon setName:@"loadUnit"];
                    [unit addChild:loadIcon];
                    [loadableUnits addObject:unit];
                    goto outer;
                }
            }
        }
    outer:;
    }
    return YES;
}*/

-(BOOL)unitCanAttack: (Unit*)unitToCheck {
    
    attackableUnits = [NSMutableArray array];
    CGPoint unitPosition = unitToCheck.unitPosition;
    
    for(NSArray *units in ((Player*)players[playerOnTurn]).enemyUnits){
        for (Unit *unit in units) {
            //TODO increase the range for attacking to unit.weaponRange (requires checking in a spiral circle around unitPosition)
            NSArray *neighbors = [self getNeighborsForTile:unitPosition];
            for (int i = 0; i < neighbors.count; i++) {
                CGPoint pos = [((NSValue*)neighbors[i]) CGPointValue];
                if (pos.x == unit.unitPosition.x && pos.y == unit.unitPosition.y) {
                    [attackableUnits addObject:unit];
                    SKSpriteNode *crosshair = [SKSpriteNode spriteNodeWithImageNamed:@"unitAttack_1"];
                    [crosshair setPosition:CGPointMake(0.0, 55.0)];
                    [crosshair setZPosition:1000.0];
                    [crosshair setName:@"crosshair"];
                    [unit addChild:crosshair];
                    continue;
                }
            }
        }
    }
    NSLog(@"ATTACKABLE UNITS: %d", (int)attackableUnits.count);
    if (attackableUnits.count == 0) {
        attackableUnits = nil;
        return NO;
    }
    return YES;
}

-(void)unitDiedInCombat: (Unit*)unit {
    //remove the unit that died from players array;
    for (Player *player in players) {
        [player removeUnit:unit];
    }
    [self updateMapForPlayer:players[playerOnTurn]];
}

-(void)deselectUnit {
    for (Unit *unitTmp in attackableUnits) {
        [[unitTmp childNodeWithName:@"crosshair"] removeFromParent];
        [[unitTmp childNodeWithName:@"attackHighlight"]removeFromParent];
    }
    for (SKNode *tile in highlightArea) {
        [tile removeFromParent];
    }
    //loadableUnits = nil;
    highlightArea = nil;
    attackableUnits = nil;
    selectedUnit = nil;
}

-(BOOL)tileIsInVisibleMap:(CGPoint)tilePosition {
    //TODO optimize
    NSArray *terrainMap = levelMap;
    if (tilePosition.y < terrainMap.count && tilePosition.y >= 0 && tilePosition.x < ((NSArray*)terrainMap[(int)tilePosition.y]).count && tilePosition.x >= 0) {
        //NSLog(@"%@",terrainMap[(int)tilePosition.x][(int)tilePosition.y]);
        //NSLog(@"%@",terrainMap[(int)tilePosition.y][(int)tilePosition.x]);
        if (terrainMap[(int)tilePosition.y][(int)tilePosition.x] != [NSNull null]) {
            return YES;
        }
    }
    
    return NO;
}

-(NSArray*)getNeighborsForTile:(CGPoint)tilePosition {
    NSMutableArray *neighbors = [NSMutableArray array];
    
    //check if neighbors are on the map and valid places
    //TODO optimize this if needed
    if ([self tileIsInVisibleMap:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(1 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, -1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 0 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(-1 + tilePosition.x, 1 + tilePosition.y)]];
    }
    
    if ([self tileIsInVisibleMap:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]) {
        [neighbors addObject: [NSValue valueWithCGPoint:CGPointMake(0 + tilePosition.x, 1 + tilePosition.y)]];
    }
    return neighbors;
}

-(void)panHandler:(UIPanGestureRecognizer*)gesture {
    NSLog(@"PAN");
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [world.physicsBody setVelocity:CGVectorMake(0.0, 0.0)];
    }
    CGPoint gestureLocation = [self convertPointFromView:[gesture locationInView:self.view]];
    if (waitingForNextPlayer && !didLoadNextPlayer) {
        return;
    }
    if (structureMenuVisible) {
        //NSLog(@"STRUCTURE MENU PAN");
        /*
        SKNode *items = [[self childNodeWithName:@"structureMenu"]childNodeWithName:@"items"];
        if (gesture.state == UIGestureRecognizerStateBegan) {
            gesturePanStart = gestureLocation;
            structureMenuStartX = items.position.x;
        }
        
        items.position = CGPointMake(structureMenuStartX+(gestureLocation.x-gesturePanStart.x), 0.0);
        if (gesture.state == UIGestureRecognizerStateEnded) {
            CGPoint vel = [gesture velocityInView:self.view];
            [items.physicsBody applyImpulse:CGVectorMake(vel.x, 0.0)];
        }
         */
        return;
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        prevTouchLocation = CGPointZero;
        CGPoint vel = [gesture velocityInView:self.view];
        //NSLog(@"pan vel: %f x %f",vel.x,vel.y);
        if (ABS(vel.x) > 100.0 && ABS(vel.y) > 100.0) {
            [world.physicsBody applyImpulse:CGVectorMake(vel.x, -vel.y)];
        }
        return;
    }
    
    if (prevTouchLocation.x == 0 && prevTouchLocation.y == 0) {
        prevTouchLocation = gestureLocation;
    }
    
    
    CGFloat moveX = prevTouchLocation.x-gestureLocation.x;
    CGFloat moveY = prevTouchLocation.y-gestureLocation.y;
    
    CGPoint worldPosition = CGPointMake(world.position.x-moveX, world.position.y-moveY);
    world.position = worldPosition;
    NSLog(@"WORLD: %@",world);
    prevTouchLocation = gestureLocation;
}

-(void)swipeFromLeftEdge:(UIScreenEdgePanGestureRecognizer*)gesture {
    //CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    NSLog(@"LEFT EDGE SWIPE");
    if (gesture.state == UIGestureRecognizerStateBegan && !sideMenuVisible) {
        [sideMenu setHidden:NO];
        [sideMenu runAction:[SKAction moveToX:-1.0 duration:0.07]];
        [playerInfoCard runAction:[SKAction moveByX:159.0*factor y:0.0 duration:0.075]];
        sideMenuVisible = YES;
    }
}

-(void)leftSwipeHandler:(UISwipeGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan && sideMenuVisible) {
        [sideMenu runAction:[SKAction moveToX:-170.0*factor duration:0.07] completion:^{
            [sideMenu setHidden:YES];
            sideMenuVisible = NO;
        }];
        [playerInfoCard runAction:[SKAction moveByX:-159.0*factor y:0.0 duration:0.09]];
    }
}

/*
-(void)label:(SKLabelNode*)labelNode animateToFontSize:(CGFloat)fontSize duration:(CGFloat)duration {
    CGFloat prevFontSize = labelNode.fontSize;
    if (labelNode.fontSize < fontSize) {
        [labelNode setFontSize:fontSize];
        [labelNode setScale:prevFontSize/fontSize];
        [labelNode runAction:[SKAction scaleTo:1.0 duration:duration]];
    }
    else {
        [labelNode runAction:[SKAction scaleTo:fontSize/prevFontSize duration:duration] completion:^{
            [labelNode setFontSize:fontSize];
            [labelNode setScale:1.0];
        }];
    }
}
 */

-(void)updateFogForPlayer: (Player*)player {
    //TODO optimize by only updating the changed values?
    
    //remove all fog nodes
    for (SKSpriteNode *node in fogTiles) {
        [node removeFromParent];
    }
    
    //then replace them with new ones
    NSArray *levelForPlayer = player.level;
    NSArray *levelTiles = [GameScene getLevelTiles];
    for (int i = 0; i < levelForPlayer.count; i++) {
        for (int j = 0; j < ((NSArray*)levelForPlayer[i]).count; j++) {
            //if the tile is not empty (it is the actual map)
            if (((NSArray*)levelForPlayer[i][j]).count > 1) {
                //check if it should have a fog of war on this tile
                if (![[levelForPlayer[i][j]objectAtIndex:3]boolValue]) {
                    SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed:@"Fog"];
                    [node setName:@"fog"];
                    [node setZPosition:levelForPlayer.count + 10];
                    node.alpha = 0.6;
                    [levelTiles[i][j] addChild:node];
                    [fogTiles addObject:node];
                }
            }
        }
    }
}

-(void)updateMapForPlayer: (Player*)player {
    [player updateFogMap];
    [self updateFogForPlayer:player];
    [self updateBuildingsForPlayer:player];
    [self updateUnitsForPlayer:player];
    NSMutableArray* trueDeepCopyArray = [NSKeyedUnarchiver unarchiveObjectWithData:
                                  [NSKeyedArchiver archivedDataWithRootObject:levelCostMap]];
    [player updateLevelCostMapWith:trueDeepCopyArray];
    
}

-(void)updateBuildingsForPlayer: (Player*)player {
    //TODO update so the player does not see updated images of captured, repaired or destroyed buildings
}

-(void)updateUnitsForPlayer: (Player*)player {
    //TODO update this when teams come into play (if not sooner)
    for (int i = 0; i < players.count; i++) {
        if (playerOnTurn == i) {
            //make your units visible
            for (Unit *unit in (NSArray*)((Player*)players[i]).friendlyUnits) {
                [self makeUnit:unit invisible:NO forPlayer:player];
            }
            continue;
        }
        for (Unit *unit in (NSArray*)((Player*)players[i]).friendlyUnits) {
            CGPoint pos = unit.unitPosition;
            if (![(((Player*)players[playerOnTurn]).level[(int)pos.y][(int)pos.x][3]) boolValue]) {
                [self makeUnit:unit invisible:YES forPlayer:player];
            }
            else {
                if (!unit.invisible) {
                    //if the unit is not invisible then make it visible
                    [self makeUnit:unit invisible:NO forPlayer:player];
                }
                else {
                    //else if it is invisible then check if it gets detected to determine if it should be visible
                    if ([self unitGetsDetected:unit]) {
                        [self makeUnit:unit invisible:NO forPlayer:player];
                    }
                    else {
                        [self makeUnit:unit invisible:YES forPlayer:player];
                    }
                }
            }
        }
    }
}

-(void)makeUnit: (Unit*)unit invisible: (BOOL) isInvisible forPlayer: (Player*)player{
    if (isInvisible) {
        if (![player.friendlyUnits containsObject:unit]) {
            for (NSArray *enemyUnits in player.enemyUnits) {
                //if the unit is already in the players enemy units list then remove it (because we dont see it anymore)
                if ([enemyUnits containsObject:unit]) {
                    [player.enemyUnits[unit.owner] removeObject:unit];
                    break;
                }
            }
            //TODO this is testing only, for actual gameplay use value 0.0 (so the unit is invisible to enemy)
            [unit setAlpha:0.1];
        }
        else {
            [unit setAlpha:1.0];
        }
    }
    else {
        if (![player.friendlyUnits containsObject:unit]) {
            BOOL temp = YES;
            for (NSArray *enemyUnits in player.enemyUnits) {
                //if the unit is already in the players enemy units list then dont add it again
                if ([enemyUnits containsObject:unit]) {
                    temp = NO;
                }
            }
            if (temp) {
                [player.enemyUnits[unit.owner] addObject:unit];
            }
        }
        [unit setAlpha:1.0];
    }
}

-(BOOL)unitGetsDetected: (Unit*) unitToCheck {
    
    NSArray *neighbors = [self getNeighborsForTile:unitToCheck.unitPosition];
    for (int i = 0; i < neighbors.count; i++) {
        CGPoint pos = [((NSValue*)neighbors[i]) CGPointValue];
        for (int i = 0; i < players.count; i++) {
            if (playerOnTurn == i) {
                for (Unit *unit in (NSArray*)((Player*)players[i]).friendlyUnits) {
                    if (pos.x == unit.unitPosition.x && pos.y == unit.unitPosition.y) {
                        return YES;
                    }
                }
            }
        }
    }
    return NO;
}

-(void)endTurn {
    NSLog(@"END TURN 1");
    if (![players[playerOnTurn] computer]) {
        ((Player*)players[playerOnTurn]).cameraPosition = world.position;
    }
    [self deselectUnit];
    if (++playerOnTurn > players.count-1) {
        playerOnTurn = 0;
        turn++;
    }
    waitingForNextPlayer = YES;
    NSLog(@"END TURN 1.1");
    if (![players[playerOnTurn] computer]) {
        SKSpriteNode *dimNode = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.0 alpha:0.85] size:self.size];
        [dimNode setName:@"dim"];
        [dimNode setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [dimNode setZPosition:9999999.0];
        [self addChild:dimNode];
        
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        [label setFontColor:[UIColor whiteColor]];
        [label setFontSize:50.0*factor];
        [label setText:((Player*)players[playerOnTurn]).playerName];
        [label runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.9 duration:0.9],[SKAction scaleTo:1.0 duration:0.8]]]]];
        [dimNode addChild:label];
    }
    
    for (Player *player in players) {
        if (player.playerNumber == playerOnTurn) {
            
            for (Structure *structure in player.structures) {
                player.money += [[structure.structureDictionary objectForKey:@"GeneratesRevenue"]intValue]*[[levelDictionary objectForKey:@"Income Multiplier"]intValue];
            }
            
            for (Unit *unit in player.friendlyUnits) {
                [unit setCanMove:YES];
                unit.canAttack = YES;
                unit.bestChoice = 0;
            }
            
            
        }
    }
    if (![players[playerOnTurn] computer]) {
        /*[players[playerOnTurn] updateArrays];*/
        [self updateMapForPlayer:players[playerOnTurn]];
        
        [[playerInfoCard childNodeWithName:@"playerColor"]runAction:[SKAction colorizeWithColor:((Player*)players[playerOnTurn]).playerColor colorBlendFactor:1.0 duration:0.4]];
        [((SKLabelNode*)[playerInfoCard childNodeWithName:@"money"])setText:[NSString stringWithFormat:@"%d",((Player*)players[playerOnTurn]).money]];
        [((SKLabelNode*)[sideMenu childNodeWithName:@"GuildName"])setText:[players[playerOnTurn]playerName]];
        [turnLabel setText:@(turn).stringValue];
        
        [world setPosition:((Player*)players[playerOnTurn]).cameraPosition];
    }
    
    /*
    if ([players[playerOnTurn] computer]) {
        waitingForNextPlayer = NO;
        //DO AI STUFF
        //update level map for AI players
        ((Player*)players[playerOnTurn]).levelMap = levelMap;
        
        for (Unit *unit in ((Player*)players[playerOnTurn]).units[playerOnTurn]) {
            [unit setCanMove:YES];
            unit.bestChoice = 0;
        }
        [players[playerOnTurn] playForAI];
    }
        
    //AI is done and you pass the game to next player
    //TODO add waiting so animations finish
    if ([players[playerOnTurn] computer]) {
        [self endTurn];
    }*/
    didLoadNextPlayer = YES;
    NSLog(@"END TURN 2");
}

-(void)checkStructuresForAction {
    //if it's not a infantry unit
    if (selectedUnit.type != 0) {
        return;
    }
    //TODO redo this method and change the distance checker
    for (Structure *structure in [Structure getAllStructuresOnMap]) {
        //NSLog(@"structure name: %@",structure.structureName);
        CGFloat distance = sqrtf(powf(structure.parent.position.x-selectedUnit.position.x, 2)+powf(structure.parent.position.y-selectedUnit.position.y, 2));
        if ([structure.parent containsPoint:selectedUnit.position] || ([structure.structureName isEqualToString:@"Bridge Ruin"] && distance <= 128.0*[Functions deviceFactor])) {
            if (structureToChange == nil) {
                structureToChange = [NSMutableArray array];
            }
            [structureToChange addObject: structure];
            if ([selectedUnit.special isEqualToString:@"Repair"] && [structure.structureDictionary objectForKey:@"NodeAfterChange"] &&
                [[structure.structureDictionary objectForKey:@"Health"]intValue] < 100 && (structure.owner == playerOnTurn || structure.owner == -1)) {
                NSLog(@"show repair option");
                SKSpriteNode *repairButton = [SKSpriteNode spriteNodeWithImageNamed:@"repairButton"];
                [repairButton setPosition:CGPointMake(40.0*[Functions deviceFactor], 40.0*[Functions deviceFactor])];
                [repairButton setName:@"repair"];
                [repairButton setZPosition:10000.0];
                [repairButton setScale:0.0];
                [repairButton runAction:[SKAction scaleTo:1.7 duration:0.12]];
                [structure addChild:repairButton];
                
            }
            else if ([[structure.structureDictionary objectForKey:@"Health"]intValue] > 0 && structure.owner != playerOnTurn) {
                NSLog(@"show capture option");
                SKSpriteNode *captureButton = [SKSpriteNode spriteNodeWithImageNamed:@"unitCapture"];
                [captureButton setPosition:CGPointMake(40.0*[Functions deviceFactor], 40.0*[Functions deviceFactor])];
                [captureButton setName:@"capture"];
                [captureButton setZPosition:10000.0];
                [captureButton setScale:0.0];
                [captureButton runAction:[SKAction scaleTo:1.7 duration:0.12]];
                [structure addChild:captureButton];
                
            }
        }
    }
}

-(void)setSelectedUnit:(Unit*)unit {
    selectedUnit = unit;
}

-(void)showSaveDialog {
    
}

-(void)saveGame:(NSString*)name {
    NSMutableArray *savedGamesArray = [NSMutableArray arrayWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"savedGames.save"]];
    if (!savedGamesArray) {
        savedGamesArray = [NSMutableArray array];
    }
    [NSKeyedArchiver setClassName:@"GameScene" forClass:[GameScene class]];
    NSData *saveData = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSData *compressedSaveData = [saveData gzippedDataWithCompressionLevel:1.0];
    NSMutableDictionary *saveDict = [NSMutableDictionary dictionary];
    [saveDict setObject:name forKey:@"Name"];
    [saveDict setObject:compressedSaveData forKey:@"saveData"];
    [savedGamesArray addObject:saveDict];
    //NSLog(@"array: %@",savedGamesArray);
    BOOL success = [savedGamesArray writeToFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"savedGames.save"] atomically:YES];
    NSLog(@"gameSaved %d",success);
}

-(void)didSimulatePhysics {
    /*
    if (structureMenuVisible) {
        SKNode *items = [[self childNodeWithName:@"structureMenu"]childNodeWithName:@"items"];
        if (items.position.x > 0.0) {
            items.position = CGPointMake(0.0, 0.0);
        }
        else if (items.position.x < -items.calculateAccumulatedFrame.size.width+self.size.width-150.0) {
            items.position = CGPointMake(-items.calculateAccumulatedFrame.size.width+self.size.width-150.0, 0.0);
        }
    }
     */
}

-(void)update:(CFTimeInterval)currentTime {
    playTime = [NSDate timeIntervalSinceReferenceDate] - playStartTime;
    if (sideMenuVisible) {
        [((SKLabelNode*)[sideMenu childNodeWithName:@"playTimeLabel"])setText:[NSString stringWithFormat:@"%.0fs",playTime]];
    }
}

@end
