//
//  LevelLoader.h
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Unit.h"
#import "Structure.h"
#import "Player.h"

@interface LevelLoader : NSObject

+(LevelLoader*)loader;
+(void)resetLevelLoader;
/*
-(void)createExtraDefForAllPlayers;
-(void)updateGround:(SKSpriteNode*)node;
-(void)saveLevelImage:(int)levelNum;*/
 -(void)saveAllLevelImages;

-(SKSpriteNode*)createLevelWithDict:(NSDictionary*)levelDictionary andSettings:(NSDictionary*)gameSettings forPlayers:(NSArray*)players;
-(NSArray*)getLevelTilesNodesArray;
-(NSArray*)getLevelTerrainMap;
-(NSMutableArray*)getLevelCostMap;
-(NSArray*)getTerrainDefence;
-(SKNode*)getGridOverlay;
-(BOOL)createUnit:(int)unitNum ofType:(int)unitType on:(SKNode*)tile forPlayer:(Player*)player;

@property (nonatomic) int maxWidthOfMap;
@property (nonatomic) int incomeMultiplier;
@property (nonatomic,strong) NSArray *structuresArray;
@property (nonatomic,strong) NSArray *unitsArray;
@property (nonatomic,strong) NSArray *levelArray;
@property (nonatomic,strong) SKView *view;
@property (nonatomic,strong) SKScene *gameScene;

@end
