//
//  DesignRoomMenu.h
//  Genesis Project
//
//  Created by Klemen Kosir on 7. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Functions.h"

@interface DesignRoomMenu_Builder : SKScene <UIGestureRecognizerDelegate>

@property (strong, nonatomic) SKScene *mainMenu;

-(void)setMapSize:(CGSize)size;
-(void)editLevel:(NSDictionary*)levelDict;

@end

@interface DesignRoomMenu : SKScene <UIGestureRecognizerDelegate>

@property (strong, nonatomic) SKScene *mainMenu;

@end

@interface DesignRoomMenu_New : SKScene <UIGestureRecognizerDelegate>

@property (strong, nonatomic) SKScene *prevMenu;

@end

@interface DesignRoomMenu_NewCustom : SKScene <UIGestureRecognizerDelegate>

@property (strong, nonatomic) SKScene *mainMenu;

@end

