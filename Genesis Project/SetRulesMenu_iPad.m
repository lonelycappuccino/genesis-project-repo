//
//  SetRulesMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "SetRulesMenu.h"

@implementation SetRulesMenu_iPad

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:40.0/255.0 blue:45.0/255.0 alpha:1.0];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:@"Set Rules"];
        [titleLabel setPosition:CGPointMake(self.size.width/2, self.size.height-80.0)];
        [titleLabel setFontSize:70.0];
        [self addChild:titleLabel];
        
        SKLabelNode *doneLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [doneLabel setText:@"DONE"];
        [doneLabel setName:@"doneLabel"];
        [doneLabel setPosition:CGPointMake(self.size.width-90.0, 50.0)];
        [doneLabel setFontColor:[UIColor greenColor]];
        [self addChild:doneLabel];
    }
    return self;
}


@end
