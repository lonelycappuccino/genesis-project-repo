//
//  MyScene.h
//  Genesis Project
//

//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MainMenu : SKScene

-(void)resetDesignMenu;

@end
