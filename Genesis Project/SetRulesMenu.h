//
//  SetRulesMenu.h
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SetRulesMenu_iPad : SKScene

@property (nonatomic,strong, setter=passLevelAndPlayerData:) NSDictionary *levelAndPlayerData;

@end
