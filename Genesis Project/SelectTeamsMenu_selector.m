//
//  SelectTeamsMenu_selector.m
//  Genesis Project
//
//  Created by Klemen Kosir on 28. 09. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "SelectTeamsMenu.h"

@implementation SelectTeamsMenu_selector {
    NSMutableArray *teamsArray;
    SKSpriteNode *doneButton, *backButton;
    SKNode *teams;
    CGFloat teamsXPos, panXPos;
    SKShapeNode *addButton, *cancelButton, *addButton2;
    BOOL createTeamMenuVisible;
    SKLabelNode *addLabel, *guildNameInputLabel;
    NSArray *createTeamMenuNodes;
    KeyboardView *keyboard;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        teamsArray = [NSMutableArray arrayWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"teams.data"]];
        NSLog(@"Teams: %ld",teamsArray.count);
        
        self.backgroundColor = [UIColor colorWithRed:131.0/255.0 green:140.0/255.0 blue:145.0/255.0 alpha:1.0];
        
        SKSpriteNode *titleBg = [SKSpriteNode spriteNodeWithImageNamed:@"titleBg"];
        [titleBg setPosition:CGPointMake(self.size.width/2, self.size.height-titleBg.size.height/2)];
        [titleBg setZPosition:1.0];
        //[self addChild:titleBg];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:@"Choose guild"];
        [titleLabel setFontSize:30.0*factor];
        [titleLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [titleLabel setZPosition:1.0];
        [titleLabel setPosition:CGPointMake(self.size.width/2, self.size.height-32.0*factor)];
        [self addChild:titleLabel];
        
        /*
        doneButton = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
        [doneButton setPosition:CGPointMake(self.size.width-doneButton.size.width/2, doneButton.size.height/2+7.5*factor)];
        [doneButton setZPosition:1.0];
        [self addChild:doneButton];
        */
        /*
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7.5*factor)];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        */
        SKLabelNode *tipLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [tipLabel setText:@"Double tap to select guild"];
        [tipLabel setFontSize:15.0*factor];
        [tipLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [tipLabel setZPosition:1.0];
        [tipLabel setPosition:CGPointMake(self.size.width/2, 20.0*factor)];
        [self addChild:tipLabel];
        
        /*
        int counter = 1, counterAll = 0;
        CGFloat distance = 150.0, angle = 0.0;
        for (int i = 0; i < 50; i++) {
            
            CGFloat x;
            CGFloat y;
            
            x = cosf(angle)*distance;
            y = sinf(angle)*distance;
            */
            /*
             if (x+pos.x > 0.0 && y+pos.y > 0.0 &&
             x+pos.x < viewSize.width && y+pos.y < viewSize.height) {
             break;
             }
             */
            /*
            angle -= (2*M_PI)/(6.0*counter);
            counterAll++;
            
            if (counterAll/6.0 == counter) {
                NSLog(@"distance");
                distance += 150.0;
                counterAll = 0;
                counter++;
                angle = 0.0;
            }
            
            NSLog(@"LOOP");
            NSLog(@"x: %f y: %f",x,y);
            
            SKShapeNode *circle = [SKShapeNode shapeNodeWithCircleOfRadius:65.0];
            [circle setFillColor:[UIColor whiteColor]];
            [circle setStrokeColor:[UIColor darkGrayColor]];
            [circle setLineWidth:4.0];
            
            circle.position = CGPointMake(self.size.width/2+x, self.size.height/2+y);
            
            [self addChild:circle];
            
        }
        */
        
        teams = [SKNode node];
        
        for (int i = 0; i < teamsArray.count; i++) {
            SKSpriteNode *teamContainer = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(190.0*factor, 210.0*factor)];
            [teamContainer setPosition:CGPointMake(235.0*factor*i, -10.0)];
            [teamContainer setUserData:teamsArray[i]];
            [teams addChild:teamContainer];
            
            SKLabelNode *teamName = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [teamName setPosition:CGPointMake(0.0, -100.0*factor)];
            [teamName setText:[teamsArray[i] objectForKey:@"Name"]];
            [teamName setFontColor:[UIColor darkGrayColor]];
            [teamName setFontSize:18.0*factor];
            [teamContainer addChild:teamName];
            
            SKSpriteNode *mark = [SKSpriteNode spriteNodeWithColor:[UIColor brownColor] size:CGSizeMake(180.0*factor, 180.0*factor)];
            [mark setPosition:CGPointMake(0.0, 10.0*factor)];
            [mark setName:@"Mark"];
            [teamContainer addChild:mark];
        }
        [teams setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:teams];
        
        addButton = [SKShapeNode shapeNodeWithCircleOfRadius:20.0*factor];
        [addButton setFillColor:[UIColor colorWithWhite:0.97 alpha:0.999]];
        [addButton setLineWidth:1.0*factor];
        [addButton setStrokeColor:[UIColor colorWithRed:0.0 green:0.6 blue:0.0 alpha:1.0]];
        [addButton setPosition:CGPointMake(self.size.width-addButton.frame.size.width/2-10.0*factor, self.size.height-addButton.frame.size.height/2-10.0*factor)];
        [addButton setZPosition:9999.0];
        [self addChild:addButton];
        
        addLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [addLabel setText:@"+"];
        [addLabel setFontColor:[UIColor colorWithRed:0.0 green:0.6 blue:0.0 alpha:1.0]];
        [addLabel setFontSize:30.0*factor];
        [addLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [addLabel setPosition:addButton.position];
        [addLabel setZPosition:9999.1];
        [self addChild:addLabel];
        
        createTeamMenuVisible = NO;
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *gesture in view.gestureRecognizers) {
        [view removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapHandler:)];
    [doubleTap setNumberOfTapsRequired:2];
    [view addGestureRecognizer:doubleTap];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panHandler:)];
    [view addGestureRecognizer:pan];
    
    keyboard = [KeyboardView new];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"SINGLE TAP");
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (createTeamMenuVisible) {
        if ([cancelButton containsPoint:location]) {
            [keyboard resignFirstResponder];
            for (SKNode *node in createTeamMenuNodes) {
                [node runAction:[SKAction fadeOutWithDuration:0.4] completion:^{
                    [node removeFromParent];
                }];
            }
            [addButton runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3],[SKAction scaleTo:1.0 duration:0.3]]]];
            [addLabel runAction:[SKAction fadeInWithDuration:0.55]];
            createTeamMenuVisible = NO;
        }
        else if ([addButton2 containsPoint:location] && guildNameInputLabel.text.length > 0) {
            NSLog(@"SAVE GUILD");
            NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"teams.data"];
            NSMutableArray *teamsArrayFromFile = [NSMutableArray arrayWithContentsOfFile:filePath];
            NSMutableDictionary *team = [NSMutableDictionary dictionaryWithObjects:@[@"",guildNameInputLabel.text] forKeys:@[@"Mark",@"Name"]];
            [teamsArrayFromFile addObject:team];
            BOOL success = [teamsArrayFromFile writeToFile:filePath atomically:YES];
            if (success) {
                int index = (int)(teamsArrayFromFile.count) - 1;
                SKSpriteNode *teamContainer = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(190.0*factor, 210.0*factor)];
                [teamContainer setPosition:CGPointMake(235.0*factor*index, -10.0)];
                [teamContainer setUserData:team];
                [teams addChild:teamContainer];
                
                SKLabelNode *teamName = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
                [teamName setPosition:CGPointMake(0.0, -100.0*factor)];
                [teamName setText:[team objectForKey:@"Name"]];
                [teamName setFontColor:[UIColor darkGrayColor]];
                [teamName setFontSize:18.0*factor];
                [teamContainer addChild:teamName];
                
                SKSpriteNode *mark = [SKSpriteNode spriteNodeWithColor:[UIColor brownColor] size:CGSizeMake(180.0*factor, 180.0*factor)];
                [mark setPosition:CGPointMake(0.0, 10.0*factor)];
                [mark setName:@"Mark"];
                [teamContainer addChild:mark];
            }
            
            [keyboard resignFirstResponder];
            for (SKNode *node in createTeamMenuNodes) {
                [node runAction:[SKAction fadeOutWithDuration:0.4] completion:^{
                    [node removeFromParent];
                }];
            }
            [addButton runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3],[SKAction scaleTo:1.0 duration:0.3]]]];
            [addLabel runAction:[SKAction fadeInWithDuration:0.55]];
            createTeamMenuVisible = NO;
        }
        else if ([guildNameInputLabel.parent containsPoint:location]) {
            if (keyboard.isFirstResponder) {
                [keyboard resignFirstResponder];
            }
            else {
                [keyboard becomeFirstResponder];
            }
        }
        return;
    }
    if ([addButton containsPoint:location]) {
        NSLog(@"Add new guild");
        [self showCreateTeamMenu];
    }
}

-(void)doubleTapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"DOUBLE TAP");
    if (createTeamMenuVisible) {
        return;
    }
    CGPoint location = [teams convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    for (SKSpriteNode *team in teams.children) {
        if ([team containsPoint:location]) {
            NSLog(@"CONTAINS");
            NSMutableDictionary *teamDict = [NSMutableDictionary dictionaryWithDictionary:_teamContainer.userData];
            [teamDict addEntriesFromDictionary:team.userData];
            [((SKLabelNode*)[_teamContainer childNodeWithName:@"Name"]) setText:[teamDict objectForKey:@"Name"]];
            [((SKSpriteNode*)[_teamContainer childNodeWithName:@"Mark"]) setTexture:[SKTexture textureWithImageNamed:[teamDict objectForKey:@"Mark"]]];
            [_teamContainer setUserData:teamDict];
            [self.view presentScene:_menu transition:[SKTransition revealWithDirection:SKTransitionDirectionUp duration:0.2]];
            break;
        }
    }
}

-(void)showCreateTeamMenu {
    createTeamMenuVisible = YES;
    /*
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.97 alpha:0.99] size:CGSizeMake(700.0, 700.0)];
    [bg setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [bg setZPosition:9999.0];
    [self addChild:bg];
    */
    [addButton runAction:[SKAction scaleTo:35.0 duration:0.45]];

    [addLabel runAction:[SKAction fadeOutWithDuration:0.3]];
    
    cancelButton = [SKShapeNode shapeNodeWithCircleOfRadius:20.0*factor];
    [cancelButton setFillColor:[UIColor colorWithWhite:0.97 alpha:0.999]];
    [cancelButton setLineWidth:1.0*factor];
    [cancelButton setStrokeColor:[UIColor colorWithRed:0.7 green:0.0 blue:0.0 alpha:1.0]];
    [cancelButton setPosition:CGPointMake(self.size.width-cancelButton.frame.size.width/2-10.0*factor, self.size.height-cancelButton.frame.size.height/2-10.0*factor)];
    [cancelButton setZPosition:99999.0];
    [cancelButton setAlpha:0.0];
    [self addChild:cancelButton];
    
    SKLabelNode *cancelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [cancelLabel setText:@"-"];
    [cancelLabel setFontColor:[UIColor colorWithRed:0.7 green:0.0 blue:0.0 alpha:1.0]];
    [cancelLabel setFontSize:50.0*factor];
    [cancelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [cancelLabel setZPosition:99999.1];
    [cancelButton addChild:cancelLabel];
    
    SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [title setFontColor:[UIColor darkGrayColor]];
    [title setFontSize:30.0*factor];
    [title setText:@"New guild"];
    [title setZPosition:99999.0];
    [title setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [title setPosition:CGPointMake(self.size.width/2, self.size.height-32.0*factor)];
    [title setAlpha:0.0];
    [self addChild:title];
    
    SKLabelNode *guildNameLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [guildNameLabel setFontColor:[UIColor darkGrayColor]];
    [guildNameLabel setFontSize:20.0*factor];
    [guildNameLabel setText:@"Guild name"];
    [guildNameLabel setZPosition:99999.0];
    [guildNameLabel setPosition:CGPointMake(self.size.width*3/4, self.size.height*7/10)];
    [guildNameLabel setAlpha:0.0];
    [self addChild:guildNameLabel];
    
    SKSpriteNode *guildMarkHolder = [SKSpriteNode spriteNodeWithColor:[UIColor brownColor] size:CGSizeMake(200.0*factor, 200.0*factor)];
    [guildMarkHolder setPosition:CGPointMake(self.size.width/4, self.size.height/2)];
    [guildMarkHolder setZPosition:99999.0];
    [guildMarkHolder setAlpha:0.0];
    [self addChild:guildMarkHolder];
    
    SKShapeNode *guildNameHolder = [SKShapeNode shapeNodeWithRect:CGRectMake(-110.0*factor, -15.0*factor, 220.0*factor, 30.0*factor)];
    [guildNameHolder setStrokeColor:[UIColor blackColor]];
    [guildNameHolder setLineWidth:0.5*factor];
    [guildNameHolder setZPosition:99999.0];
    [guildNameHolder setPosition:CGPointMake(self.size.width*3/4, self.size.height/2+20.0*factor)];
    [guildNameHolder setAlpha:0.0];
    [self addChild:guildNameHolder];
    
    addButton2 = [SKShapeNode shapeNodeWithCircleOfRadius:20.0*factor];
    [addButton2 setFillColor:[UIColor colorWithWhite:0.97 alpha:0.999]];
    [addButton2 setLineWidth:1.0*factor];
    [addButton2 setStrokeColor:[UIColor colorWithRed:0.0 green:0.6 blue:0.0 alpha:1.0]];
    [addButton2 setPosition:CGPointMake(addButton2.frame.size.width/2+10.0*factor, self.size.height-addButton2.frame.size.height/2-10.0*factor)];
    [addButton2 setZPosition:99999.0];
    [addButton2 setAlpha:0.0];
    [self addChild:addButton2];
    
    SKLabelNode *addLabel2 = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [addLabel2 setText:@"+"];
    [addLabel2 setFontColor:[UIColor colorWithRed:0.0 green:0.6 blue:0.0 alpha:1.0]];
    [addLabel2 setFontSize:30.0*factor];
    [addLabel2 setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [addLabel2 setZPosition:99999.1];
    [addButton2 addChild:addLabel2];
    
    guildNameInputLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
    [guildNameInputLabel setFontSize:20.0*factor];
    [guildNameInputLabel setFontColor:[UIColor blackColor]];
    [guildNameInputLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [guildNameInputLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeBaseline];
    [guildNameInputLabel setZPosition:99999.1];
    [guildNameInputLabel setPosition:CGPointMake(-guildNameHolder.frame.size.width/2+5.0*factor, -9.0*factor)];
    [guildNameHolder addChild:guildNameInputLabel];
    
    createTeamMenuNodes = @[cancelButton,title,guildNameLabel,guildMarkHolder,guildNameHolder,addButton2];
    for (SKNode *node in createTeamMenuNodes) {
        [node runAction:[SKAction sequence:@[[SKAction waitForDuration:0.3],[SKAction fadeAlphaTo:1.0 duration:0.3]]]];
    }
    
    [self.view addSubview:keyboard];
    [keyboard setTextlabel:guildNameInputLabel];
    [keyboard setMaxLength:guildNameHolder.frame.size.width-10.0*factor];
    [keyboard becomeFirstResponder];
}

- (void)keyboardIsReturned {
    [keyboard resignFirstResponder];
}

-(void)panHandler:(UIPanGestureRecognizer*)gesture {
    NSLog(@"PAN");
    if (createTeamMenuVisible) {
        [self tapHandler:(UITapGestureRecognizer*)gesture];
        return;
    }
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        teamsXPos = teams.position.x;
        panXPos = location.x;
    }
    teams.position = CGPointMake(teamsXPos+(location.x-panXPos), self.size.height/2);
}

@end
