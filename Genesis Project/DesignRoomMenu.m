//
//  DesignRoomMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 17. 08. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "DesignRoomMenu.h"
#import "MainMenu.h"

@implementation DesignRoomMenu {
    NSMutableArray *customMapsArray;
    NSMutableArray *maps;
    SKNode *mapsNode;
    CGFloat startPanX,startMapsNodeX;
    //SKSpriteNode *newMapButton, *backButton;
    MainMenu *mainMenu;
    SKShapeNode *newButton, *backButton;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self loadData];
        //NSLog(@"DESIGN ROOM: %@",customMapsArray);
        
        self.backgroundColor = [UIColor whiteColor];
        
        SKLabelNode *title = [SKLabelNode labelNodeWithText:NSLocalizedString(@"Custom Maps",nil)];
        [title setPosition:CGPointMake(self.size.width/2, self.size.height-35.0*[Functions deviceFactor])];
        [title setFontColor:[UIColor darkGrayColor]];
        [title setFontSize:35.0*[Functions deviceFactor]];
        [title setFontName:@"Futura"];
        [self addChild:title];
        /*
        newMapButton = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(50.0*[Functions deviceFactor], 50.0*[Functions deviceFactor])];
        [newMapButton setPosition:CGPointMake(self.size.width-newMapButton.size.width/2-10.0, self.size.height-newMapButton.size.height/2-10.0)];
        [newMapButton setZPosition:1.0];
        [self addChild:newMapButton];
        */
        if (customMapsArray) {
            [self createMapsCards];
        }
        /*
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-15.0)];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        */
        newButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        newButton.position = CGPointMake(self.size.width-newButton.frame.size.width/2-5.0*factor, self.size.height-newButton.frame.size.height/2-5.0*factor);
        [newButton setFillColor:[UIColor clearColor]];
        [newButton setStrokeColor:[UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:1.0]];
        [newButton setLineWidth:2.0*factor];
        [newButton setZPosition:100.0];
        [self addChild:newButton];
        
        [newButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *newLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        newLabel.fontColor = [UIColor colorWithRed:0.0 green:0.8 blue:0.0 alpha:1.0];
        newLabel.fontSize = 12.0*[Functions deviceFactor];
        newLabel.text = @"NEW";
        newLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [newLabel setZPosition:100.1];
        [newButton addChild:newLabel];
        /*
         backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
         [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7*[Functions deviceFactor])];
         [backButton setZPosition:1.0];
         [self addChild:backButton];
         */
        backButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        backButton.position = CGPointMake(backButton.frame.size.width/2+5.0*factor, self.size.height-backButton.frame.size.height/2-5.0*factor);
        [backButton setFillColor:[UIColor clearColor]];
        [backButton setStrokeColor:[UIColor redColor]];
        [backButton setLineWidth:0.7*factor];
        [backButton setZPosition:100.0];
        [self addChild:backButton];
        
        [backButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *backLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        backLabel.fontColor = [UIColor redColor];
        backLabel.fontSize = 12.0*[Functions deviceFactor];
        backLabel.text = @"back";
        backLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [backLabel setZPosition:100.1];
        [backButton addChild:backLabel];
        
        if (customMapsArray.count == 0) {
            SKLabelNode *infoText = [SKLabelNode labelNodeWithText:NSLocalizedString(@"You dont have any custom mapz yet.",nil)];
            [infoText setFontColor:[UIColor darkGrayColor]];
            [infoText setFontSize:25.0*[Functions deviceFactor]];
            [infoText setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
            [self addChild:infoText];
        }
    }
    return self;
}

-(void)createMapsCards {
    maps = [NSMutableArray array];
    mapsNode= [SKNode node];
    mapsNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(100.0, 100.0)];
    mapsNode.physicsBody.allowsRotation = NO;
    mapsNode.physicsBody.affectedByGravity = NO;
    mapsNode.physicsBody.linearDamping = 9.0;
    mapsNode.physicsBody.mass = 1.0;
    
    for (int i = 0; i < customMapsArray.count; i++) {
        SKNode *mapContainer = [SKNode node];
        
        SKSpriteNode *mapImage = [SKSpriteNode spriteNodeWithTexture:[NSKeyedUnarchiver unarchiveObjectWithData:[customMapsArray[i]objectForKey:@"Image"]]];
        [mapImage setZPosition:0.0];
        [mapImage setSize:CGSizeMake(240.0*factor, 240.0*factor)];
        [mapContainer addChild:mapImage];
        
        SKLabelNode *mapName = [SKLabelNode labelNodeWithText:[customMapsArray[i]objectForKey:@"LevelName"]];
        [mapName setPosition:CGPointMake(0.0, -mapImage.size.height/2-20.0*factor)];
        [mapName setFontSize:20.0*factor];
        [mapName setFontColor:[UIColor blackColor]];
        
        SKNode *options = [SKNode node];
        
        SKSpriteNode *editMap = [SKSpriteNode spriteNodeWithColor:[UIColor yellowColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
        //[editMap setPosition:CGPointMake(mapImage.size.width/2-100.0, mapImage.size.height/2-20.0)];
        [editMap setPosition:CGPointMake(25.0*factor, -165.0*factor)];
        [editMap setName:@"edit"];
        [editMap setZPosition:1.0];
        [options addChild:editMap];
        
        SKSpriteNode *removeMap = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
        //[removeMap setPosition:CGPointMake(mapImage.size.width/2-20.0, mapImage.size.height/2-20.0)];
        [removeMap setPosition:CGPointMake(75.0*factor, -165.0*factor)];
        [removeMap setName:@"remove"];
        [removeMap setZPosition:1.0];
        [options addChild:removeMap];
        
        SKSpriteNode *renameMap = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
        //[renameMap setPosition:CGPointMake(mapImage.size.width/2-180.0, mapImage.size.height/2-20.0)];
        [renameMap setPosition:CGPointMake(-25.0*factor, -165.0*factor)];
        [renameMap setName:@"rename"];
        [renameMap setZPosition:1.0];
        [options addChild:renameMap];
        
        SKSpriteNode *shareMap = [SKSpriteNode spriteNodeWithColor:[UIColor blueColor] size:CGSizeMake(35.0*factor, 35.0*factor)];
        //[renameMap setPosition:CGPointMake(mapImage.size.width/2-180.0, mapImage.size.height/2-20.0)];
        [shareMap setPosition:CGPointMake(-75.0*factor, -165.0*factor)];
        [shareMap setName:@"share"];
        [shareMap setZPosition:1.0];
        [options addChild:shareMap];

        [mapContainer addChild:options];
        [options setName:@"options"];
        
        [mapContainer addChild:mapName];
        [mapContainer setPosition:CGPointMake(self.size.width/2 + 275.0*factor*i, self.size.height/2)];
        [mapsNode addChild:mapContainer];
        [maps addObject:mapContainer];
        
        [mapContainer setUserData:customMapsArray[i]];
    }
    [self addChild:mapsNode];
}

-(void)loadData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"CustomMaps.plist"];
    //NSLog(@"path: %@",filePath);
    if ([fileManager fileExistsAtPath:filePath]) {
        customMapsArray = [NSMutableArray arrayWithContentsOfFile:filePath];
    }
    else {
        customMapsArray = [NSMutableArray array];
    }
}

-(void)saveData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"CustomMaps.plist"];
    [customMapsArray writeToFile:filePath atomically:YES];
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *gestureRecognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:gestureRecognizer];
    }
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panHandler:)];
    [pan setDelegate:self];
    [view addGestureRecognizer:pan];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [tap setDelegate:self];
    [view addGestureRecognizer:tap];
    
    mainMenu = [MainMenu sceneWithSize:self.size];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint gestureLocation = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([newButton containsPoint:gestureLocation]) {
        //NSLog(@"NEW MAP");
        /*
        DesignRoomMenu_Builder *mapBuilder = [DesignRoomMenu_Builder sceneWithSize:self.size];
        [mapBuilder setMainMenu:self];
        [mapBuilder mapSizeMenu];
        [self.view presentScene:mapBuilder];
         */
        DesignRoomMenu_New *newMap = [DesignRoomMenu_New sceneWithSize:self.size];
        [newMap setPrevMenu:self];
        [self.view presentScene:newMap];
        return;
    }
    if ([backButton containsPoint:gestureLocation]) {
        [self.view presentScene:mainMenu transition:[SKTransition crossFadeWithDuration:0.3]];
    }
    for (SKNode *mapContainer in maps) {
        SKNode *options = [mapContainer childNodeWithName:@"options"];
        if (options.alpha < 0.1) {
            continue;
        }
        /*
        if ([[options childNodeWithName:@"edit"]containsPoint:[options convertPoint:gestureLocation fromNode:self]]) {
            //NSLog(@"EDIT");
            DesignRoomMenu_Builder *mapBuilder = [DesignRoomMenu_Builder sceneWithSize:self.size];
            [mapBuilder editLevel:mapContainer.userData];
            [mapBuilder setMainMenu:self];
            [self.view presentScene:mapBuilder];
            break;
        }
         */
        if ([[options childNodeWithName:@"remove"]containsPoint:[options convertPoint:gestureLocation fromNode:self]]) {
            //NSLog(@"REMOVE");
            int index = (int)[maps indexOfObjectIdenticalTo:mapContainer];
            for (int i = index+1; i < maps.count; i++) {
                [maps[i]runAction:[SKAction moveByX:-275.0*factor y:0.0 duration:0.2]];
            }
            [customMapsArray removeObjectIdenticalTo:mapContainer.userData];
            [mapContainer removeFromParent];
            [self saveData];
            break;
        }
        if ([[options childNodeWithName:@"share"]containsPoint:[options convertPoint:gestureLocation fromNode:self]]) {
            //NSLog(@"SHARE");
            [self share:mapContainer.userData];
            break;
        }
    }
}

-(void)panHandler:(UIPanGestureRecognizer*)gesture {
    CGPoint gestureLocation = [self convertPointFromView:[gesture locationInView:self.view]];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        startPanX = gestureLocation.x;
        startMapsNodeX = mapsNode.position.x;
        [mapsNode.physicsBody setVelocity:CGVectorMake(0.0, 0.0)];
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        /*
        [mapsNode.physicsBody applyImpulse:CGVectorMake([gesture velocityInView:self.view].x, 0.0)];
        if (mapsNode.position.x > 0.0) {
            [mapsNode runAction:[SKAction moveToX:0.0 duration:0.5]];
        }
        else if (mapsNode.position.x < -mapsNode.calculateAccumulatedFrame.size.width+400.0) {
            [mapsNode runAction:[SKAction moveToX:-mapsNode.calculateAccumulatedFrame.size.width+400.0 duration:0.5]];
        }
        */
        [mapsNode runAction:[SKAction moveToX:round(mapsNode.position.x/275*factor)*275*factor duration:0.3]];
    }
    
    [mapsNode setPosition:CGPointMake(startMapsNodeX-(startPanX-gestureLocation.x), mapsNode.position.y)];
}

-(void)share:(NSDictionary*)mapDict {
    //NSLog(@"mapDict: %@",mapDict);
    NSData *mapData = [NSKeyedArchiver archivedDataWithRootObject:mapDict];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"mapShareTmp.map"];
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:filePath]) {
        //NSLog(@"create file");
        [[NSFileManager defaultManager]createFileAtPath:filePath contents:nil attributes:nil];
    }
    if (![mapData writeToURL:fileUrl atomically:YES]) {
        NSLog(@"ERROR");
    }
    
    //NSLog(@"mapData: %@",mapData);
    //NSString *map = [mapData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    //NSLog(@"mapString: %@",map);
    
    NSArray *mapToShare = @[fileUrl];
    //NSLog(@"view: %@, widnow: %@, viewController: %@",self.view,self.view.window,self.view.window.rootViewController);
    UIActivityViewController *shareController = [[UIActivityViewController alloc]initWithActivityItems:mapToShare applicationActivities:nil];
    shareController.popoverPresentationController.sourceRect = CGRectMake(0.0, 675.0, 725.0, 700.0);
    shareController.popoverPresentationController.sourceView = self.view;
    //shareController.popoverPresentationController.backgroundColor = [UIColor lightGrayColor];
    shareController.excludedActivityTypes = @[UIActivityTypeMessage];
    [self.view.window.rootViewController presentViewController:shareController animated:YES completion:nil];
}

-(void)update:(NSTimeInterval)currentTime {
    for (SKSpriteNode *map in maps) {
        CGPoint mapLocation = [self convertPoint:map.position fromNode:mapsNode];
        /*
        if ([maps indexOfObject:map] == 0) {
            NSLog(@"mapLocationX: %f, width/2: %f", mapLocation.x, self.size.width/2);
        }
         */
        if (mapLocation.x > 0 && mapLocation.x < self.size.width) {
            [[map childNodeWithName:@"options"] setAlpha:(mapLocation.x <= self.size.width/2 ? mapLocation.x/(self.size.width/2) : 2.0 - (mapLocation.x/(self.size.width/2)))];
            /*
            if ([maps indexOfObject:map] == 0) {
                NSLog(@"alpha %f", map.alpha);
            }
             */
        }
        else {
            [[map childNodeWithName:@"options"] setAlpha:0.0];
        }
    }
}

@end
