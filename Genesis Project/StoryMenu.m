//
//  StoryMenu.m
//  Genesis Project
//
//  Created by Klemen Košir on 16/04/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "StoryMenu.h"
#import "GameScene.h"
#import "LevelLoader.h"
#import "Functions.h"
#import "MainMenu.h"

@implementation StoryMenu {
    MainMenu *mainMenu;
    SKShapeNode *backButton;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        titleLabel.fontColor = [UIColor whiteColor];
        titleLabel.fontSize = 35.0*factor;
        titleLabel.text = @"Story";
        titleLabel.position = CGPointMake(self.size.width/2, self.size.height*4/5);
        [titleLabel setZPosition:100.0];
        [self addChild:titleLabel];
        
        SKSpriteNode *map = [SKSpriteNode spriteNodeWithImageNamed:@"StoryMap"];
        [map setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [map setZPosition:1.0];
        [self addChild:map];
        
        backButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        backButton.position = CGPointMake(backButton.frame.size.width/2+10.0*factor, self.size.height-backButton.frame.size.height/2-10.0*factor);
        [backButton setFillColor:[UIColor colorWithWhite:0.0 alpha:0.6]];
        [backButton setStrokeColor:[UIColor whiteColor]];
        [backButton setLineWidth:0.7*factor];
        [backButton setZPosition:100.0];
        [self addChild:backButton];
        
        [backButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *backLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        backLabel.fontColor = [UIColor whiteColor];
        backLabel.fontSize = 12.0*[Functions deviceFactor];
        backLabel.text = @"back";
        backLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [backLabel setZPosition:100.1];
        [backButton addChild:backLabel];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    mainMenu = [MainMenu sceneWithSize:self.size];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [view addGestureRecognizer:tap];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([backButton containsPoint:location]) {
        [self.view presentScene:mainMenu];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touches began menu");
    //GameScene *gameScene = [GameScene gameSceneWithSize:self.size];
    //[gameScene initGameSceneForLevel:[[LevelLoader loader]levelArray][6]];
    //NSLog(@"present scene");
    //[self.view presentScene:gameScene];
}

-(void)update:(CFTimeInterval)currentTime {
    
}

@end
