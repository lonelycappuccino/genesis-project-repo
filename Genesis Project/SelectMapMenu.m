//
//  VersusNewMenu.m
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "SelectMapMenu.h"
#import "SelectTeamsMenu.h"
#import "LevelLoader.h"
#import "GameScene.h"
#import "Functions.h"
#import "MainMenu.h"

@implementation SelectMapMenu {
    SKLabelNode *doneLabel;
    SelectTeamsMenu *selectTeamsMenu;
    NSArray *customMapsArray;
    NSArray *levelDataArray;
    SKSpriteNode *mapSelectNode;
    SKSpriteNode *mapPreview;
    NSDictionary *levelImages;
    CGFloat prevSelectedItemY;
    NSMutableArray *levelsByNumberOfPlayers; //0 - 2P, 1 - 3P, 2 - 4P, 3 - CUSTOM MAPS
    NSMutableArray *levelsTypeLabels;
    int currentMenuPosition;
    NSArray *titleString;
    SKSpriteNode *doneButton, *indexNode;
    CGFloat prevYPosition, prevYLocation;
    int levelNumber;
    CGFloat pinchValue;
    SKShapeNode *backButton;
    MainMenu *mainMenu;
}
/*
-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self loadData];
        
        NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"screenshots1.plist"];
        levelImages = [NSDictionary dictionaryWithContentsOfFile:filePath];
        
        self.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:40.0/255.0 blue:45.0/255.0 alpha:1.0];
        
        doneButton = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
        [doneButton setPosition:CGPointMake(self.size.width-doneButton.size.width/2, doneButton.size.height/2+15.0)];
        [doneButton setZPosition:1.0];
        [self addChild:doneButton];
        
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7.5*[Functions deviceFactor])];
        [backButton setZPosition:1.0];
        [self addChild:backButton];
        
        SKSpriteNode *titleBg = [SKSpriteNode spriteNodeWithImageNamed:@"titleBg"];
        [titleBg setPosition:CGPointMake(self.size.width/2, self.size.height-titleBg.size.height/2)];
        [titleBg setZPosition:1.0];
        [self addChild:titleBg];
        
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:@"Select Map"];
        [titleLabel setFontSize:32.5*[Functions deviceFactor]];
        [titleLabel setZPosition:1.0];
        [titleLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [titleBg addChild:titleLabel];
        
        mapSelectNode = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenuBg"];
        [mapSelectNode setPosition:CGPointMake(mapSelectNode.size.width/2+20.0, self.size.height/2-32.0)];
        [mapSelectNode setZPosition:1.0];
        [self addChild:mapSelectNode];
        
        SKSpriteNode *leftArrow = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_arrow"];
        [leftArrow setPosition:CGPointMake(-mapSelectNode.size.width/2+12.5*[Functions deviceFactor], mapSelectNode.size.height/2-15.0*[Functions deviceFactor])];
        [leftArrow setZPosition:1.0];
        [mapSelectNode addChild:leftArrow];
        
        SKSpriteNode *rightArrow = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_arrow"];
        [rightArrow setPosition:CGPointMake(mapSelectNode.size.width/2-12.5*[Functions deviceFactor], mapSelectNode.size.height/2-15.0*[Functions deviceFactor])];
        [rightArrow setZPosition:1.0];
        [rightArrow setXScale:-1.0];
        [mapSelectNode addChild:rightArrow];
        
        SKSpriteNode *line = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:CGSizeMake(mapSelectNode.size.width-15.0*[Functions deviceFactor], 1.0)];
        [line setPosition:CGPointMake(0.0, 105.0*[Functions deviceFactor])];
        [line setZPosition:1.0];
        [mapSelectNode addChild:line];
        
        mapPreview = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.9 alpha:0.95] size:CGSizeMake(255.0*[Functions deviceFactor], 325.0*[Functions deviceFactor])];
        [mapPreview setPosition:CGPointMake(self.size.width-mapPreview.size.width/2-10.0*[Functions deviceFactor], self.size.height/2-25.0*[Functions deviceFactor])];
        [mapPreview setTexture:[self getTextureFor:@"Level0"]];
        [mapPreview setSize:mapPreview.texture.size];
        [self addChild:mapPreview];
        
        NSArray *colors = @[[UIColor redColor], [UIColor blueColor], [UIColor greenColor], [UIColor yellowColor], [UIColor grayColor]];
        
        for (int i = 0; i < 5; i++) {
            SKSpriteNode *mapInfoNode = [SKSpriteNode spriteNodeWithColor:colors[i] size:CGSizeMake(25.0*[Functions deviceFactor], 25.0*[Functions deviceFactor])];
            [mapInfoNode setPosition:CGPointMake(mapPreview.size.width/2-(5*mapInfoNode.size.width)+(mapInfoNode.size.width+7.0)*i, -mapPreview.size.height/2)];
            [mapInfoNode setName:@"mapInfo"];
            [mapInfoNode setZPosition:10.0];
            [mapPreview addChild:mapInfoNode];
            
            SKLabelNode *valueLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [valueLabel setText:((NSNumber*)[levelDataArray[0]objectForKey:@"Structures"][i]).stringValue];
            [valueLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [valueLabel setFontSize:15.0*[Functions deviceFactor]];
            [valueLabel setName:@"value"];
            [valueLabel setZPosition:1.0];
            [mapInfoNode addChild:valueLabel];
        }
        
        SKLabelNode *title = [SKLabelNode labelNodeWithText:@"2 Players"];
        [title setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-22.5*[Functions deviceFactor])];
        [title setFontColor:[UIColor darkGrayColor]];
        [title setFontSize:20.0*[Functions deviceFactor]];
        [title setFontName:@"Futura"];
        [title setName:@"title"];
        [title setZPosition:1.0];
        [mapSelectNode addChild:title];
        
        SKSpriteNode *mask = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(mapSelectNode.size.width, 235.0*[Functions deviceFactor])];
        [mask setPosition:CGPointMake(0.0, -12.5*[Functions deviceFactor])];
        
        SKCropNode *mapSelectCropNode = [SKCropNode node];
        [mapSelectCropNode setZPosition:1.0];
        [mapSelectCropNode setMaskNode:mask];
        [mapSelectNode addChild:mapSelectCropNode];
        
        titleString = @[@"2 Players", @"3 Players", @"4 Players", @"Custom Maps"];
        
        levelsByNumberOfPlayers = [NSMutableArray array];
        for (int i = 0; i < 4; i++) {
            SKNode *node = [SKNode node];
            if (i > 0) {
                [node setAlpha:0.0];
            }
            node.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10.0, 10.0)];
            node.physicsBody.mass = 1.0;
            node.physicsBody.affectedByGravity = NO;
            node.physicsBody.linearDamping = 8.5;
            node.physicsBody.contactTestBitMask = 0;
            node.physicsBody.collisionBitMask = 0;
            node.physicsBody.allowsRotation = NO;
            [levelsByNumberOfPlayers addObject:node];
            [node setZPosition:1.0];
            //[mapSelectNode addChild:node];
            
            [mapSelectCropNode addChild:node];
        }
        currentMenuPosition = 0;
        
        indexNode = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(1.5*[Functions deviceFactor], 15.0*[Functions deviceFactor])];
        indexNode.position = CGPointMake(mapSelectNode.size.width/2-5.0*[Functions deviceFactor], mapSelectNode.size.height/2-45.0*[Functions deviceFactor]);
        indexNode.zPosition = 100.0;
        [mapSelectNode addChild:indexNode];
        
        int counter = 0;
        for (NSDictionary *level in levelDataArray) {
            //NSLog(@"LEVEL: %@",level);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[level objectForKey:@"LevelName"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor darkGrayColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setFontSize:18.0*[Functions deviceFactor]];
            [levelLabel setZPosition:1.0];
            //[levelLabel setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-90.0-50.0*((SKNode*)levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]).children.count)];
            //[mapSelectNode addChild:levelLabel];
            //[levelLabel setUserData:[NSMutableDictionary dictionaryWithObject:@(counter) forKey:@"levelNumber"]];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_element"];
            [levelHolder setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-52.5*[Functions deviceFactor]-35.0*[Functions deviceFactor]*((SKNode*)levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]).children.count)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:level]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            
            [levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2] addChild:levelHolder];
            counter++;
        }
        counter = 0;
        for (NSDictionary *customLevel in customMapsArray) {
            NSLog(@"CUSTOM LEVEL: %@",customLevel);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[customLevel objectForKey:@"LevelName"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor darkGrayColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setFontSize:18.0*[Functions deviceFactor]];
            [levelLabel setZPosition:1.0];
            //[levelLabel setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-90.0-50.0*((SKNode*)levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]).children.count)];
            //[mapSelectNode addChild:levelLabel];
            //[levelLabel setUserData:[NSMutableDictionary dictionaryWithObject:@(counter) forKey:@"levelNumber"]];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_element"];
            [levelHolder setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-52.5*[Functions deviceFactor]-35.0*[Functions deviceFactor]*((SKNode*)levelsByNumberOfPlayers[3]).children.count)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:customLevel]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            
            [levelsByNumberOfPlayers[3] addChild:levelHolder];
            counter++;
        }
        
        prevSelectedItemY = 2000.0;
    }
    return self;
}
*/

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        [self loadData];
        
        NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"screenshots.plist"];
        levelImages = [NSDictionary dictionaryWithContentsOfFile:filePath];
        //NSLog(@"levelImages: %@",levelImages.description);
        self.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:40.0/255.0 blue:65.0/255.0 alpha:1.0];
        
        SKSpriteNode *bar = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.98 alpha:0.9] size:CGSizeMake(self.size.width, 50.0*factor)];
        [bar setPosition:CGPointMake(self.size.width/2, self.size.height-bar.size.height/2)];
        [self addChild:bar];
        [bar setZPosition:100.0];
        /*
        doneButton = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
        [doneButton setPosition:CGPointMake(self.size.width-doneButton.size.width/2, self.size.height-doneButton.size.height/2-1.8*[Functions deviceFactor])];
        [doneButton setZPosition:101.0];
        [self addChild:doneButton];
        */
        /*
        backButton = [SKSpriteNode spriteNodeWithImageNamed:@"backButton"];
        [backButton setPosition:CGPointMake(backButton.size.width/2, self.size.height-backButton.size.height/2-7*[Functions deviceFactor])];
        [backButton setZPosition:101.0];
        [self addChild:backButton];
        */
        backButton = [SKShapeNode shapeNodeWithCircleOfRadius:19.0*factor];
        backButton.position = CGPointMake(backButton.frame.size.width/2+5.0*factor, self.size.height-backButton.frame.size.height/2-5.0*factor);
        [backButton setFillColor:[UIColor clearColor]];
        [backButton setStrokeColor:[UIColor redColor]];
        [backButton setLineWidth:0.7*factor];
        [backButton setZPosition:100.0];
        [self addChild:backButton];
        
        [backButton runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.97 duration:1.4],[SKAction scaleTo:1.0 duration:1.4]]]]];
        
        SKLabelNode *backLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        backLabel.fontColor = [UIColor redColor];
        backLabel.fontSize = 12.0*[Functions deviceFactor];
        backLabel.text = @"back";
        backLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [backLabel setZPosition:100.1];
        [backButton addChild:backLabel];
        /*
        SKSpriteNode *titleBg = [SKSpriteNode spriteNodeWithImageNamed:@"titleBg"];
        [titleBg setPosition:CGPointMake(self.size.width/2, self.size.height-titleBg.size.height/2)];
        [titleBg setZPosition:101.0];
        [self addChild:titleBg];
        */
        SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [titleLabel setText:@"Select Map"];
        [titleLabel setFontSize:32.5*[Functions deviceFactor]];
        [titleLabel setZPosition:101.1];
        [titleLabel setFontColor:[UIColor blackColor]];
        [titleLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [bar addChild:titleLabel];
        
        mapSelectNode = [SKSpriteNode node];
        [self addChild:mapSelectNode];
        levelsByNumberOfPlayers = [NSMutableArray arrayWithCapacity:4];
        NSArray *menusLabel = @[@"2 Players",@"3 Players",@"4 Players",@"Custom"];
        levelsTypeLabels = [NSMutableArray arrayWithCapacity:4];
        for (int i = 0; i < 4; i++) {
            SKNode *menuNode = [SKNode node];
            [menuNode setPosition:CGPointMake(0.0, -25.0*factor-(self.size.height-90.0*factor)*i)];
            [levelsByNumberOfPlayers addObject:menuNode];
            [mapSelectNode addChild:menuNode];
            
            SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [label setText:menusLabel[i]];
            [label setFontColor:[UIColor whiteColor]];
            [label setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [label setFontSize:20.0*[Functions deviceFactor]];
            [label setZPosition:1.0];
            [label setPosition:CGPointMake(100.0, self.size.height/5*4-(self.size.height-80.0*factor)*i)];
            [mapSelectNode addChild:label];
            [levelsTypeLabels addObject:label];
            
            menuNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(10.0, 10.0)];
            menuNode.physicsBody.mass = 1.0;
            menuNode.physicsBody.affectedByGravity = NO;
            menuNode.physicsBody.linearDamping = 4;
            menuNode.physicsBody.contactTestBitMask = 0;
            menuNode.physicsBody.collisionBitMask = 0;
            menuNode.physicsBody.allowsRotation = NO;
        }
        int counter = 0;
        for (NSDictionary *level in levelDataArray) {
            //NSLog(@"LEVEL: %@",level);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[level objectForKey:@"LevelName"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor whiteColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setFontSize:18.0*[Functions deviceFactor]];
            [levelLabel setZPosition:1.0];
            [levelLabel setPosition:CGPointMake(0.0, -120.0*factor)];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithTexture:[NSKeyedUnarchiver unarchiveObjectWithData:[levelImages objectForKey:[level objectForKey:@"LevelName"]]]];
            if (levelHolder.texture == nil) {
                levelHolder = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(250.0*factor, 250.0*factor)];
            }
            [levelHolder setSize:CGSizeMake(200.0*factor, 200.0*factor)];
            //NSLog(@"levelHolder: %@",levelHolder);
            [levelHolder setPosition:CGPointMake(self.size.width/2+self.size.width/3*2.2*[[levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]children]count], self.size.height/2)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:level]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            
            [levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2] addChild:levelHolder];
            counter++;
        }
        counter = 0;
        
        for (NSDictionary *level in customMapsArray) {
            //NSLog(@"LEVEL: %@",level);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[level objectForKey:@"LevelName"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor whiteColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setFontSize:18.0*[Functions deviceFactor]];
            [levelLabel setZPosition:1.0];
            [levelLabel setPosition:CGPointMake(0.0, -120.0*factor)];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithTexture:[NSKeyedUnarchiver unarchiveObjectWithData:[levelImages objectForKey:[level objectForKey:@"LevelName"]]]];
            if (levelHolder.texture == nil) {
                levelHolder = [SKSpriteNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(500.0, 500.0)];
            }
            [levelHolder setSize:CGSizeMake(200.0*factor, 200.0*factor)];
            //NSLog(@"levelHolder: %@",levelHolder);
            [levelHolder setPosition:CGPointMake(self.size.width/2+self.size.width/3*2.2*counter, self.size.height/2)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:level]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            
            [levelsByNumberOfPlayers[3] addChild:levelHolder];
            counter++;
        }
        
        currentMenuPosition = 0;
        /*
        for (NSDictionary *customLevel in customMapsArray) {
            NSLog(@"CUSTOM LEVEL: %@",customLevel);
            SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [levelLabel setText:[customLevel objectForKey:@"LevelName"]];
            [levelLabel setName:[NSString stringWithFormat:@"%d",counter]];
            [levelLabel setFontColor:[UIColor darkGrayColor]];
            [levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
            [levelLabel setFontSize:18.0*[Functions deviceFactor]];
            [levelLabel setZPosition:1.0];
            //[levelLabel setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-90.0-50.0*((SKNode*)levelsByNumberOfPlayers[[[level objectForKey:@"Players"]intValue]-2]).children.count)];
            //[mapSelectNode addChild:levelLabel];
            //[levelLabel setUserData:[NSMutableDictionary dictionaryWithObject:@(counter) forKey:@"levelNumber"]];
            
            SKSpriteNode *levelHolder = [SKSpriteNode spriteNodeWithImageNamed:@"selectMenu_element"];
            [levelHolder setPosition:CGPointMake(0.0, mapSelectNode.size.height/2-52.5*[Functions deviceFactor]-35.0*[Functions deviceFactor]*((SKNode*)levelsByNumberOfPlayers[3]).children.count)];
            [levelHolder setUserData:[NSMutableDictionary dictionaryWithDictionary:customLevel]];
            [levelHolder addChild:levelLabel];
            [levelHolder setZPosition:1.0];
            
            [levelsByNumberOfPlayers[3] addChild:levelHolder];
            counter++;
        }
         */
        
        SKLabelNode *tipLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
        [tipLabel setText:@"Double tap to select map"];
        [tipLabel setFontSize:12.0*factor];
        [tipLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [tipLabel setZPosition:100.0];
        [tipLabel setPosition:CGPointMake(self.size.width/2, 10.0*factor)];
        [self addChild:tipLabel];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [tap setDelegate:self];
    [view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftRightSwipeHandler:)];
    [leftSwipe setDelegate:self];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    //[view addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftRightSwipeHandler:)];
    [rightSwipe setDelegate:self];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    //[view addGestureRecognizer:rightSwipe];
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeUpHandler:)];
    [swipeUp setDelegate:self];
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [view addGestureRecognizer:swipeUp];
    
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeDownHandler:)];
    [swipeDown setDelegate:self];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [view addGestureRecognizer:swipeDown];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panHandler:)];
    [pan setDelegate:self];
    [view addGestureRecognizer:pan];
    
    UIScreenEdgePanGestureRecognizer *leftEdge = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(leftEdgeSwipeHandler:)];
    [leftEdge setDelegate:self];
    [leftEdge setEdges:UIRectEdgeLeft];
    //[view addGestureRecognizer:leftEdge];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchHandler:)];
    [pinch setDelegate:self];
    [view addGestureRecognizer:pinch];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapHandler:)];
    [doubleTap setDelegate:self];
    [doubleTap setNumberOfTapsRequired:2];
    [view addGestureRecognizer:doubleTap];
    
    mainMenu = [MainMenu sceneWithSize:self.size];
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        return YES;
    }
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    if ([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
        return YES;
    }
    return  NO;
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([doneButton containsPoint:location]) {
        [selectTeamsMenu createMenuForMap:[[LevelLoader loader]levelArray][levelNumber]];
        [self.view presentScene:selectTeamsMenu];
        return;
    }
    if ([backButton containsPoint:location]) {
        [self.view presentScene:mainMenu transition:[SKTransition crossFadeWithDuration:0.3]];
        return;
    }
    /*
    for (SKNode *levelHolder in ((SKNode*)levelsByNumberOfPlayers[currentMenuPosition]).children) {
        SKLabelNode *level = levelHolder.children.firstObject;
        level.fontColor = [UIColor darkGrayColor];
        if ([levelHolder containsPoint:[levelHolder.parent convertPoint:location fromNode:self]]) {
            SKSpriteNode *mapPreviewTmp = mapPreview.copy;
            //SKTexture *texture = [self getTextureFor:level.text];
            SKTexture *texture = [NSKeyedUnarchiver unarchiveObjectWithData:[levelHolder.userData objectForKey:@"Image"]];
            [mapPreviewTmp setTexture:texture];
            [mapPreviewTmp setSize:texture.size];
            levelNumber = level.name.intValue;
            if (levelHolder.position.y < prevSelectedItemY) {
                [mapPreviewTmp setPosition:CGPointMake(mapPreviewTmp.position.x, -500.0)];
                [mapPreview runAction:[SKAction moveToY:2000.0 duration:0.25]];
            }
            else if (levelHolder.position.y > prevSelectedItemY) {
                [mapPreviewTmp setPosition:CGPointMake(mapPreviewTmp.position.x, 2000.0)];
                [mapPreview runAction:[SKAction moveToY:-500.0 duration:0.25]];
            }
            else if (levelHolder.position.y == prevSelectedItemY) {
                GameScene *gameScene = [GameScene gameSceneWithSize:self.size];
                [gameScene initGameSceneForLevel:levelHolder.userData];
                //NSLog(@"present scene");
                [self.view presentScene:gameScene];
                return;
            }
            SKAction *mapPreviewAction = [SKAction moveToY:self.size.height/2 duration:0.25];
            [mapPreviewAction setTimingMode:SKActionTimingEaseOut];
            [mapPreviewTmp runAction:mapPreviewAction completion:^{
                [mapPreview removeFromParent];
                mapPreview = mapPreviewTmp;
            }];
            [self addChild:mapPreviewTmp];
            level.fontColor = [UIColor redColor];
            prevSelectedItemY = levelHolder.position.y;*/
            /*
            for (int i = 0; i < mapInfoLabels.count; i++) {
                [((SKLabelNode*)mapInfoLabels[i]) setText:((NSNumber*)[levelDataArray[level.name.intValue]objectForKey:@"Structures"][i]).stringValue];
            }
            */
            /*
            int numOfStructureInfoNodes = 0;
            for (NSNumber *value in [levelDataArray[level.name.intValue]objectForKey:@"Structures"]) {
                if (value.intValue > 0) {
                    numOfStructureInfoNodes++;
                }
            }
            
            __block int counter = 0, posCounter = 0;
            [mapPreviewTmp enumerateChildNodesWithName:@"mapInfo" usingBlock:^(SKNode *node, BOOL *stop) {
                SKSpriteNode *sprtNode = (SKSpriteNode*)node;
                SKLabelNode *label = (SKLabelNode*)node.children.firstObject;
                [label setText:((NSNumber*)[levelDataArray[level.name.intValue]objectForKey:@"Structures"][counter++]).stringValue];
                [sprtNode setPosition:CGPointMake(mapPreviewTmp.size.width/2-(numOfStructureInfoNodes*sprtNode.size.width)+(sprtNode.size.width+7.0)*posCounter, -mapPreviewTmp.size.height/2)];
                if ([label.text isEqualToString:@"0"]) {
                    [label.parent setAlpha:0.0];
                }
                else {
                    [label.parent setAlpha:1.0];
                    posCounter++;
                }
                
            }];
            
            //NSLog(@"%@",mapPreview);
        }
    }
             */
}

-(void)doubleTapHandler:(UITapGestureRecognizer*)gesture {
    NSLog(@"DOUBLE TAP");
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    for (SKNode *levelHolder in ((SKNode*)levelsByNumberOfPlayers[currentMenuPosition]).children) {
        if ([levelHolder containsPoint:[levelHolder.parent convertPoint:location fromNode:self]]) {
            /*
            NSLog(@"start loading");
            [self removeAllChildren];
            
            SKSpriteNode *dim = [SKSpriteNode spriteNodeWithColor:[UIColor darkGrayColor] size:self.size];
            [dim setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
            [self addChild:dim];
            [dim setZPosition:9999999.0];
            
            SKLabelNode *loadinglabel = [SKLabelNode labelNodeWithFontNamed:@"Futura"];
            [loadinglabel setFontColor:[UIColor whiteColor]];
            [loadinglabel setFontSize:25.0*factor];
            [loadinglabel setPosition:CGPointMake(0.0, 75.0*factor)];
            [loadinglabel setText:@"LOADING"];
            [dim addChild:loadinglabel];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10000000), dispatch_get_main_queue(), ^{
                NSLog(@"load scene");
                GameScene *gameScene = [GameScene gameSceneWithSize:self.size];
                [gameScene initGameSceneForLevel:levelHolder.userData];
                [self.view presentScene:gameScene];
                NSLog(@"present scene");
            });
             */
            
            selectTeamsMenu = [SelectTeamsMenu sceneWithSize:self.size];
            [selectTeamsMenu passPreviousMenu:self];
            [selectTeamsMenu createMenuForMap:levelHolder.userData];
            [self.view presentScene:selectTeamsMenu transition:[SKTransition crossFadeWithDuration:0.3]];
            
            break;
        }
    }
}

-(void)leftRightSwipeHandler:(UISwipeGestureRecognizer*)gesture {
    //NSLog(@"SWIPE");
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([mapSelectNode containsPoint:location]) {
        if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
            [levelsByNumberOfPlayers[currentMenuPosition]runAction:[SKAction fadeOutWithDuration:0.3]];
            [levelsByNumberOfPlayers[(currentMenuPosition == 3 ? currentMenuPosition = 0 : ++currentMenuPosition)]runAction:[SKAction fadeInWithDuration:0.3]];
        }
        else {
            [levelsByNumberOfPlayers[currentMenuPosition]runAction:[SKAction fadeOutWithDuration:0.3]];
            [levelsByNumberOfPlayers[(currentMenuPosition == 0 ? currentMenuPosition = 3 : --currentMenuPosition)]runAction:[SKAction fadeInWithDuration:0.3]];
        }
        if ([levelsByNumberOfPlayers[currentMenuPosition]calculateAccumulatedFrame].size.height <= 220.0*factor) {
            [indexNode setHidden:YES];
        }
        else {
            [indexNode setHidden:NO];
        }
        [((SKLabelNode*)[mapSelectNode childNodeWithName:@"title"])setText:titleString[currentMenuPosition]];
    }
}

-(void)loadData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"CustomMaps.plist"];
    if ([fileManager fileExistsAtPath:filePath]) {
        customMapsArray = [NSArray arrayWithContentsOfFile:filePath];
    }
    
    levelDataArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"LevelData" ofType:@"plist"]];
}

-(void)swipeUpHandler:(UISwipeGestureRecognizer*)gesture {
    //NSLog(@"SWIPE UP");
    if (currentMenuPosition < 3) {
        [mapSelectNode runAction:[SKAction moveByX:0.0 y:(self.size.height-90.0*factor)*mapSelectNode.xScale duration:0.17]];
        currentMenuPosition++;
    }
}

-(void)swipeDownHandler:(UISwipeGestureRecognizer*)gesture {
    //NSLog(@"SWIPE DOWN");
    if (currentMenuPosition > 0) {
        [mapSelectNode runAction:[SKAction moveByX:0.0 y:-(self.size.height-90.0*factor)*mapSelectNode.xScale duration:0.17]];
        currentMenuPosition--;
    }
}

-(void)pinchHandler:(UIPinchGestureRecognizer*)gesture {
    //NSLog(@"PINCH");
    if (gesture.state == UIGestureRecognizerStateBegan) {
        pinchValue = gesture.scale;
        return;
    }
    if (pinchValue > gesture.scale) {
        //zoom-out
        [mapSelectNode runAction:[SKAction scaleTo:0.5 duration:0.5]];
    }
    else if (pinchValue < gesture.scale) {
        //zoom-in
        [mapSelectNode runAction:[SKAction scaleTo:1.0 duration:0.5]];
    }
}

-(void)panHandler:(UIPanGestureRecognizer*)gesture {
    //NSLog(@"PAN HANDLER");
    SKNode *currMenu;
    CGPoint location = [mapSelectNode convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    for (SKNode *curr in levelsByNumberOfPlayers) {
        if ([curr containsPoint:location]) {
            currMenu = curr;
            break;
        }
    }
    //SKNode *currMenu = levelsByNumberOfPlayers[currentMenuPosition];
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        //NSLog(@"TEST");
        prevYPosition = currMenu.position.x;
        prevYLocation = location.x;
    }
    [currMenu setPosition:CGPointMake(prevYPosition+(location.x-prevYLocation), currMenu.position.y)];
    //NSLog(@"width: %f, pos: %f",currMenu.calculateAccumulatedFrame.size.width,currMenu.position.x);
    
    if (currMenu.position.x > 0.0) {
        currMenu.position = CGPointMake(0.0, currMenu.position.y);
    }
    else if (currMenu.position.x < -currMenu.calculateAccumulatedFrame.size.width+self.size.width/2) {
        currMenu.position = CGPointMake(-currMenu.calculateAccumulatedFrame.size.width+self.size.width/2, currMenu.position.y);
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        CGPoint veloc = [gesture velocityInView:gesture.view];
        [currMenu.physicsBody applyImpulse:CGVectorMake(veloc.x, 0.0)];
    }
    
    /*
    SKNode *currMenu = levelsByNumberOfPlayers[currentMenuPosition];
    currMenu.physicsBody.velocity = CGVectorMake(0.0, 0.0);
    
    CGPoint location = [mapSelectNode convertPoint:[self convertPointFromView:[gesture locationInView:self.view]] fromNode:self];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        prevYPosition = currMenu.position.y;
        prevYLocation = location.y;
    }
    [currMenu setPosition:CGPointMake(0.0, prevYPosition+(location.y-prevYLocation))];
    //NSLog(@"height: %f",currMenu.calculateAccumulatedFrame.size.height);
    if (currMenu.position.y < 0.0 || currMenu.calculateAccumulatedFrame.size.height <= 440.0) {
        currMenu.position = CGPointZero;
        return;
    }
    else if (currMenu.position.y > currMenu.calculateAccumulatedFrame.size.height-440.0) {
        currMenu.position = CGPointMake(0.0, currMenu.calculateAccumulatedFrame.size.height-440.0);
        return;
    }
    if (gesture.state == UIGestureRecognizerStateEnded) {
        CGPoint veloc = [gesture velocityInView:gesture.view];
        [currMenu.physicsBody applyImpulse:CGVectorMake(0.0, -veloc.y)];
    }*/
    /*
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSLog(@"gesture ended");
        CGPoint velocity = [gesture velocityInView:self.view];
        SKAction * action = [SKAction moveToY:currMenu.position.y-velocity.y/30.0 duration:0.5];
        [action setTimingMode:SKActionTimingEaseOut];
        [currMenu runAction:action];
    }
     */
}

-(void)didSimulatePhysics {
    /*
    SKNode *currMenu = levelsByNumberOfPlayers[currentMenuPosition];
    if (currMenu.position.y < 0.0 || currMenu.calculateAccumulatedFrame.size.height <= 440.0) {
        currMenu.position = CGPointZero;
        currMenu.physicsBody.velocity = CGVectorMake(0.0, 0.0);
    }
    else if (currMenu.position.y > currMenu.calculateAccumulatedFrame.size.height-440.0) {
        currMenu.position = CGPointMake(0.0, currMenu.calculateAccumulatedFrame.size.height-440.0);
        currMenu.physicsBody.velocity = CGVectorMake(0.0, 0.0);
    }
    [indexNode setPosition:CGPointMake(indexNode.position.x, mapSelectNode.size.height/2-90.0-currMenu.position.y*((mapSelectNode.size.height+90)/currMenu.calculateAccumulatedFrame.size.height))];
    //[indexNode setPosition:CGPointMake(indexNode.position.x, mapSelectNode.size.height/2-90.0-currMenu.position.y)];
     */
    for (SKNode *currMenu in levelsByNumberOfPlayers) {
        if (currMenu.position.x >= 0.0) {
            currMenu.physicsBody.velocity = CGVectorMake(0.0, 0.0);
            currMenu.position = CGPointMake(0.0, currMenu.position.y);
        }
        else if (currMenu.position.x < -currMenu.calculateAccumulatedFrame.size.width+self.size.width/2) {
            currMenu.physicsBody.velocity = CGVectorMake(0.0, 0.0);
            currMenu.position = CGPointMake(-currMenu.calculateAccumulatedFrame.size.width+self.size.width/2, currMenu.position.y);
        }
    }
}

-(void)leftEdgeSwipeHandler:(UIScreenEdgePanGestureRecognizer*)gesture {
    //NSLog(@"LEFT EDGE SWIPE");
    [self.view presentScene:mainMenu transition:[SKTransition moveInWithDirection:SKTransitionDirectionLeft duration:0.5]];
}

-(SKTexture*)getTextureFor:(NSString*)level {
    //TODO data is empty
    NSData *data = [levelImages objectForKey:level];
    SKTexture *texture = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return texture;
}


@end
