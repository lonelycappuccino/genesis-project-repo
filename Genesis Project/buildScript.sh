#!/bin/bash
# This was taken from variations from:
# http://davedelong.com/blog/2009/04/15/incrementing-build-numbers-xcode

if [ "$CONFIGURATION" != "Release" ]
then
exit
fi


buildPlist="$SRCROOT/$PROJECT_NAME/$PROJECT_NAME-Info.plist"


# Get the existing buildVersion and buildNumber values from the buildPlist
buildVersion=$(/usr/libexec/PlistBuddy -c "Print CFBuildVersion" "$buildPlist")
echo "$buildVersion"

buildNumber=$(/usr/libexec/PlistBuddy -c "Print CFBuildNumber" "$buildPlist")
echo "$buildNumber"

buildDate=$(date "+%s")


# Increment the buildNumber
buildNumber=$(($buildNumber + 1))

# Set the version numbers in the buildPlist
/usr/libexec/PlistBuddy -c "Set :CFBuildNumber $buildNumber" "$buildPlist"
/usr/libexec/PlistBuddy -c  "Set :CFBuildDate $buildDate" "$buildPlist"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildVersion.$buildNumber" "$buildPlist"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $buildVersion.$buildNumber" "$buildPlist"
/usr/libexec/PlistBuddy -c "Set :CFBundleLongVersionString $buildVersion.$buildNumber.$buildDate" "$buildPlist"
