//
//  SelectTeamsMenu.h
//  Genesis Project
//
//  Created by Klemen Kosir on 12. 07. 14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Functions.h"

@interface SelectTeamsMenu : SKScene

@property (nonatomic) int numberOfPlayers;
@property (nonatomic,strong,setter=passPreviousMenu:) SKScene *previousMenu;

-(void)createMenuForMap:(NSDictionary*)levelDict;
-(void)setTeam:(NSDictionary*)teamDict;

@end

@interface SelectTeamsMenu_selector : SKScene

@property (nonatomic,strong) SelectTeamsMenu *menu;
@property (nonatomic,strong, setter=passTeam:) SKNode *teamContainer;

@end
